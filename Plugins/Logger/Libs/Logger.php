<?php
/**
 * Logger plugin library Logger class file.
 *
 * @package Slick
 * @subpackage Plugins\Logger\libs
 * @since Version 1.0.0
 * @author Filipe Silva <silvam.filipe@gmail.com>
 *
 * @license Apache License, Version 2.0 (the "License")
 * @copyright 2013 Filipe Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Logger\Libs;

use \Slick\Base as Base;
use \Slick\Core\Exception as Exception;

/**
 * The logger class
 *
 * @package Slick
 * @subpackage Plugins\Logger
 */
class Logger extends Base {

	/**
	 * The log file with full path.
	 * @readwrite
	 * @var string
	 */
	protected $_file = null;

	/**
	 * The log entries list
	 * @var array
	 */
	protected $_entries = array();

	/**
	 * The time started the log (microtime)
	 * @var float
	 */
	protected $_start;

	/**
	 * The time ended the log (microtime)
	 * @var Float
	 */
	protected $_end;

	/**
	 * Overrides constructor to set default values.
	 *
	 * @param array|Object $options The properties for the object beeing constructed.
	 */
	public function __construct($options = array()) {
		parent::__construct($options);
		if (is_null($this->_file)) {
			throw new Exception\Property("Log file is invalid");
		}
		$this->_start = microtime(true);
	}

	/**
	 * Sums a list of values.
	 *
	 * @param array $values The list of values to sum.
	 * @return integer The total sum of values in list.
	 */
	protected function _sum($values) {
		$count = 0;
		foreach ($values as $value) {
			$count += $value;
		}
		return $count;
	}

	/**
	 * Calculates the avarage value of a list of values.
	 *
	 * @param array $values The list of values to retreive the avarage.
	 * @return float The avarage value from the the list of values.
	 */
	protected function _avarage($values) {
		if (sizeof($values) > 0) {
			return $this->_sum($values) / sizeof($values);
		}
		return 0;
	}

	/**
	 * Logs a message to the logger.
	 *
	 * @param string $message The message to log
	 */
	public function log($message) {
		static $l;
		if (!$l) {
			$l = $this->_start;
		}
		$time = microtime(true);
		$formated = number_format(($time - $l), 5) . 's';
		$l = $time;
		$this->_entries[] = array(
			"message" => "[" . date("Y-m-d H:i:s") . "][{$formated}] " . $message,
			"time" => $time
		);
	}

	/**
	 * Write down the log before destruction.
	 */
	public function __destruct() {
		$messages = "";
		$last = $this->_start;
		$times = array();

		foreach ($this->_entries as $entry) {
			$messages .= "{$entry['message']}\n";
			$times[] = $entry['time'] - $last;
			$last = $entry['time'];
		}

		$messages .= "\n Avarage: " . number_format($this->_avarage($times), 5);
		$messages .= ", Longest: " . number_format(max($times), 5);
		$messages .= ", Shortest: " . number_format(min($times), 5);
		$total = number_format((microtime(true) - $this->_start), 5);
		$messages .= ", Total: {$total}";
		$messages .= "\n\n";

		file_put_contents($this->_file, $messages, FILE_APPEND);
	}

}
