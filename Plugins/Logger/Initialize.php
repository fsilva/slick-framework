<?php
/**
 * Slick Framework plugin initialize class.
 *
 * @package Slick
 * @subpackage Plugins\Logger
 * @since Version 1.0.0
 * @author Filipe Silva <silvam.filipe@gmail.com>
 *
 * @license Apache License, Version 2.0 (the "License")
 * @copyright 2013 Filipe Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Logger;

use \Slick\Events as Events;

/**
 * Logger plugin initialization class.
 *
 * @package Slick
 * @subpackage Plugins\Logger
 */
class Initialize implements \Slick\Core\PluginInterface {

	/**
	 * The plugin readable name.
	 * @var string
	 */
	public static $name = 'Logger';

	/**
	 * The plugin version
	 * @var string
	 */
	public static $version = '1.0.0';

	/**
	 * Bootstrap the plugin. This will be called from \Slick\Core::load() method.
	 *
	 * @see \Slick\Core::load()
	 */
	public static function bootstrap() {

		$logger = new \Logger\Libs\Logger(array(
			"file" => APP_PATH . '/Tmp/Logs/' . date("Y-m-d") . '.txt'
		));

		//log cache events
		Events::add('cache.before.initialize', function($cache) use ($logger) {
			$logger->log('Setting cache type: ' . $cache->getType());
		});

		Events::add('cache.after.initialize', function($cache) use ($logger) {
			$logger->log('Cache initializes successfully using: ' . $cache->getType());
		});

		//log configuration events
		Events::add('configuration.before.initialize', function($configuration) use ($logger) {
			$logger->log('Setting configuration type: ' . $configuration->getType());
		});

		Events::add('configuration.after.initialize', function($configuration) use ($logger) {
			$logger->log('Configuration initializes successfully using: ' . $configuration->getType());
		});

		//log database events
		Events::add('database.before.initialize', function($database) use ($logger) {
			$logger->log('Setting database type: ' . $database->getType());
		});

		Events::add('database.after.initialize', function($database) use ($logger) {
			$logger->log('Database initializes successfully using: ' . $database->getType());
		});

		//log session events
		Events::add('session.before.initialize', function($session) use ($logger) {
			$logger->log('Setting session type: ' . $session->getType());
		});

		Events::add('session.after.initialize', function($session) use ($logger) {
			$logger->log('Session initializes successfully using: ' . $session->getType());
		});

		//log controller events
		Events::add('controller.before.construct', function($name) use ($logger) {
			$logger->log('Initializing controller: ' . $name);
		});

		Events::add('controller.after.construct', function($controller, $name) use ($logger) {
			$logger->log("Controller {$name} successfully initialized");
		});

		Events::add('controller.before.render', function($controller) use ($logger) {
			$name = $controller->getName();
			$logger->log("Controller {$name} prepare for render output");
		});

		Events::add('controller.after.render', function($controller) use ($logger) {
			$name = $controller->getName();
			$logger->log("Controller {$name} has rendered the output");
		});

		//log router events
		Events::add('router.before.dispatch', function($router) use ($logger) {
			$logger->log("Router is preparing to dispatch: ".$router->url);
		});

		Events::add('router.after.dispatch', function($router) use ($logger) {
			$logger->log("Router dispatched: ".$router->url);
		});

	}
}