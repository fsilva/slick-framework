<?php
namespace Codeception\Module;

require dirname(__FILE__) . '/SlickConnector.php';

class Slick extends \Codeception\Util\Framework {

	public function _initialize() {

	}

	public function _before(\Codeception\TestCase $test) {
		$this->client = new \Codeception\Util\Connector\Slick();
	}

	public function _after(\Codeception\TestCase $test) {
		$_SESSION = array();
		$_GET = array();
		$_POST = array();
		$_COOKIE = array();
		parent::_after($test);
	}

}