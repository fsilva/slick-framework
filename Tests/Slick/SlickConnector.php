<?php

namespace Codeception\Util\Connector;

use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;
use Slick\Core;
use Slick\Router;
use Slick\Registry;

class Slick extends \Symfony\Component\BrowserKit\Client
{

    public function setIndex($index)
    {
        $this->index = $index;
    }

    public function doRequest($request)
    {

        //print_r($request); die();
        $_COOKIE = $request->getCookies();
        $_SERVER = $request->getServer();
        $_FILES = $request->getFiles();

        Core::$debugMode = 1;

        // Load application bootstrap file
        if (is_file(APP_PATH.'/Configuration/Bootstrap.php')) {
            include APP_PATH.'/Configuration/Bootstrap.php';
        }

        Core::bootstrap();

        $uri = str_replace('http://localhost/', '', $request->getUri());

        $router = new Router();
        if (is_file(APP_PATH.'/Configuration/Routes.php')) {
            include APP_PATH.'/Configuration/Routes.php';
        }

        preg_match('/^([a-zA-Z0-9\/\-_]+)\.?([a-zA-Z]+)?$/i', $uri, $matches);

        $router->setUrl(isset($matches[1]) ? $matches[1] : "pages/home")
        ->setExtension(isset($matches[2]) ? $matches[2] : "html");

        Registry::set('router', $router);
        $router->dispatch();

        $response = $router->getResponse();

        Registry::erase('request');
        Registry::erase('response');

        $data = new Response($response->body, $response->statusCode, $response->headers);
        return $data;
    }

}