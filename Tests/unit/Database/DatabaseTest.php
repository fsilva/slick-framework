<?php

/**
 * DatabaseTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */


namespace Database;

use Codeception\Util\Stub;
use Slick\Database;

/**
 * DatabaseTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class DatabaseTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    // tests
    
    /**
     * Test for not implemented method exception
     * @test
     * @expectedException \Slick\Database\Exception\Implementation
     */
    public function callUnimplementedMethod()
    {
        $db = new Database();
        $db->notImplemented();
    }

    /**
     * Test for not implemented method exception
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function initializeWithInvalidType()
    {
        $db = new Database();
        $db->initialize();
    }

    /**
     * Test connector initializarion
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function initializeDatabase()
    {
        $db = new Database(array('type' => 'mysql'));
        $conn = $db->initialize();
        $this->assertInstanceOf('\Slick\Database\Connector', $conn);
        $this->assertInstanceOf('\Slick\Database\Connector\Mysql', $conn);

        $db->type = 'sqlite';
        $conn = $db->initialize();
        $this->assertInstanceOf('\Slick\Database\Connector', $conn);
        $this->assertInstanceOf('\Slick\Database\Connector\Sqlite', $conn);

        $db->type = 'unknown';
        $conn = $db->initialize();
    }

    /**
     * Checking exception output
     * @test
     */
    public function verifyExceptionOutput()
    {
        $debug = \Slick\Core::$debugMode;
        \Slick\Core::$debugMode = 2;
        $e = new \Slick\Database\Exception\Sql(
            'Cannot run sql',
            'Invalid field',
            'SELECT TEST FROM users'
        );
        $this->assertTrue(is_string($e->getDescription()));
        \Slick\Core::$debugMode = $debug;
    }

}