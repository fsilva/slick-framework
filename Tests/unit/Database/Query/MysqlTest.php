<?php

/**
 * MysqlTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Database\Query;

use Codeception\Util\Stub;
use Slick\Database\Query\Mysql as Query;
use Slick\Database\Connector\Mysql as Connector;

/**
 * MysqlTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class MysqlTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT (Mysql query)
     * @var Slick\Database\Query\Mysql
     */
    protected $query;

    /**
     * Creating the environment for tests
     */
    protected function _before()
    {
        parent::_before();
        $connector = new MockedConnector();
        $this->query = new Query(array('connector' => $connector));
        MockedConnector::$fail = false;
    }

    protected function _after()
    {
        unset($this->query);
        parent::_after();
    }

    // tests
   
   /**
     * Check the core implementation for non implemented methods.
     * @test
     * @expectedException \Slick\Database\Exception\Implementation
     */
    public function notImplemented()
    {
        $this->query->notImplemented();
    }

    /**
     * Testing the SQL from assign
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function queryFrom()
    {
        $q = $this->query->from('users');
        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('\Slick\Database\Query', $q);
        $this->assertEquals('users', $this->query->getFrom());
        $fields = $this->query->getFields();
        $this->assertEquals(array('*'), $fields['users']);

        $q = $this->query->from('users', array('id', 'username'));
        $fields = $this->query->getFields();
        $this->assertEquals(array('id', 'username'), $fields['users']);

        $this->query->from(null);
    }

    /**
     * Testing the SQL join assign
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function queryJoin()
    {
        $q = $this->query->join('profiles', 'profile.id = user.id');
        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('\Slick\Database\Query', $q);

        $this->assertEquals(array('profiles' => array()), $this->query->getFields());
        $this->assertContains('JOIN profiles ON profile.id = user.id', $this->query->getJoin());

        try {
            $this->query->join('profiles', null);
            $this->fail("This should raise an exception here.");
        } catch(\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Database\Exception\Argument', $e);
        }

        $this->query->join(null, null);
    }

    /**
     * Testing the SQL limit assign
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function queryLimit()
    {
        $q = $this->query->limit(10, 3);
        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('\Slick\Database\Query', $q);
        $this->assertEquals(10, $this->query->getLimit());
        $this->assertEquals((10 * (3 - 1)), $this->query->getOffset());

        $this->query->limit(null);
    }

    /**
     * Testing the SQL limit assign
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function queryOrderBy()
    {
        $q = $this->query->order('name', 'DESC');

        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('\Slick\Database\Query', $q);
        $this->assertEquals('name', $this->query->getOrder());
        $this->assertEquals('DESC', $this->query->getDirection());

        $this->query->order(null);
    }

    /**
     * Testing the SQL where clause
     * @test
     * @expectedException \Slick\Database\Exception\Argument
     */
    public function queryWhere()
    {
        $q = $this->query->where("user.id = ?", 1);

        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('Slick\Database\Query', $q);

        $expected = array('clause' => 'user.id = 1', 'op' => ' AND ');
        $this->assertContains($expected, $this->query->getWhere());

        $q = $this->query->where("user.name = ?", '%fsilva%');
        $expected = array('clause' => "user.name = '%fsilva%'", 'op' => ' AND ');
        $this->assertContains($expected, $this->query->getWhere());

        $q = $this->query->where("user.active = ?", true);
        $expected = array('clause' => "user.active = 1", 'op' => ' AND ');
        $this->assertContains($expected, $this->query->getWhere());

        $q = $this->query->where("user.password <> ?", null);
        $expected = array('clause' => "user.password <> NULL", 'op' => ' AND ');
        $this->assertContains($expected, $this->query->getWhere());


        $q = $this->query->where("user.age IN (?)", array(2, 4, 5, 6, 7, 8, 10));
        $expected = array('clause' => "user.age IN (2, 4, 5, 6, 7, 8, 10)", 'op' => ' AND ');
        $this->assertContains($expected, $this->query->getWhere());

        $this->query->where();
    }

    /**
     * Testing the orWhere andWhere methods
     * @test
     */
    public function queryWhereAlias()
    {
        $q = $this->query->andWhere("user.id = ?", 1);

        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('\Slick\Database\Query', $q);

        $expected = array('clause' => 'user.id = 1', 'op' => ' AND ');
        $this->assertContains($expected, $this->query->getWhere());

        $q = $this->query->orWhere("user.id = ?", 1);

        $this->assertInstanceOf('\Slick\Database\Query\Mysql', $q);
        $this->assertInstanceOf('\Slick\Database\Query', $q);

        $expected = array('clause' => 'user.id = 1', 'op' => ' OR ');
        $this->assertContains($expected, $this->query->getWhere());
    }

    /**
     * Check the first and all methods
     * @test
     * @expectedException \Slick\Database\Exception\Sql
     */
    public function checkFirstAll()
    {
        $this->query
            ->from('users AS User', array('User.id' => 'number', 'User.name'))
            ->where('name LIKE ?', '%fsilva%')
            ->orWhere('name LIKE ?', '%other name%')
            ->order('name', 'ASC')
            ->join('profiles AS Profile', 'User.profile_id = Profile.id', array('Profile.group'))
            ->limit(5, 3)
            ->first();

        $expected =  "SELECT User.id AS number, User.name, Profile.group FROM users AS User";
        $expected .= " JOIN profiles AS Profile ON User.profile_id = Profile.id";
        $expected .= " WHERE name LIKE '%fsilva%' OR name LIKE '%other name%'";
        $expected .= " ORDER BY name ASC";
        $expected .= " LIMIT 1";

        $this->assertEquals($expected, MockedConnector::$query);

        $this->assertEquals(5, $this->query->getLimit());
        $this->assertEquals((5 * (3 - 1)), $this->query->getOffset());


        $all = $this->query->all();

        $expected =  "SELECT User.id AS number, User.name, Profile.group FROM users AS User";
        $expected .= " JOIN profiles AS Profile ON User.profile_id = Profile.id";
        $expected .= " WHERE name LIKE '%fsilva%' OR name LIKE '%other name%'";
        $expected .= " ORDER BY name ASC";
        $expected .= " LIMIT 5, 10";

        $this->assertEquals($expected, MockedConnector::$query);

        $this->assertEquals(1, count($all));
        $expected = array(
            'User' => array(
                'id' => 1,
                'name' => 'fsilva',
                'group' => 'admin'
            )
        );
        $this->assertContains($expected, $all);

        MockedConnector::$fail = true;
        $this->query->first();
    }

    /**
     * Testing count 
     * @test
     */
    public function queryCount()
    {
        $rows = $this->query
            ->from('users AS User', array('User.id' => 'number', 'User.name'))
            ->where('name LIKE ?', '%fsilva%')
            ->orWhere('name LIKE ?', '%other name%')
            ->order('name', 'ASC')
            ->join('profiles AS Profile', 'User.profile_id = Profile.id', array('Profile.group'))
            ->limit(5, 3)
            ->count();
        $this->assertEquals(3, $rows);

        $expected =  "SELECT COUNT(1) AS rows FROM users AS User";
        $expected .= " JOIN profiles AS Profile ON User.profile_id = Profile.id";
        $expected .= " WHERE name LIKE '%fsilva%' OR name LIKE '%other name%' LIMIT 1";

        $this->assertEquals($expected, MockedConnector::$query);
    }

    /**
     * Testing delete
     * @test
     * @expectedException \Slick\Database\Exception\Sql
     */
    public function queryDelete()
    {
        $rows = $this->query
            ->from('users')
            ->where('users.id = ?', 2)
            ->limit(2)
            ->delete();
        $this->assertEquals(2, $rows);
        $expected =  "DELETE FROM users WHERE users.id = 2 LIMIT 2";
        $this->assertEquals($expected, MockedConnector::$query);
        MockedConnector::$fail = true;
        $this->query->delete();
    }

    /**
     * Testing save method.
     * @test
     * @expectedException \Slick\Database\Exception\Sql
     */
    public function querySave()
    {
        $data = array('name' => 'test');
        $id = $this->query->from('users')->save($data);
        $this->assertEquals(30, $id);
        $expected = "INSERT INTO users (`name`) VALUES ('test')";
        $this->assertEquals($expected, MockedConnector::$query);

        $id = $this->query
            ->from('users')
            ->where('id = ?', 10)
            ->limit(1)
            ->save($data);
        $this->assertEquals(0, $id);
        $expected = "UPDATE users SET `name` = 'test' WHERE id = 10 LIMIT 1";
        $this->assertEquals($expected, MockedConnector::$query);

        MockedConnector::$fail = true;
        $this->query->save($data);
    }

}

class DbResource
{

    protected $mode;

    public function __construct($mode = 'first')
    {
        $this->mode = $mode;
    }

    public $num_rows = 1;

    public function fetch_array($const)
    {

        switch ($this->mode) {
            case 'count':
                return array(3);
            case 'first':
            default:
                return array(1, 'fsilva', 'admin');
        }
        
    }

    public function fetch_fields()
    {
        switch ($this->mode) {
            case 'count':
                return  array(
                    (object) array(
                        'name' => 'rows',
                        'table' => null
                    )
                );
            case 'first':
            default:
                return array(
                    (Object) array(
                        'name' => 'id',
                        'table' => 'User'
                    ),
                    (Object) array(
                        'name' => 'name',
                        'table' => 'User'
                    ),
                    (Object) array(
                        'name' => 'group',
                        'table' => 'User'
                    ),
                );

        }
    }
}

/**
 * A mock connector to the query tests.
 */
class MockedConnector extends Connector
{

    public static $query = null;

    public static $fail = false;

    public $affectedRows = 2;

    public $lastInsertId = 30;

    public function escape($value)
    {
        return $value;
    }

    public function execute($sql, $useResults = false )
    {
        self::$query = $sql;
        if (self::$fail) return false;

        if (preg_match('#COUNT\(1\)#', $sql)) {
            return new DbResource('count');
        }

        return new DbResource();
    }

    public function getLastError()
    {
        return "Some error";
    }

}