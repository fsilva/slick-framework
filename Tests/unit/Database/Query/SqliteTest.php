<?php

/**
 * SqliteTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Database\Query;

use Codeception\Util\Stub;
use Slick\Database\Query\Sqlite as Query;
use Slick\Database\Connector\Sqlite as Connector;

/**
 * SqliteTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class SqliteTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Creating the environment for tests
     */
    protected function _before()
    {
        parent::_before();
        $connector = new MockedSqliteConnector();
        $this->query = new Query(array('connector' => $connector));
        MockedSqliteConnector::$fail = false;
    }

    /**
     * Clean up after test.
     */
    protected function _after()
    {
        unset($this->query);
        parent::_after();
    }

    // tests
    
    /**
     * Check the first and all methods
     * @test
     * @expectedException \Slick\Database\Exception\Sql
     */
    public function checkAll()
    {
        $all = $this->query
            ->from('users AS User', array('User.id' => 'number', 'User.name'))
            ->where('name LIKE ?', '%fsilva%')
            ->orWhere('name LIKE ?', '%other name%')
            ->order('name', 'ASC')
            ->join('profiles AS Profile', 'User.profile_id = Profile.id', array('Profile.group'))
            ->limit(5, 3)
            ->all();

        $expected =  "SELECT User.id AS number, User.name, Profile.group FROM users AS User";
        $expected .= " JOIN profiles AS Profile ON User.profile_id = Profile.id";
        $expected .= " WHERE name LIKE '%fsilva%' OR name LIKE '%other name%'";
        $expected .= " ORDER BY name ASC";
        $expected .= " LIMIT 5, 10";

        $this->assertEquals($expected, MockedSqliteConnector::$query);

        $this->assertEquals(5, $this->query->getLimit());
        $this->assertEquals((5 * (3 - 1)), $this->query->getOffset());

        $this->assertEquals(1, count($all));
        $expected = array(
            'id' => 1,
            'name' => 'fsilva',
            'group' => 'admin'
        );
        $this->assertContains($expected, $all);

        MockedSqliteConnector::$fail = true;
        $this->query->all();
    }

    /**
     * Testing save method.
     * @test
     * @expectedException \Slick\Database\Exception\Sql
     */
    public function querySave()
    {
        $data = array('name' => 'test');
        $id = $this->query->from('users')->save($data);
        $this->assertEquals(30, $id);
        $expected = "INSERT INTO users (name) VALUES ('test')";
        $this->assertEquals($expected, MockedSqliteConnector::$query);

        $id = $this->query
            ->from('users')
            ->where('id = ?', 10)
            ->limit(1)
            ->save($data);
        $this->assertEquals(0, $id);
        $expected = "UPDATE users SET name = 'test' WHERE id = 10";
        $this->assertEquals($expected, MockedSqliteConnector::$query);

        MockedSqliteConnector::$fail = true;
        $this->query->save($data);
    }

    /**
     * Testing delete
     * @test
     * @expectedException \Slick\Database\Exception\Sql
     */
    public function queryDelete()
    {
        $rows = $this->query
            ->from('users')
            ->where('users.id = ?', 2)
            ->limit(2)
            ->delete();
        $this->assertEquals(2, $rows);
        $expected =  "DELETE FROM users WHERE users.id = 2";
        $this->assertEquals($expected, MockedSqliteConnector::$query);
        MockedSqliteConnector::$fail = true;
        $this->query->delete();
    }

}

class SqliteResource
{

    protected $mode;

    public function __construct($mode = 'first') {
        $this->mode = $mode;
    }

    public $num_rows = 1;

    public function fetchArray($const) {
        static $hasLeft;
        if (!is_null($hasLeft)) {
            return false;
        }
        $hasLeft = true;
        switch ($this->mode) {
            case 'count':
                return array(
                    'rows' => 3
                );
            case 'first':
            default:
                return array(
                    'id' => 1,
                    'name' => 'fsilva',
                    'group' => 'admin'
                );
        }
        
    }
}

/**
 * A mock connector to the query tests.
 */
class MockedSqliteConnector extends Connector {

    public static $query = null;

    public static $fail = false;

    public $affectedRows = 2;

    public $lastInsertId = 30;

    public function escape($value) {
        return $value;
    }

    public function execute($sql) {
        self::$query = $sql;
        if (self::$fail) return false;

        if (preg_match('#COUNT\(1\)#', $sql)) {
            return new SqliteResource('count');
        }
        return new SqliteResource();
    }

    public function getLastError() {
        return "Some error";
    }

}
