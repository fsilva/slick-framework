<?php

/**
 * MysqlTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Database\Connector;

use Models\User;
use Codeception\Util\Stub;
use Slick\Database\Connector;

/**
 * MysqlTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class MysqlTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Mysql is a Mysql Connector object.
     * @var \Slick\Database\Connector\Mysql
     */
    protected $mysql;

    /**
     * Creates a Mysql Connector object for tests.
     */
    protected function _before()
    {
        parent::_before();
        $this->mysql = new Connector\Mysql(
            array(
                'host' => 'localhost',
                'username' => 'slick_',
                'schema' => 'dummy_test'
            )
        );

        $service = $this->getMock('\MySQLi');

        $this->mysql->service = $service;
        $this->mysql->isConnected = true;
    }

    /**
     * Clears all for next test.
     */
    protected function _after()
    {
        unset($this->mysql);
        parent::_after();
    }

    // tests
    
    /**
     * Check the core implementation for non implemented methods.
     * @test
     * @expectedException \Slick\Database\Exception\Implementation
     */
    public function notImplemented()
    {
        $this->mysql->notImplemented();
    }

    /**
     * Check the initializantion of driver
     * @test
     */
    public function initialization()
    {
        $this->assertInstanceOf(
            '\Slick\Database\Connector',
            $this->mysql->initialize()
        );
        $this->assertInstanceOf(
            '\Slick\Database\Connector\Mysql',
            $this->mysql->initialize()
        );
    }

    /**
     * Testing connect method
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function connect()
    {

        $cnn = new Connector\Mysql();
        $this->assertInstanceOf(
            '\Slick\Database\Connector',
            $cnn->connect()
        );
        
        $this->assertInstanceOf(
            '\Slick\Database\Connector',
            $this->mysql->connect()
        );
        $this->assertInstanceOf(
            '\Slick\Database\Connector',
            $this->mysql->disconnect()
        );
        $this->assertFalse($this->mysql->isConnected);
        $this->mysql->service = null;
        $this->mysql->connect();
    }

    /**
     * Test query method
     * @test
     */
    public function query()
    {
        $this->assertInstanceOf(
            '\Slick\Database\Query\Mysql',
            $this->mysql->query()
        );
    }

    /**
     * Tests the executing query
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function executingQuery()
    {
        $this->assertNull($this->mysql->execute(''));
        $this->mysql->disconnect();
        $this->mysql->execute('');
    }

    /**
     * Checking scape method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function excapeText()
    {
        $this->assertNull($this->mysql->escape(''));
        $this->mysql->disconnect();
        $this->mysql->escape('');
    }

    /**
     * Checking last inserted id method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function lastInsertedId()
    {
        $this->mysql = new Connector\Mysql();
        $this->mysql->connect();
        $this->assertEquals(0, $this->mysql->getLastInsertId(''));
        $this->mysql->disconnect();
        $this->mysql->getLastInsertId('');
    }

    /**
     * Checking affected rows method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function getAffectedRows()
    {
        $this->mysql = new Connector\Mysql();
        $this->mysql->connect();
        $this->assertEquals(0, $this->mysql->getAffectedRows(''));
        $this->mysql->disconnect();
        $this->mysql->getAffectedRows('');
    }

    /**
     * Checking last error method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function getLastError()
    {
        $this->mysql = new Connector\Mysql();
        $this->mysql->connect();
        $this->assertEquals(null, $this->mysql->getLastError(''));
        $this->mysql->disconnect();
        $this->mysql->getLastError('');
    }

    /**
     * Model syncronization method
     * @test
     */
    public function syncModel()
    {
        $user = new User();
        $expected = "CREATE TABLE users (
`id` int(11) NOT NULL AUTO_INCREMENT,
`number` int(11) NOT NULL AUTO_INCREMENT,
`username` varchar(128) DEFAULT NULL,
`password` text,
`age` int(11) DEFAULT NULL,
`active` int(4) DEFAULT NULL,
`birthDate` datetime DEFAULT NULL,
`rank` float DEFAULT NULL,
`type` text,
PRIMARY KEY (`id`),
KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $this->mysql->service = new Service(null, null, null, null);
        $this->assertInstanceOf('\Slick\Database\Connector\Mysql', $this->mysql->sync($user));
        $this->assertEquals($expected, $this->mysql->service->query);

        try {
            $this->mysql->sync($user);
            $this->fail('This should raise an exception here.');
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Database\Exception\sql', $e);
            $this->assertEquals('DROP TABLE IF EXISTS users', $this->mysql->service->query);
        }

        try {
            $this->mysql->sync($user);
            $this->fail('This should raise an exception here.');
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Database\Exception\sql', $e);
            $this->assertEquals($expected, $this->mysql->service->query);
        }
    }

}

/**
 * A mock class to test the sync method
 */
class Service extends \MySQLi
{

    public $query = null;

    protected $current = 0;

    protected $results = array(
        true, true, false, true, false
    );

    public function query($sql)
    {
        $this->query = $sql;
        return $this->results[$this->current++];
    }
}