<?php

/**
 * SqliteTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Database\Connector;

use Models\User;
use Codeception\Util\Stub;
use Slick\Database\Connector;

/**
 * SqliteTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class SqliteTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Sqlite is a Sqlite3 Connector object.
     * 
     * @var \Slick\Database\Connector\Sqlite
     */
    protected $sqlite;

    /**
     * Creates a Sqlite Connector object for tests.
     */
    protected function _before()
    {
        parent::_before();
        $this->sqlite = new Connector\Sqlite();

        $service = new ServiceLite();

        $this->sqlite->service = $service;
        $this->sqlite->isConnected = true;
    }

    /**
     * Clears all for next test.
     */
    protected function _after()
    {
        unset($this->sqlite);
        parent::_after();
    }

    // tests
    
    /**
     * Check the core implementation for non implemented methods.
     * @test
     * @expectedException \Slick\Database\Exception\Implementation
     */
    public function notImplemented()
    {
        $this->sqlite->notImplemented();
    }

    /**
     * Check the initializantion of driver
     * @test
     */
    public function initialization()
    {
        $this->assertInstanceOf('\Slick\Database\Connector', $this->sqlite->initialize());
        $this->assertInstanceOf('\Slick\Database\Connector\Sqlite', $this->sqlite->initialize());
    }

    /**
     * Testing connect method
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function connect()
    {
        $this->sqlite->disconnect();
        $con = $this->sqlite->connect();
        $this->assertInstanceOf('\Slick\Database\Connector', $con);
        $this->assertInstanceOf('\Slick\Database\Connector\Sqlite', $con);
        $this->assertTrue($this->sqlite->isConnected);

        $this->sqlite->disconnect();
        $this->assertFalse($this->sqlite->isConnected);
        $this->sqlite->file = '/_not_existing_folder/database/.test.db';
        $this->connect();
    }

    /**
     * Test query method
     * @test
     */
    public function query()
    {
        $this->assertInstanceOf('\Slick\Database\Query\Sqlite', $this->sqlite->query());
    }

    /**
     * Tests the executing query
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function executingQuery()
    {
        $sql = "SELECT * from somewhere";
        $this->assertFalse($this->sqlite->execute($sql));
        $this->assertEquals($sql, $this->sqlite->service->query);
        $this->assertEquals("query", $this->sqlite->service->method);

        $sql = "update somewhere...";
        $this->assertTrue($this->sqlite->execute($sql));
        $this->assertEquals($sql, $this->sqlite->service->query);
        $this->assertEquals("exec", $this->sqlite->service->method);

        $this->sqlite->disconnect();
        $this->sqlite->execute('');
    }

    /**
     * Checking scape method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function excapeText()
    {
        $this->assertEquals('value_escaped', $this->sqlite->escape('value'));
        $this->sqlite->disconnect();
        $this->sqlite->escape('');
    }

    /**
     * Checking last inserted id method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function lastInsertedId()
    {
        $this->assertEquals(1001, $this->sqlite->getLastInsertId(''));
        $this->sqlite->disconnect();
        $this->sqlite->getLastInsertId('');
    }

    /**
     * Checking affected rows method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function getAffectedRows()
    {
        $this->assertEquals(30, $this->sqlite->getAffectedRows());
        $this->sqlite->disconnect();
        $this->sqlite->getAffectedRows();
    }

    /**
     * Checking last error method.
     * @test
     * @expectedException \Slick\Database\Exception\Service
     */
    public function getLastError()
    {
        $this->assertEquals(
            "I am an happy error message.",
            $this->sqlite->getLastError()
        );
        $this->sqlite->disconnect();
        $this->sqlite->getLastError('');
    }

    /**
     * Model syncronization method
     * @test
     */
    public function syncModel()
    {
        $user = new User();
        $expected = "CREATE TABLE 'users' (
'id' INTEGER PRIMARY KEY AUTOINCREMENT,
'number' INTEGER NOT NULL,
'username' VARCHAR(128) DEFAULT NULL,
'password' TEXT,
'age' INTEGER DEFAULT NULL,
'active' INTEGER DEFAULT NULL,
'birthDate' DATETIME DEFAULT NULL,
'rank' REAL DEFAULT NULL,
'type' TEXT)";

        $this->assertInstanceOf('\Slick\Database\Connector\Sqlite', $this->sqlite->sync($user));
        $this->assertEquals($expected, $this->sqlite->service->query);

        try {
            $this->sqlite->sync($user);
            $this->fail('This should raise an exception here.');
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Database\Exception\sql', $e);
            $this->assertEquals('DROP TABLE IF EXISTS users', $this->sqlite->service->query);
        }

        try {
            $this->sqlite->sync($user);
            $this->fail('This should raise an exception here.');
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Database\Exception\sql', $e);
            $this->assertEquals($expected, $this->sqlite->service->query);
        }
    }

}

/**
 * A mock class to test the sync method
 */
class ServiceLite extends \SQLite3
{

    public function __construct()
    {
        parent::__construct(':memory:');
    }

    public $query = null;

    public $method = null;

    protected $current = 0;

    protected $results = array(
        true, true, false, true, false
    );

    public function query($sql)
    {
        $this->query = $sql;
        $this->method = 'query';
        return false;
    }

    public function exec($sql)
    {
        $this->method = 'exec';
        $this->query = $sql;
        return $this->results[$this->current++];
    }

    public static function escapestring($value)
    {
        return "{$value}_escaped";
    }

    public function lastInsertRowID()
    {
        return 1001;
    }

    public function changes()
    {
        return 30;
    }

    public function lastErrorMsg()
    {
        return "I am an happy error message.";
    }
}