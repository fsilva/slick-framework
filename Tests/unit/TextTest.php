<?php

/**
 * TextTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

use Codeception\Util\Stub;
use Slick\Text;

class TextTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    protected $terms = array(
        'person' => 'people',
        'man' => 'men',
        'user' => 'users',
        'knife' => 'knives',
        'life' => 'lives',
        'ox' => 'oxen',
        'child' => 'children',
        'woman' => 'women',
        'crisis' => 'crises',
    );

    /**
     * Singular conversion tests
     * @test
     */
    public function singular()
    {
        foreach ($this->terms as $singular => $plural) {
            $this->assertEquals($singular, Text::singular($plural));
        }
    }

    /**
     * Singular conversion tests
     * @test
     */
    public function plural()
    {
        foreach ($this->terms as $singular => $plural) {
            $this->assertEquals($plural, Text::plural($singular));
        }
    }

    /**
     * String sanitization test
     * @test
     */
    public function sanitize()
    {
        $expected = "hell\o w\orld\!";
        $args = array('!', 'o');
        $str = "!o";
        $input = "hello world!";
        $this->assertEquals($expected, Text::sanitize($input, $str));
        $this->assertEquals($expected, Text::sanitize($input, $args));
        $this->assertEquals($input, Text::sanitize($input, false));
    }

    /**
     * Unique char test
     * @test
     */
    public function unique()
    {
        $expected = "abcdef";
        $input = "aabbcdeff";
        $this->assertEquals($expected, Text::unique($input));
    }

    /**
     * Returning the index off a char test
     * @test
     */
    public function indexOf()
    {
        $this->assertEquals(2, Text::indexOf('abcd', 'c'));
        $this->assertEquals(-1, Text::indexOf('abcd', 'e'));
    }

    /**
     * Testing delimiter setting
     * @test
     */
    public function delimiter()
    {
        $delimiter = Text::getDelimiter();
        Text::setDelimiter("&");
        $this->assertEquals("&", Text::getDelimiter());
        Text::setDelimiter($delimiter);
    }

    /**
     * Matching strings test.
     * @test
     */
    public function match()
    {
        $expected = array('one');
        $input = "one two three four";
        $this->assertEquals($expected, Text::match($input, 'one'));
    }

    /**
     * Test text truncate
     * @test
     */
    public function truncate()
    {
        $text = "I am an PHP developer!";
        $this->assertEquals($text, Text::truncate($text));
        $expected = "I am an...";
        $this->assertEquals($expected, Text::truncate($text, 10));
        $expected = "I am an";
        $this->assertEquals($expected, Text::truncate($text, 10, ''));
    }

    /**
     * Tests the split in lines 
     * @test
     */
    public function splitInLines()
    {
        $text = "I am an PHP developer!";
        $expected = array(
            'I am an PHP',
            'developer!'
        );
        $this->assertEquals($expected, Text::splitInLines($text, 12));
        $this->assertEquals(array($text), Text::splitInLines($text));
        $this->assertEquals(-1, Text::lastIndexOf(' ', "String_with-no_spaces"));
    }

}