<?php

/**
 * TwigTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Template\Engine
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Template\Engine;

use Codeception\Util\Stub;
use Slick\Template\Engine\Twig as TwigEngine;

/**
 * TwigTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Template\Engine
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class TwigTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT object instance
     * @var \Slick\Template\Engine\Twig
     */
    protected $engine;

    /**
     * Create a new instante of engine for tests.
     */
    protected function _before()
    {
        parent::_before();
        $this->engine = new TwigEngine();
    }

    /**
     * Cleans up the engine at the end of the test.
     */
    protected function _after()
    {
        unset($this->engine);
        parent::_after();
    }

    // tests
    
    /**
     * Checks parse method.
     * @test
     */
    public function parse()
    {
        $source = 'Template/test.html';
        $obj = $this->engine->parse($source);
        $this->assertEquals($source, $this->engine->source);
        $this->assertInstanceOf('\Twig_Environment', $this->engine->twig);
        $this->assertInstanceOf('\Slick\Template\Engine\Twig', $obj);
    }

    /**
     * Check base class implementation.
     * 
     * @test
     * @expectedException \Slick\Template\Exception\Implementation
     */
    public function implementation()
    {
        $this->engine->_Unknown();
    }

    /**
     * Test twig engine process data.
     * @test
     * @expectedException \Slick\Template\Exception\Parser
     */
    public function process()
    {
        $source = 'Template/test.html';
        $data = array('name' => 'test_twig');
        $expected = '<div>test_twig</div>';
        $this->engine->parse($source);
        $this->assertEquals($expected, $this->engine->process($data));

        $this->engine->twig = new MockTwig();
        $this->engine->process($data);
    }

}

/**
 * A mocking TWIG library for tests.
 */
class MockTwig
{

    /**
     * Just throw an exception.
     */
    public function render()
    {
        throw new \Exception("Error Processing Request", 1);
    }
}