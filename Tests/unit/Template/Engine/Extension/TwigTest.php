<?php

/**
 * TwigTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Template\Engine\Extension
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Template\Engine\Extension;

use Slick\Core;
use Slick\Registry;
use Codeception\Util\Stub;
use Slick\Template\Engine\Twig as TwigEngine;

/**
 * TwigTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Template\Engine\Extension
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class TwigTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT object instance
     * @var \Slick\Template\Engine\Twig
     */
    protected $engine;

    /**
     * Create a new instante of engine for tests.
     */
    protected function _before()
    {
        parent::_before();
        $this->engine = new TwigEngine();
    }

    /**
     * Cleans up the engine at the end of the test.
     */
    protected function _after()
    {
        unset($this->engine);
        parent::_after();
    }

    // tests
    
    /**
     * Testing extension globals
     * @test
     */
    public function globals()
    {
        $source = 'Template/base.html';
        $data = array();
        $this->assertEquals(
            Core::$basePath,
            $this->engine->parse($source)->process($data)
        );
    }

    /**
     * Testing extension url output
     * @test
     */
    public function url()
    {
        $expected = Core::$basePath . 'tests/index/8';
        $path = array('controller' => 'tests', 'action' => 'index', 8);
        $source = "Template/url.html";
        $this->assertEquals(
            $expected,
            $this->engine->parse($source)->process(compact('path'))
        );
    }

}