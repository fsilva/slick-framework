<?php

/**
 * TemplateTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Template
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Template;

use Codeception\Util\Stub;
use Slick\Template;

/**
 * TemplateTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Template
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class TemplateTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT instance object.
     * @var Slick\Template
     */
    protected $template;

    /**
     * Create a new template instance for test.
     */
    protected function _before()
    {
        parent::_before();
        $this->template = new Template();
    }

    /**
     * Unsets the SUT template instance
     */
    protected function _after()
    {
        unset($this->template);
        parent::_after();
    }

    // tests
    
    /**
     * Tests Slick\Base implementation
     * @test
     * @expectedException \Slick\Template\Exception\Implementation
     */
    public function implementation()
    {
        $this->template->_UnImplemented();
    }

    /**
     * Test the template initializartion method.
     * @test
     * @expectedException \Slick\Template\Exception\Argument
     */
    public function initialization()
    {
        try {
            $this->template->setType(null);
            $this->fail("This should raise un exception here.");
        } catch(\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Template\Exception\Argument', $e);
        }

        $this->template->setType('twig');
        $this->assertInstanceOf('\Slick\Template\Engine', $this->template->engine);
        $this->assertInstanceOf('\Slick\Template\Engine\Twig', $this->template->engine);
        $this->template->setType('unknown');
    }

    /**
     * Tests the parsing metods
     * @test
     */
    public function parseIO()
    {
        $source = 'Template/test.html';
        $data = array('name' => 'test');
        $expected = '<div>test</div>';
        $obj = $this->template->parse($source);
        $this->assertInstanceOf('\Slick\Template\Engine', $obj);
        $result = $this->template->process($data);
        $this->assertEquals($expected, $result);
    }

}