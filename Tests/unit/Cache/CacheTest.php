<?php

/**
 * CacheTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Cache;

use Codeception\Util\Stub;
use Slick\Cache as Cache;

/**
 * CacheTest case
 * 
 * @package    Slick
 * @subpackage Tests\Unit\Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class CacheTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Tests invalid method on cache object.
     * @expectedException Slick\Cache\Exception\Implementation
     * @test
     */
    public function callInvalidMethod()
    {
        $cache = new Cache();
        $cache->unImplemented();
    }

    /**
     * Tests invalid cache type.
     * @expectedException Slick\Cache\Exception\Argument
     * @test
     */
    public function initializeInvalidCacheType()
    {
        $cache = new Cache(array('type' => 'invalid'));
        $cache->initialize();
    }

    /**
     * Tests invalid cache type.
     * @expectedException Slick\Cache\Exception\Argument
     * @test
     */
    public function unsetCacheType()
    {
        $cache = new Cache();
        $cache->initialize();
    }

    /**
     * Tests the return from cache initialization
     * @test
     */
    public function runCacheInitialization()
    {
        //File type cache
        $cache = new Cache(array('type' => 'file'));
        $this->assertInstanceOf('Slick\Cache\Driver\File', $cache->initialize());

        //Memcached type cache
        $cache = new Cache(array('type' => 'memcached'));
        $this->assertInstanceOf('Slick\Cache\Driver\Memcached', $cache->initialize());
    }

}