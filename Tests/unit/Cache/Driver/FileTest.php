<?php
namespace Cache\Driver;
use Codeception\Util\Stub;
use Slick\Cache\Driver\File as CacheFile;

class FileTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Tests the cache file diver inheritance.
     * @test
     */
    public function checkInheritance() {
        $cache = new CacheFile();
        $this->assertInstanceOf('Slick\Cache\Driver', $cache);
        $this->assertInstanceOf('Slick\Cache\Driver\File', $cache->initialize());
    }

    /**
     * Tests invalid metod on cache File driver object.
     * @expectedException Slick\Cache\Exception\Implementation
     * @test
     */
    public function invalidMethod() {
        $cache = new CacheFile();
        $cache->unImplemented();
    }

    /**
     * Test driver connection
     * Connect is used for the majority of the cache systems.
     * We use it here to chech if we can write files in the file system.
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function connect() {
        if (!$this->_isWritable()) {
            $this->markTestSkipped(
                "The Cache path does not exists or is not writable."
            );
        }

        //Testing normal connection
        $cache = new CacheFile();
        $this->assertInstanceOf('Slick\Cache\Driver\File', $cache->connect());
        $this->assertTrue($cache->isConnected);

        //Disconencting
        $this->assertInstanceOf('Slick\Cache\Driver\File', $cache->disconnect());
        $this->assertFalse($cache->isConnected);

        //testing exception
        $cache->path = '/to/nowhere';
        $cache->connect();
    }

    /**
     * Test the get function
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function get() {
        if (!$this->_isWritable()) {
            $this->markTestSkipped(
                "The Cache path does not exists or is not writable."
            );
        }

        $cache = new CacheFile();
        $cache->connect();
        $value = array('test', array('1' => 'one'));

        //Test default return for empty values.
        $this->assertNull($cache->get('test', null));
        $this->assertTrue($cache->get('test', true));

        //Test value retrive
        $cache->set('test', $value);
        $this->assertEquals($value, $cache->get('test'));
        $cache->erase('test');

        //Test exceptions
        $cache->disconnect();
        $cache->get('test');
    }

    /**
     * Test the set function
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function set() {
        if (!$this->_isWritable()) {
            $this->markTestSkipped(
                "The Cache path does not exists or is not writable."
            );
        }

        $cache = new CacheFile();
        $cache->connect();
        $value = array('test', array('1' => 'one'));

        //Test value retrive
        $cache->set('test', $value);
        $this->assertEquals($value, $cache->get('test'));
        $this->assertTrue(is_file(APP_PATH . '/Tmp/Cache/' . $cache->prefix . 'test'));

        //Test duration
        $cache->set('test', $value, -1);
        $this->assertTrue($cache->get('test', true));
        $cache->erase('test');

        //Test exceptions
        $cache->disconnect();
        $cache->set('test', 'test');
    }

    /**
     * Test the erase function
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function erase() {
        if (!$this->_isWritable()) {
            $this->markTestSkipped(
                "The Cache path does not exists or is not writable."
            );
        }

        $cache = new CacheFile();
        $cache->connect();
        $value = array('test', array('1' => 'one'));

        //Test erase
        $cache->set('test', $value);
        $this->assertEquals($value, $cache->get('test'));
        $cache->erase('test');
        $this->assertTrue($cache->get('test', true));
        $this->assertFalse(is_file(APP_PATH . '/Tmp/Cache/' . $cache->prefix . 'test'));

        //Test exception
        $cache->disconnect();
        $cache->erase('test');
    }

    /**
     * Check whenever the default path is writable.
     *
     * @return boolean True if is writable, false otherwise.
     */
    protected function _isWritable() {
        return is_writable(APP_PATH . '/Tmp/Cache');
    }
}