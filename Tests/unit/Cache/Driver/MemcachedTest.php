<?php

/**
 * MemcachedTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Cache\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Cache\Driver;

use Codeception\Util\Stub;
use Slick\Cache\Driver\Memcached as MemCache;

/**
 * Memcached driver Test
 * 
 * @package    Slick
 * @subpackage Tests\Unit\Cache\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class MemcachedTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    // tests
    
    /**
     * Tests the cache memcached diver inheritance.
     * @test
     */
    public function checkInheritance()
    {
        $cache = new MemCache();
        $this->assertInstanceOf('Slick\Cache\Driver', $cache);
        $this->assertInstanceOf('Slick\Cache\Driver\Memcached', $cache->initialize());
    }

    /**
     * Tests invalid metod on cache Memcached driver object.
     * @expectedException Slick\Cache\Exception\Implementation
     * @test
     */
    public function callInvalidMethod()
    {
        $cache = new MemCache();
        $cache->unImplemented();
    }

    /**
     * Test driver connection
     * Connect is used for the majority of the cache systems.
     * We use it here to chech if we can write files in the file system.
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function connectToDriver()
    {
        if (!class_exists('\Memcache')) {
            $this->markTestSkipped(
                "The the PHP Memcache extension is not installed."
            );
        }

        //Testing normal connection
        $cache = new MemCache();
        $this->assertInstanceOf('Slick\Cache\Driver\Memcached', $cache->connect());
        $this->assertTrue($cache->isConnected);

        //Disconencting
        $this->assertInstanceOf('Slick\Cache\Driver\Memcached', $cache->disconnect());
        $this->assertFalse($cache->isConnected);

        //testing exception
        $cache->port = '66666';
        $cache->connect();
    }

    /**
     * Test the get function
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function getValues()
    {
        if (!class_exists('\Memcache')) {
            $this->markTestSkipped(
                "The the PHP Memcache extension is not installed."
            );
        }

        $cache = new MemCache();
        $cache->connect();
        $value = array('test', array('1' => 'one'));

        //Test default return for empty values.
        $this->assertNull($cache->get('test', null));
        $this->assertTrue($cache->get('test', true));

        //Test value retrive
        $cache->set('test', $value);
        $this->assertEquals($value, $cache->get('test'));
        $cache->erase('test');

        //Test exceptions
        $cache->disconnect();
        $cache->get('test');
    }

    /**
     * Test the set function
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function setValues()
    {
        if (!class_exists('\Memcache')) {
            $this->markTestSkipped(
                "The the PHP Memcache extension is not installed."
            );
        }

        $cache = new MemCache();
        $cache->connect();
        $value = array('test', array('1' => 'one'));

        //Test value retrive
        $cache->set('test', $value);
        $this->assertEquals($value, $cache->get('test'));

        //Test duration
        $cache->set('test', $value, -1);
        $this->assertTrue($cache->get('test', true));
        $cache->erase('test');

        //Test exceptions
        $cache->disconnect();
        $cache->set('test', 'test');
    }

    /**
     * Test the erase function
     * @test
     * @expectedException Slick\Cache\Exception\Service
     */
    public function eraseValues()
    {
        if (!class_exists('\Memcache')) {
            $this->markTestSkipped(
                "The the PHP Memcache extension is not installed."
            );
        }

        $cache = new MemCache();
        $cache->connect();
        $value = array('test', array('1' => 'one'));

        //Test erase
        $cache->set('test', $value);
        $this->assertEquals($value, $cache->get('test'));
        $cache->erase('test');
        $this->assertTrue($cache->get('test', true));

        //Test exception
        $cache->disconnect();
        $cache->erase('test');
    }

}