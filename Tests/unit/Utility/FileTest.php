<?php

/**
 * FileTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Utility;

use Slick\Utility\File;
use Slick\Utility\Folder;
use Codeception\Util\Stub;

/**
 * FileTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class FileTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT instance
     * @var \Slick\Utility\File
     */
    protected $file = null;

    /**
     * Creates a new file object for test.
     */
    protected function _before()
    {
        parent::_before();
        $this->file = new File(
            array(
                'folder' => new Folder(array('path' => APP_PATH . '/Tmp')),
                'name' => 'test.txt'
            )
        );
    }

    /**
     * Unsets the file and deletes the it on file system.
     */
    protected function _after()
    {
        @unlink(APP_PATH . '/Tmp/test.txt');
        unset($this->file);
        parent::_after();
    }

    // tests
    
    /**
     * Test the Slick base implementation
     * @test
     * @expectedException \Slick\Utility\Exception\Implementation
     */
    public function implementation()
    {
        $this->file->_unimplemented();
    }

    /**
     * Test read contents
     * @test
     * @expectedException \Slick\Utility\Exception\Permissions
     */
    public function read()
    {
        $contents = "Some text";
        $bites = file_put_contents(APP_PATH . '/Tmp/read.txt', $contents);
        
        $file = new File(array('folder' => APP_PATH . '/Tmp', 'name' => 'read.txt'));
        $this->assertEquals($contents, $file->read());
        unlink(APP_PATH . '/Tmp/read.txt');

        $file = new File(array('folder' => '/root', 'name' => 'write.txt'));
        $file->read();
    }

    /**
     * Testing content write to file.
     * @test
     * @expectedException \Slick\Utility\Exception\Permissions
     */
    public function write()
    {
        $contents = "Some text";
        $file = new File(array('folder' => APP_PATH . '/Tmp', 'name' => 'write.txt'));
        $resp = $file->write($contents);
        $this->assertInstanceOf('\Slick\Utility\File', $resp);
        $this->assertEquals($resp, $file);

        $saved = file_get_contents(APP_PATH . '/Tmp/write.txt');
        $this->assertEquals($contents, $saved);

        unlink(APP_PATH . '/Tmp/write.txt');

        $file = new File(array('folder' => '/root', 'name' => 'write.txt'));
        $file->write($contents);
    }

    /**
     * Test file info refresh
     * @test
     */
    public function refresh()
    {
        $file = new File(array('folder' => APP_PATH . '/Tmp', 'name' => 'refresh.txt'));
        $this->assertFalse($file->exists);
        file_put_contents(APP_PATH . '/Tmp/refresh.txt', 'Test refresh');
        $resp = $file->refresh();
        $this->assertInstanceOf('\Slick\Utility\File', $resp);
        $this->assertTrue($file->exists);
        $fTime = filemtime(APP_PATH . '/Tmp/refresh.txt');
        $this->assertEquals($fTime, $file->lastModified->getTimestamp());
        unlink(APP_PATH . '/Tmp/refresh.txt');
    }

    /**
     * Tests the append method
     * @test
     * @expectedException \Slick\Utility\Exception\Permissions
     */
    public function append()
    {
        $content = "Initial content\n";
        $append = "Appended text.";
        $file = new File(array('folder' => APP_PATH . '/Tmp', 'name' => 'append.txt'));
        $file->write($content);
        $this->assertEquals($content, $file->read());
        $file->append($append);
        $this->assertEquals($content.$append, $file->read());
        unlink(APP_PATH . '/Tmp/append.txt');

        $file = new File(array('folder' => '/root', 'name' => 'append.txt'));
        $file->append($content);
    }

    /**
     * Tests file delete
     * @test
     * @expectedException \Slick\Utility\Exception\Permissions
     */
    public function delete()
    {
        $this->file->append("One more line")->refresh();
        $this->assertTrue($this->file->exists);
        $this->file->delete()->refresh();
        $this->assertFalse(file_exists($this->file->fullPath()));
        $this->assertFalse($this->file->exists);
        $file = new File(array('folder' => '/root', 'name' => 'delete.txt'));
        $file->delete();
    }

}