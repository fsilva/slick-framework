<?php

/**
 * FileListTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 */

namespace Utility;

use Slick\Utility\File;
use Codeception\Util\Stub;
use Slick\Utility\FileList;

/**
 * FileListTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class FileListTest extends \Codeception\TestCase\Test
{
   
   /**
     * File List test
     * @test
     */
    public function addFiles()
    {
        $coll = new FileList();
        $file = new File(array('name' => 'test.txt'));
        $rep = $coll->add($file);
        $this->assertEquals($rep, $coll);
        $this->assertEquals(1, count($coll));
        $this->assertEquals($file, $coll->findByName("test.txt"));
        $this->assertNull($coll->findByName('unknown.txt'));
    }

}