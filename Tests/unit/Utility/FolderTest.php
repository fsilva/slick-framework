<?php

/**
 * FolderTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Utility;

use Slick\Utility\File;
use Slick\Utility\Folder;
use Codeception\Util\Stub;

/**
 * FolderTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class FolderTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT instance
     * @var Slick\Utility\Folder
     */
    protected $folder = null;

    /**
     * Creates a new instance for test.
     */
    protected function _before()
    {
        parent::_before();
        $this->folder = new Folder(array(
            'path' => APP_PATH . '/Tmp/Utility/Test'
        ));
    }

    /**
     * Unsets the folder after each test.
     */
    protected function _after()
    {
        @rmdir(APP_PATH . '/Tmp/Utility/Test');
        @rmdir(APP_PATH . '/Tmp/Utility');
        unset($this->folder);
        parent::_after();
    }

    // tests
    
    /**
     * Test the Slick base implementation
     * @test
     * @expectedException \Slick\Utility\Exception\Implementation
     */
    public function implementation()
    {
        //Cheching defaults
        $this->assertTrue($this->folder->exists);
        $this->assertTrue($this->folder->writable);
        $this->assertTrue($this->folder->isWritable());
        $this->folder->_unimplemented();
    }

    /**
     * Check constructor exceptions
     * @test
     * @expectedException \Slick\Utility\Exception\Permissions
     */
    public function construction()
    {
        try {
            $folder = new Folder();
            $this->fail("This should raise an Exception here.");
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Utility\Exception\Argument', $e);
        }

        $folder = new Folder(array('path' => '/root/test'));
    }

    /**
     * Test the usage of the __toString() method.
     * @test
     */
    public function stringify()
    {
        $this->assertEquals(APP_PATH . '/Tmp/Utility/Test', (string) $this->folder);
    }

    /**
     * Test object state for existing folders
     * @test
     */
    public function existingFolder()
    {
        $this->folder = new Folder(array('path' => APP_PATH . '/Tmp'));
        $this->assertTrue($this->folder->exists);
        $this->assertTrue($this->folder->isWritable());
    }

    /**
     * Test the adding files to a folder
     * @test
     */
    public function addingFiles()
    {
        $file_list =  $this->folder->files;
        $this->assertInstanceOf('\Slick\Utility\FileList', $file_list);
        $this->assertEquals(0, count($file_list));

        $resp = $this->folder->addFile("test_1.txt");
        $this->assertInstanceOf('\Slick\Utility\Folder', $resp);
        $file_list =  $this->folder->files;
        $this->assertEquals(1, count($file_list));
        $file = $file_list->findByName("test_1.txt");
        $this->assertInstanceOf('\Slick\Utility\File', $file);
        $this->assertEquals($this->folder, $file->folder);
    }

    /**
     * Test the exceptio thronw when adding a file witch allready exists in directory.
     * @test
     * @expectedException \Slick\Utility\Exception\FileExists
     */
    public function addExistingFile()
    {
        $file = new File(array('name' => "another_text.txt"));
        $this->folder->addFile($file);
        $this->assertTrue($this->folder->hasFile($file));
        $this->folder->addFile("another_text.txt");
    }

    /**
     * Test file dete operation
     * @test
     */
    public function deleteFile()
    {
        $file = new File(array('name' => "another_text.txt"));
        $this->folder->addFile($file);
        $file->write('Some content');
        $file_list =  $this->folder->files;
        $this->assertEquals(1, count($file_list));

        $this->folder->deleteFile($file);
        $file_list =  $this->folder->files;
        $this->assertEquals(0, count($file_list));
        $this->assertFalse($this->folder->hasFile($file));
    }

    /**
     * Checking folder delete operation
     * @test
     */
    public function delete()
    {
        $file = new File(array('name' => "another_test.txt"));

        $this->folder->addFile($file);
        $this->folder->delete();
        $this->assertFalse(is_dir(APP_PATH . '/Tmp/Utility/Test'));
    }

    /**
     * Tests the add folder method
     * @test
     */
    public function addFolder()
    {
        $fld = $this->folder->addFolder("test");
        $this->assertInstanceOf('\Slick\Utility\Folder', $fld);
        $fld->delete();
    }

}