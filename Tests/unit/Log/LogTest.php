<?php
namespace Log;

use Slick\Log;
use Codeception\Util\Stub;

class LogTest extends \Codeception\TestCase\Test
{

    /**
     * The SUT object
     * @var \Slick\Log
     */
    protected $_log = null;

    /**
     * The codeceptibn tester.
     * @var \CodeGuy
     */
    protected $codeGuy;

    /**
     * Crate a new Log object.
     */
    protected function _before()
    {
        $this->_log = new Log();
    }

    /**
     * Clean um properties after test.
     */
    protected function _after()
    {
        unset($this->log);
        $this->codeGuy->deleteDir(APP_PATH.'/Tmp/Logs/');
    }

    // tests
    
    /**
     * Check default values on log.
     * @test
     */
    public function checkDefaultValues()
    {
        $this->assertEquals('general', $this->_log->defaultLogger);
        $this->assertEquals(
            APP_PATH . '/Tmp/Logs',
            $this->_log->getLogFilePath()
        );
    }

    /**
     * Log message using a default logger.
     * @test
     */
    public function logMessage()
    {
        $logger =  $this->_log->getLogger();
        $this->assertInstanceOf('Monolog\Logger', $logger);
        $this->assertSame($logger, $this->_log->getLogger('general'));
        $logger->addInfo("Hello world!");
        $this->codeGuy->openFile(APP_PATH.'/Tmp/Logs/debug.log');
        $this->codeGuy->seeInThisFile("Hello world!");
        $this->codeGuy->seeInThisFile("general.INFO");
        $logger->addError("This should happen.");
        $this->codeGuy->dontSeeInThisFile("general.ERROR");
        $this->codeGuy->openFile(APP_PATH.'/Tmp/Logs/error.log');
        $this->codeGuy->seeInThisFile("This should happen.");
        $this->codeGuy->seeInThisFile("general.ERROR");
        $log = Log::logger('test');
        $this->assertEquals('test', $log->getName());
        $this->assertInstanceOf('Monolog\Logger', $log);
    }

}