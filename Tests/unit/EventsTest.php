<?php

/**
 * EventsTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */


use Codeception\Util\Stub;
use Slick\Events;

/**
 * EventsTest
 * 
 * Test case for class Slick\Events
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class EventsTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Stores a callback result;
     * @var string
     */
    public static $result = null;

    /**
     * Testing add callback and fire event.
     * @test
     */
    public function registerAddFireRemove()
    {
        $callback = function($param){
            EventsTest::$result = $param;
        };
        Events::add('myEvent', $callback);
        Events::fire('myEvent', array('Some value'));
        $this->assertEquals('Some value', EventsTest::$result);

        Events::remove('myEvent', $callback);
        Events::fire('myEvent', array('Other value'));
        $this->assertEquals('Some value', EventsTest::$result);
    }

}