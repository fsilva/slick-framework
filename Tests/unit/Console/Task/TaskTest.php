<?php

/**
 * TaskTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Console\Task;

use Codeception\Util\Stub;
use Slick\Console;
use Slick\Console\Colors;
use Slick\Console\Task\ShellList;

/**
 * TaskTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console\Task
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class TaskTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Console instance 
     * @var Slick\Console
     */
    protected $_console = null;

    /**
     * The shell obiect
     * @var Slick\Console\Shell\DefaultShell
     */
    protected $_shell = null;

    /**
     * The Task obiect
     * @var Slick\Console\Task\ShellList
     */
    protected $_task = null;

    /**
     * Setsup the SUT instance
     */
    protected function _before()
    {
        parent::_before();
        $this->_console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'DefaultShell',
                    '-q', '-c0'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
        $this->_shell = $this->_console->getShell();
        $this->_task = new ShellList(array('shell' => $this->_shell));
    }

    /**
     * Cleans up after every test.
     */
    protected function _after()
    {
        $this->_console->setQuiet(true);
        $this->_task = null;
        $this->_shell = null;
        $this->_console = null;
        parent::_after();
    }

    // tests
    
    /**
     * Test shell ouput
     * @test
     */
    public function ouputFromTask()
    {
        $this->_console->setQuiet(false);
        ob_start();
            $expected = "Test output";
            $obj = $this->_task->out($expected);
            $output = ob_get_contents();
            ob_clean();
            $this->_task->bLine();
            $newoutput = ob_get_contents();
        ob_end_clean();
        $this->assertEquals("{$expected}\n", $output);
        $this->assertEquals("\n", $newoutput);
        $this->assertInstanceOf('\Slick\Console\Task', $obj);
        $this->_console->setQuiet(true);
    }

    /**
     * Tests the log system.
     * @test
     */
    public function ouputLogMessages()
    {
        $obj = $this->_task->log('Test');
        $this->assertInstanceOf('\Slick\Console\Task', $obj);
        $this->assertInstanceOf('\Slick\Console\Task\ShellList', $obj);
        $obj = $this->_task->info('Test');
        $this->assertInstanceOf('\Slick\Console\Task', $obj);
        $obj = $this->_task->warning('Test');
        $this->assertInstanceOf('\Slick\Console\Task', $obj);
        $obj = $this->_task->debug('Test');
        $this->assertInstanceOf('\Slick\Console\Task', $obj);
        $obj = $this->_task->error('Test');
        $this->assertInstanceOf('\Slick\Console\Task', $obj);
    }

}