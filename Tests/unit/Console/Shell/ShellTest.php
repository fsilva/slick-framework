<?php

/**
 * ConsoleTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Console\Shell;

use Codeception\Util\Stub;
use Slick\Console;
use Slick\Console\Colors;

/**
 * Test case for class Slick\Configuration
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ShellTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Console instance 
     * @var Slick\Console
     */
    protected $_console = null;

    /**
     * The SUT shell obiect
     * @var Slick\Console\Shell\DefaultShell
     */
    protected $_shell = null;

    protected $_shellOutput = <<<output
 Usage: slick «SHELL» [ARGS]... [OPTIONS]...

Available Shells:
------------------
 Create

Options for all shells:
------------------------
 -q, --quiet        Runs console in quiet mode. No output.
 -v, --verbose      Outputs all log messages.
 -c, --colors       Enables ANSI colors in output.
 -p, --plugin=<name>    Tells console to use the plugin path
     -p<name>         especified by the provided <name>.


output;

    /**
     * Setsup the SUT instance
     */
    protected function _before()
    {
        parent::_before();
        $this->_console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'DefaultShell',
                    '-q', '-c0'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
        $this->_shell = $this->_console->getShell();
    }

    /**
     * Cleans up after every test.
     */
    protected function _after()
    {
        $this->_console->setQuiet(true);
        $this->_shell = null;
        $this->_console = null;
        parent::_after();
    }

    // tests
    
    /**
     * Test shell preparation
     * @test
     */
    public function initializeShell()
    {
        $this->_shell->prepare();
        $this->assertEquals('Slick Console', $this->_shell->getName());
    }

    /**
     * Test shell ouput
     * @test
     */
    public function verifyShellOuput()
    {
        $this->_console->setQuiet(false);
        ob_start();
            $expected = "Test output";
            $obj = $this->_shell->out($expected);
            $output = ob_get_contents();
            ob_clean();
            $this->_shell->bLine();
            $newoutput = ob_get_contents();
        ob_end_clean();
        $this->assertEquals("{$expected}\n", $output);
        $this->assertEquals("\n", $newoutput);
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
        $this->_console->setQuiet(true);
    }

    /**
     * Test the section header ouput.
     * @test
     */
    public function verifySectionHeader()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(true);
        ob_start();
            $expected = Colors::set("Text output\n------------", 'light_gray');
            $obj = $this->_shell->section("Text output");
            $output = ob_get_contents();
            ob_clean();
        ob_end_clean();
        $this->assertEquals("{$expected}\n", $output);
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
        $this->_console->setQuiet(true);
        $this->_console->setColors(false);
    }

    /**
     * Tests the log system.
     * @test
     */
    public function logFromShell()
    {
        $obj = $this->_shell->log('Test');
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
        $this->assertInstanceOf('\Slick\Console\Shell\DefaultShell', $obj);
        $obj = $this->_shell->info('Test');
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
        $obj = $this->_shell->warning('Test');
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
        $obj = $this->_shell->debug('Test');
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
        $obj = $this->_shell->error('Test');
        $this->assertInstanceOf('\Slick\Console\Shell', $obj);
    }

    /**
     * Test the main output
     * @test
     */
    public function runMainMethod() 
    {
        $this->_console->setQuiet(false);
        $this->_shell->prepare();
        ob_start();
            $this->_shell->main();
            $output = ob_get_contents();
        ob_end_clean();
        $this->_console->setQuiet(true);
        $this->_console->setColors(true);
        $this->assertTrue((boolean) strstr($output, 'Create'));
        $this->assertTrue((boolean) strstr($output, 'Available Shells'));
        $this->_shell->main();
        $this->_console->setColors(false);
    }

}