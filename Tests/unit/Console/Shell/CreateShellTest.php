<?php

/**
 * CreateShellTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Console\Shell;

use Codeception\Util\Stub;
use Slick\Console;
use Slick\Console\Colors;

/**
 * CreateShellTest
 *
 * Test case for class Slick\Configuration
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class CreateShellTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Console instance 
     * @var Slick\Console
     */
    protected $_console = null;

    /**
     * The SUT shell obiect
     * @var Slick\Console\Shell\CreateShell
     */
    protected $_shell = null;

    /**
     * Setsup the SUT instance
     */
    protected function _before()
    {
        parent::_before();
        $this->_console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'create',
                    '-q', '-c0'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
        $this->_shell = $this->_console->getShell();
        $this->_shell->setCreateApp(
            new MockCreateApp(array('shell' => $this->_shell))
        );
        $this->_shell->setCreateHelp(
            new MockCreateHelp(array('shell' => $this->_shell))
        );
    }

    /**
     * Cleans up after every test.
     */
    protected function _after()
    {
        $this->_console->setQuiet(true);
        $this->_shell = null;
        $this->_console = null;
        parent::_after();
    }

    // tests

    /**
     * Test the default behavior
     * @test
     */
    public function checkDefaultBehavior()
    {
        $this->_console->setQuiet(false);
        ob_start();
            $this->_shell->main();
            $output = ob_get_contents();
        ob_end_clean();
        $this->assertEquals("Create help!\n", $output);
        $this->_console->setQuiet(true);
    }

    /**
     * Test the create app behavior
     * @test
     */
    public function runCreateAppTask()
    {
        $this->_console->setQuiet(false);
        $args = $this->_console->args;
        $this->_console->args = array_merge($args, array(1 => 'app'));
        ob_start();
            $this->_shell->main();
            $output = ob_get_contents();
        ob_end_clean();
        $this->assertEquals("Create app!\n", $output);
        $this->_console->setQuiet(true);
    }

    /**
     * Tests the shell preparation method.
     * @test
     */
    public function doShellPreparation()
    {
        $this->_shell->prepare();
        $this->assertInstanceOf(
            '\Slick\Console\Task\CreateApp',
            $this->_shell->createApp
        );
        $this->assertInstanceOf(
            '\Slick\Console\Task\CreateHelp',
            $this->_shell->createHelp
        );
    }
}

/**
 * A mock create help task
 */
class MockCreateHelp extends Console\Task
{
    public function run($args = array())
    {
        $this->out("Create help!");
    }
}

/**
 * A mock create app task
 */
class MockCreateApp extends Console\Task
{
    public function run($args = array())
    {
        $this->out("Create app!");
    }
}
