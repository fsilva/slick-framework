<?php

/**
 * ConsoleTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Console;

use Codeception\Util\Stub;
use Slick\Console as Console;
use Slick\Console\Colors as Colors;

/**
 * ConsoleTest
 * 
 * Test case for class Slick\Console
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ConsoleTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The system under test. 
     * @var \Slick\Console
     */
    protected $_console = null;

    /**
     * Setsup the SUT instance
     */
    protected function _before()
    {
        parent::_before();
        $this->_console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'DefaultShell',
                    '-q', '--verbose', '-w'.APP_PATH,
                    '--flag=something', '-v',
                    '-fOther_Thing', '-c1'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
    }

    /**
     * Cleans up after every test.
     */
    protected function _after()
    {
        $this->_console->setQuiet(true);
        $this->_console = null;
        parent::_after();
    }

    // tests
    
    /**
     * Test base implementations
     * @test
     * @expectedException Slick\Console\Exception\Implementation
     */
    public function checkImplementation()
    {
        $this->assertEquals(APP_PATH, Console::$workingPath);
        $this->assertTrue($this->_console->verbose);
        $this->assertTrue($this->_console->quiet);
        $expected = array('DefaultShell');
        $this->assertEquals($expected, $this->_console->args);
        $flags = $this->_console->flags;
        $this->assertContains('fOther_Thing', $flags);
        $this->assertTrue(array_key_exists('flag', $flags));
        $this->assertTrue(array_key_exists('w', $flags));
        $this->assertTrue(array_key_exists('v', $flags));
        $this->_console->unimplemented();
    }

    /**
     * Tests for log system
     * @test
     */
    public function logMessages()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        ob_start();
            $this->_console->log("Info");
            $output = ob_get_contents();
            ob_clean();
            $this->_console->setColors(true);
            $this->_console->log("Error log", 'debug');
            $colorPutput = ob_get_contents();
        ob_end_clean();
        $now = strftime('%Y-%m-%d %T');
        $expected = "\n[{$now}] INFO: Info\n";
        $this->assertEquals($expected, $output);
        $levels = $this->_console->logLevels;
        $ts = Colors::set("[{$now}] DEBUG: ", $levels['debug']);
        $body = Colors::set("Error log", 'light_gray');
        $expected = "\n{$ts}{$body}\n";
        $this->assertEquals($expected, $colorPutput);
    }

    /**
     * Checks the color 
     * @test
     */
    public function doColorReplace()
    {
        $txt = Colors::replace('Hello world!', 'Hello', 'red');
        $color = Colors::set('Hello', 'red');
        $this->assertEquals($color.' world!', $txt);
    }

    /**
     * Checks the color 
     * @test
     */
    public function outputMultipleLines()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        $txt = "Hello world, this is a very long string and therefore should be printed in two or " .
            "more separate lines. This usefull to ouput text without worring if it will fit the " .
            "console current with an can be ajustable to all the console window sizes.";
        ob_start();
            $this->_console->out($txt, true);
            $output = ob_get_contents();
        ob_end_clean();
        $this->assertGreaterThan(2, explode("\n", $output));
    }

    /**
     * Test the hability do print out with out do a new line at the end.
     * @test
     */
    public function outputInlineMessage()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        $expected = "Testing core ... Done!\n";
        ob_start();
            $this->_console->out('Testing core ... ', false, false)
                ->out('Done!');
            $output = ob_get_contents();
        ob_end_clean();
        $this->assertEquals($expected, $output);
        $this->_console->setQuiet(true);
    }

    /**
     * Tests info log ouput
     * @test
     */
    public function logInfoMessage()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        ob_start();
            $obj = $this->_console->info("Some info");
            $output = ob_get_contents();
        ob_end_clean();
        $now = strftime('%Y-%m-%d %T');
        $expected = "\n[{$now}] INFO: Some info\n";
        $this->assertEquals($expected, $output);
        $this->assertInstanceOf('Slick\Console', $obj);
    }

    /**
     * Tests info log ouput
     * @test
     */
    public function logWarningMessage()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        ob_start();
            $obj = $this->_console->warning("Some warning");
            $output = ob_get_contents();
        ob_end_clean();
        $now = strftime('%Y-%m-%d %T');
        $expected = "\n[{$now}] WARNING: Some warning\n";
        $this->assertEquals($expected, $output);
        $this->assertInstanceOf('Slick\Console', $obj);
        ;
    }

    /**
     * Tests info log ouput
     * @test
     */
    public function logDebugMessage()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        ob_start();
            $obj = $this->_console->debug("Some debug");
            $output = ob_get_contents();
        ob_end_clean();
        $now = strftime('%Y-%m-%d %T');
        $expected = "\n[{$now}] DEBUG: Some debug\n";
        $this->assertEquals($expected, $output);
        $this->assertInstanceOf('Slick\Console', $obj);
    }

    /**
     * Tests info log ouput
     * @test
     */
    public function logErrorMessage()
    {
        $this->_console->setQuiet(false);
        $this->_console->setColors(false);
        ob_start();
            $obj = $this->_console->error("Some error");
            $output = ob_get_contents();
        ob_end_clean();
        $now = strftime('%Y-%m-%d %T');
        $expected = "\n[{$now}] ERROR: Some error\n";
        $this->assertEquals($expected, $output);
        $this->assertInstanceOf('Slick\Console', $obj);
    }

    /**
     * Tests the call to an app shell
     * @test
     */
    public function callAppShell()
    {
        $console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'TestShell',
                    '-q'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
        $this->assertInstanceOf('Console\Shell\TestShell', $console->shell);
    }

    /**
     * Tests the call to an app shell
     * @test
     */
    public function callPluginShell()
    {
        $console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'PluginShell',
                    '-q', '--plugin=Test'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
        $this->assertInstanceOf(
            'Test\Console\Shell\PluginShell',
            $console->shell
        );
    }
    
    /**
     * Tests the error ouput when the typed shell isn't found
     * @test
     */
    public function callUnknownShell()
    {
        ob_start();
        $console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'Unknown'
                ),
                'core' => FRAMEWORK_APTH,
                'colors' => false
            )
        );
        $output = ob_get_contents();
        ob_end_clean();
        $this->assertFalse(is_null($output));
        $console->quiet = true;
    }

    /**
     * Testing dispatch event
     * @test
     */
    public function dispatchAShell()
    {
        $console = new Console(
            array(
                'args' => array(
                    "--working=".APP_PATH, 'TestShell',
                    '-q', '-c'
                ),
                'core' => FRAMEWORK_APTH
            )
        );
        $console->dispatch();
        $this->assertTrue($console->shell->runMain);
    }
}