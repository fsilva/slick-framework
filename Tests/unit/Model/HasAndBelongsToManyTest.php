<?php

/**
 * HasAndBelongsToMany relation test case
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 * @copyright 2013 Filipe Silva
 * @license Apache License, Version 2.0 (the "License")
 * @since Version 0.1.0
 */

namespace Model;

use Codeception\Util\Stub;
use Slick\Model\Relation\HasAndBelongsToMany;

/**
 * HasAndBelongsToMany
 *
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 */
class HasAndBelongsToManyTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Relation definition tests.
     * @test
     */
    public function checkDefinition()
    {
        $post = new \Models\Post();
        $relation = new HasAndBelongsToMany($post, 'Tag');
        $this->assertEquals('posts_tags', $relation->joinTable);
        $this->assertEquals('post_id', $relation->foreignKey);
        $this->assertTrue($relation->dependent);
        $this->assertInstanceOf('\Models\Tag', $relation->related);
        $this->assertSame($post, $relation->model);
        $this->assertEquals('select', $relation->queryType);
        $this->assertEquals('tag_id', $relation->relatedForeignKey);

        $tag = new \Models\Tag();
        $relation = new HasAndBelongsToMany($tag, 'Post');
        $this->assertEquals('posts_tags', $relation->joinTable);
    }

    /**
     * Check model relations
     * 
     * @test
     */
    public function retrieveRelations()
    {
        $node = new \Models\Node();

        $relations = $node->getRelations();
        $this->assertTrue(isset($relations['tags']));
        $relation = $relations['tags'];
        $this->assertInstanceOf(
            '\Slick\Model\Relation\HasAndBelongsToMany',
            $relation
        );
        $this->assertEquals('article_id', $relation->foreignKey);
        $this->assertEquals('article_tags', $relation->joinTable);
        $this->assertFalse($relation->dependent);
        $this->assertEquals('tag', $relation->relatedForeignKey);
    }

    /**
     * Query testing
     * @test
     */
    public function queryTheModel()
    {
        $posts = \Models\Post::all();
        $first = $posts->first();
        $this->assertInstanceOf('\Slick\Model\DataSet', $first->tags);
    }

    /**
     * Saving related data
     * @test
     */
    public function saveRelatedData()
    {
        \Models\PostsConnector::$lastQuery = array();
        $expected = array(
            0 => "UPDATE posts SET `title` = 'Test post' WHERE id = 1 ",
            1 => "DELETE FROM posts_tags WHERE post_id = 1",
            2 => "INSERT INTO posts_tags (post_id, tag_id) VALUES ". 
                "(1, 3), (1, 4), (1, 1)",
        );

        $post = new \Models\Post(
            array(
                'id' => 1,
                'title' => "Test post"
            )
        );
        $post->tags->add(
            new \Models\Tag(
                array(
                    'id' => 1,
                    'description' => 'PHP'
                )
            )
        );
        $post->save();
        $this->assertEquals($expected, \Models\PostsConnector::$lastQuery);

        \Models\PostsConnector::$lastQuery = array();
        $expected = array(
            0 => "UPDATE posts SET `title` = 'Test post' WHERE id = 1 ",
        );
        $post = new \Models\Post(
            array(
                'id' => 1,
                'title' => "Test post"
            )
        );
        $post->tags = new \Slick\Model\DataSet();
        $post->save();
        $this->assertEquals($expected, \Models\PostsConnector::$lastQuery);
    }

}