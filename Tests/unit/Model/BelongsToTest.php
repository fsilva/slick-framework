<?php

/**
 * BelongsTo relation test case
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 * @copyright 2013 Filipe Silva
 * @license Apache License, Version 2.0 (the "License")
 * @since Version 0.1.0
 */

namespace Model;
use Codeception\Util\Stub;
use Slick\Model\Relation\BelongsTo;

/**
 * BelongsTo
 *
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 */
class BelongsToTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $_codeGuy;

    /**
     * Relation data
     * @test
     */
    public function checkDefinition()
    {
        $sun = new Sun();
        $relation = new BelongsTo($sun, 'Sky', array());
        $this->assertEquals('sky_id', $relation->foreignKey);
        $this->assertEquals('skies', $relation->foreignKeyTable);
        $this->assertEquals($sun, $relation->model);
        $this->assertEquals('join', $relation->queryType);
        $this->assertInstanceOf('\Models\Sky', $relation->related);
        $this->assertTrue($relation->dependent);

        $cloud = new Cloud();
        $relationCustom = new BelongsTo(
            $cloud,
            'Sky',
            array(
                'foreignKey' => 'fk_sky',
                'dependent' => false
            )
        );
        $this->assertEquals('fk_sky', $relationCustom->foreignKey);
        $this->assertFalse($relationCustom->dependent);
    }

    /**
     * Check the relation construction
     * @test
     */
    public function retrieveRelations()
    {
        $sun = new Sun();
        $relations = $sun->relations;
        $this->assertTrue(is_array($relations));
        $this->assertTrue(isset($relations['sky']));
        $this->assertInstanceOf('Model\Sun', $relations['sky']->model);
        $this->assertInstanceOf('\Models\Sky', $relations['sky']->related);
        $this->assertEquals('sky_id', $relations['sky']->foreignKey);
        $this->assertTrue($relations['sky']->dependent);

        $cloud = new Cloud();
        $relCloud = $cloud->relations;

        $this->assertTrue(isset($relCloud['sky']));
        $this->assertInstanceOf('Model\Cloud', $relCloud['sky']->model);
        $this->assertInstanceOf('\Models\Sky', $relCloud['sky']->related);
        $this->assertEquals('fk_sky', $relCloud['sky']->foreignKey);
        $this->assertFalse($relCloud['sky']->dependent);
    }

    /**
     * Testing the join on hasone relation
     * @test
     */
    public function queryModel()
    {
        $result = Sun::all();
        $this->assertInstanceOf('\Slick\Model\DataSet', $result);
        $this->assertEquals(2, count($result));
        $first = $result->first();
        $this->assertInstanceOf('Model\Sun', $first);
        $this->assertInstanceOf('\Models\Sky', $first->sky);
    }

    /**
     * Trying to save the relational foreignkey
     * @test
     */
    public function saveRelationalData()
    {
        $this->assertTrue(true);
        $sun = new Sun(
            array(
                'id' => 1,
                'sky' => new \Models\Sky(array('id' => '1'))
            )
        );
        $sun->save();
        $sql = "UPDATE suns SET `light` = NULL, `sky_id` = '1' WHERE id = 1";

        $this->assertEquals($sql, trim(BelongsToConnector::$lastQuery));
    }

}

class Sun extends \Slick\Model
{

    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * @belongsto Sky
     * @readwrite
     * @var Sky
     */
    protected $_sky;

    /**
     * @column
     * @type integer
     * @readwrite
     * @var int
     */
    protected $_light;

    /**
     * Set the mock connector
     */
    public function getConnector()
    {
        return new BelongsToConnector();
    }
}

class Cloud extends \Slick\Model
{
    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * @belongsto Sky
     * @foreignkey fk_sky
     * @dependent false
     * @readwrite
     * @var Sky
     */
    protected $_sky;
}


class DbBelongsToResource
{

    public function fetch_array($const)
    {
        static $i;
        if (!isset($i)) {
            $i = 0;
        }
        $data = array(
            array(1, '2', '2'),
            array(2, '1', '1')
        );
        if (isset($data[$i])) {
            return $data[$i++];
        } else {
            unset($i);
            return false;
        }
        
    }

    public $num_rows = 2;

    public function fetch_fields()
    {
        $data = array(
            (Object) array(
                'name' => 'id',
                'table' => 'Sun'
            ),
            (Object) array(
                'name' => 'sky_id',
                'table' => 'Sun'
            ),
            (Object) array(
                'name' => 'id',
                'table' => 'Sky'
            ),
        );
        return  $data;

    }
}

/**
 * Mocked connector
 */
class BelongsToConnector extends \Slick\Database\Connector\Mysql
{

    public static $lastQuery = null;

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        BelongsToConnector::$lastQuery = $sql;
        return new DbBelongsToResource();
    }

}