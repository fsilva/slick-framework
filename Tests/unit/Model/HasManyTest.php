<?php

/**
 * HasMany relation test case
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 * @copyright 2013 Filipe Silva
 * @license Apache License, Version 2.0 (the "License")
 * @since Version 0.1.0
 */

namespace Model;
use Codeception\Util\Stub;
use Slick\Model\Relation\HasMany;

/**
 * HasMany
 *
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 */
class HasManyTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Relation data
     * @test
     */
    public function checkDefinition()
    {
        $basket = new Basket();
        $relation = new HasMany($basket, 'Fruit', array());
        $this->assertEquals('basket_id', $relation->foreignKey);
        $this->assertEquals('fruits', $relation->foreignKeyTable);
        $this->assertEquals($basket, $relation->model);
        $this->assertInstanceOf('\Models\Fruit', $relation->related);
        $this->assertTrue($relation->dependent);
        $this->assertEquals('select', $relation->queryType);

        $tree = new Tree();
        $relationCustom = new HasMany(
            $tree,
            'Fruit',
            array(
                'foreignKey' => 'fk_ftuit',
                'dependent' => false
            )
        );
        $this->assertEquals('fk_ftuit', $relationCustom->foreignKey);
        $this->assertFalse($relationCustom->dependent);
    }

    /**
     * Check the relation construction
     * @test
     */
    public function retriveRelations()
    {
        $basket = new Basket();
        $relations = $basket->relations;
        $this->assertTrue(is_array($relations));
        $this->assertTrue(isset($relations['fruits']));
        $this->assertInstanceOf('Model\Basket', $relations['fruits']->model);
        $this->assertInstanceOf('\Models\Fruit', $relations['fruits']->related);
        $this->assertEquals('basket_id', $relations['fruits']->foreignKey);
        $this->assertTrue($relations['fruits']->dependent);

        $tree = new Tree();
        $relTree = $tree->relations;

        $this->assertTrue(isset($relTree['fruits']));
        $this->assertInstanceOf('Model\Tree', $relTree['fruits']->model);
        $this->assertInstanceOf('\Models\Fruit', $relTree['fruits']->related);
        $this->assertEquals('fk_tree', $relTree['fruits']->foreignKey);
        $this->assertFalse($relTree['fruits']->dependent);
    }

    /**
     * Testing the join on hasone relation
     * @test
     */
    public function allQuery()
    {
        $result = Basket::all();
        $this->assertInstanceOf('\Slick\Model\DataSet', $result);
        $this->assertEquals(2, count($result));
        $first = $result->first();
        $this->assertInstanceOf('Model\Basket', $first);
        $firstCall = $first->fruits;
        $this->assertInstanceOf('\Slick\Model\DataSet', $firstCall);
        $this->assertSame($firstCall, $first->fruits);
        $this->assertInstanceOf('\Models\Fruit', $firstCall->first());
    }

    /**
     * Trying to save the relational foreignkey
     * @test
     */
    public function saveRelationalData()
    {
        $basket = new Basket(
            array(
                'id' => 1,
                'weight' => 25
            )
        );
        $basket->save();
        $sql = "UPDATE baskets SET `weight` = 25 WHERE id = 1";
        $this->assertEquals($sql, trim(HasManyConnector::$lastQuery));
    }

}

class Basket extends \Slick\Model
{
    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * @hasmany Fruit
     * 
     * @readwrite
     * @var Slick\Model\DataSet
     */
    protected $_fruits;

    /**
     * @column
     * @readwrite
     * @type integer
     * @var int
     */
    protected $_weight;

    /**
     * Set the mock connector
     */
    public function getConnector()
    {
        return new HasManyConnector();
    }

}

class Tree extends \Slick\Model
{
    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * @hasmany Fruit
     * @dependent false
     * @foreignkey fk_tree
     * 
     * @readwrite
     * @var Slick\Model\DataSet
     */
    protected $_fruits;
}


class DbHasManyResource
{

    public function fetch_array($const)
    {
        static $i;
        if (!isset($i)) {
            $i = 0;
        }
        $data = array(
            array(1),
            array(2)
        );
        if (isset($data[$i])) {
            return $data[$i++];
        } else {
            unset($i);
            return false;
        }
        
    }

    public $num_rows = 2;

    public function fetch_fields()
    {
        $data = array(
            (Object) array(
                'name' => 'id',
                'table' => 'Basket'
            )
        );
        return  $data;

    }
}

class DbHasManyChildsResource
{
    public function fetch_array($const)
    {
        static $i;
        if (!isset($i)) {
            $i = 0;
        }
        $data = array(
            array(1, 2),
            array(2, 2)
        );
        if (isset($data[$i])) {
            return $data[$i++];
        } else {
            unset($i);
            return false;
        }
        
    }

    public $num_rows = 2;

    public function fetch_fields()
    {
        $data = array(
            (Object) array(
                'name' => 'id',
                'table' => 'Fruit'
            ),
            (Object) array(
                'name' => 'basket_id',
                'table' => 'Fruit'
            )
        );
        return  $data;

    }
}

/**
 * Mocked connector
 */
class HasManyChildsConnector extends \Slick\Database\Connector\Mysql
{

    public static $lastQuery = null;

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        self::$lastQuery = $sql;
        return new DbHasManyChildsResource();
    }

}


/**
 * Mocked connector
 */
class HasManyConnector extends \Slick\Database\Connector\Mysql
{

    public static $lastQuery = null;

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        self::$lastQuery = $sql;
        return new DbHasManyResource();
    }

}