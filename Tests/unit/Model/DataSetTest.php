<?php

/**
 * DataSetTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Model;

use Codeception\Util\Stub;
use Slick\Model\DataSet;

/**
 * DataSetTest
 *
 * Dataset is a list of model objects.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class DataSetTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT object
     * @var Slick/Model/DataSet
     */
    protected $dataSet;

    /**
     * Sample data for tests.
     * @var array
     */
    protected $data = array(
        array(
            'id' => '1',
            'name' => 'Filipe'
        ),
        array(
            'id' => '2',
            'name' => 'Ana'
        ),
        array(
            'id' => '3',
            'name' => 'Miguel'
        )
    );

    /**
     * Setup creates a list with sample data.
     */
    protected function _before()
    {
        $this->dataSet = new DataSet();
        foreach ($this->data as $value) {
            $this->dataSet->add((object) $value);
        }
    }

    /**
     * Unset the SUT object for next test.
     */
    protected function _after()
    {
        unset($this->dataSet);
    }

    /**
     * Test for using dataset as an array
     * @test
     */
    public function useAsArray()
    {
        $this->assertEquals('Filipe', $this->dataSet[0]->name);
        $this->dataSet[] = (object) array('id' => '4', 'name' => 'test');
        $this->assertEquals('test', $this->dataSet->last()->name);
        $i = $this->dataSet->forward()->key();
        $this->dataSet[$i] = (object) array('id' => '4', 'name' => 'test1');
        $this->assertEquals('test1', $this->dataSet->last()->name);
        $this->assertTrue(isset($this->dataSet[$i]));
        unset($this->dataSet[$i]);
        $this->assertFalse(isset($this->dataSet[$i]));
    }

    /**
     * Test object serialization
     * @test
     */
    public function serializeDataset()
    {
        $ser = serialize($this->dataSet);
        $des = unserialize($ser);
        $this->assertNotSame($this->dataSet, $des);
        $this->assertEquals($this->dataSet, $des);
    }

    /**
     * Test the list navigation methods.
     * @test
     */
    public function useAsAList()
    {
        $this->assertEquals(3, $this->dataSet->count());
        $expected = (object) $this->data[0];
        $this->assertEquals($expected, $this->dataSet->current());

        $this->assertInstanceOf('\Slick\Model\DataSet', $this->dataSet->forward());
        $this->assertEquals(2, $this->dataSet->key());

        $this->assertInstanceOf('\Slick\Model\DataSet', $this->dataSet->next());
        $this->assertFalse($this->dataSet->valid());

        $this->dataSet->forward();

        $expected = (object) $this->data[1];
        $this->assertInstanceOf('\Slick\Model\DataSet', $this->dataSet->previous());
        $this->assertEquals($expected, $this->dataSet->current());
        $this->assertTrue($this->dataSet->valid());


        $this->assertInstanceOf('\Slick\Model\DataSet', $this->dataSet->rewind());
        $this->assertEquals(0, $this->dataSet->key());

        $this->assertInstanceOf('\Slick\Model\DataSet', $this->dataSet->previous());
        $this->assertFalse($this->dataSet->valid());

        $this->dataSet->rewind();

        $expected = (object) $this->data[1];
        $this->assertInstanceOf('\Slick\Model\DataSet', $this->dataSet->next());
        $this->assertEquals($expected, $this->dataSet->current());
        $this->assertTrue($this->dataSet->valid());

        $expected = (object) $this->data[0];
        $this->assertEquals($expected, $this->dataSet->first());

        $expected = (object) $this->data[2];
        $this->assertEquals($expected, $this->dataSet->last());

        $set = new DataSet();
        $this->assertTrue($set->isEmpty()) ;

        $this->assertNull($set->first());
        $this->assertNull($set->last());

    }

}