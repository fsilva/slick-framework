<?php

/**
 * ModelTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Model;

use Codeception\Util\Stub;
use Slick\Registry;

/**
 * ModelTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ModelTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT model.
     * @var Models\User
     */
    protected $_user;

    /**
     * Preparing the SUT for tests
     */
    protected function _before()
    {
        $this->_user = new UserSUT();
    }

    /**
     * Cleanup for next test.
     */
    protected function _after()
    {
        unset($this->_user);
        UserSUT::$connectorFail = false;
    }

    /**
     * Tests the unimplemented methods exception.
     * 
     * @expectedException Slick\Model\Exception\Implementation
     * @test
     */
    public function verifyImplementation()
    {
        $this->_user->unimplemented();
    }

    /**
     * Test some getters for model.
     * 
     * @test
     */
    public function validateDefaultGetters()
    {
        $this->assertEquals("usersuts", $this->_user->getTable());
        $expected = array(
            'raw' => '_number',
            'name' => 'number',
            'primary' => false,
            'type' => 'autonumber',
            'length' => null,
            'index' => false,
            'read' => true,
            'write' => true,
            'validate' => array(
                0 => 'required'
            ),
            'label' => null,
        );
        $this->assertEquals($expected, $this->_user->getColumn('number'));
        $this->assertNull($this->_user->getColumn('invented'));
        $this->_user->connector = null;
        $this->assertInstanceOf('Slick\Database\Query', $this->_user->query());

        $this->assertTrue(is_array($this->_user->getColumns()));
        
        try {
            $u = new BadUser();
            $this->fail("This should raise an exception here.");
        } catch(\Slick\Core\Exception $e) {
            $this->assertInstanceOf('Slick\Model\Exception\Type', $e);
        }

        try {
            $u = new NoIdUser();
            $this->fail("This should raise an exception here.");
        } catch(\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Model\Exception\Primary', $e);
        }
    }

    /**
     * Test the connector getter
     * @test
     * @expectedException \Slick\Model\Exception\Connector
     */
    public function retrieveDbConnector()
    {
        $u = new \Models\User();
        \Slick\Registry::erase("database_{$u->connection}");
        try {
            $connector = $u->getConnector();
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Model\Exception\Connector', $e);
        }
        $connector = new ModelConnector();

        $u->setConnector(null);
        $dbBackup = Registry::get("database_{$u->connection}");
        Registry::set("database_{$u->connection}", $connector);
        $c = $u->getConnector();
        $this->assertInstanceOf('\Slick\Database\Connector', $c);

        $c2 = $u->getConnector();
        $this->assertSame($c, $c2);

        Registry::set("database_{$u->connection}", $dbBackup);
        
        UserSUT::$connectorFail = true;
        $this->_user->connector = null;
        $this->_user->connection = 'unknown';
        $this->_user->getConnector();
    }

    /**
     * Test the load funtion
     * @test
     * @expectedException Slick\Model\Exception\Primary
     */
    public function loadModel()
    {
        $this->_user->id = 1;
        $this->_user->load();
        $this->assertEquals('fsilva', $this->_user->getUsername());
        ModelConnectorQuery::$loadFail = true;
        $this->_user->load();
    }

    /**
     * Testing save operations
     * @test
     */
    public function saveRecord()
    {
        ModelConnectorQuery::$loadFail = false;
        $this->_user->setId(1)->load();
        $this->assertTrue($this->_user->save());
        $this->_user->id = null;
        $this->assertEquals(5, $this->_user->save());
    }

    /**
     * Testing the delete operation
     * @test
     */
    public function deleteRecord()
    {
        $this->assertEquals(0, $this->_user->delete());
        $this->_user->setId(1)->load();
        $this->assertEquals(1, $this->_user->delete());

        $affected = UserSUT::deleteAll(
            array('id > ?' => 10)
        );

        $this->assertEquals(1, $affected);
    }

    /**
     * Test the count method.
     * @test
     */
    public function countRecords()
    {
        $this->assertEquals(10, UserSUT::count(array('id = ?', 2)));
    }

    /**
     * Test the select all records method.
     * @test
     */
    public function selectAllRecords()
    {
        $all = UserSUT::all(array('id = ?', 1), array('*'), 'username', 'DESC', 2);
        $this->assertContains('fsilva', $all[0]->username);
        $first = UserSUT::first(array('id = ?', 1), array('*'), 'username', 'DESC', 2);
        $this->assertContains('fsilva', $first->username);
        ModelConnectorQuery::$loadFail = true;
        $first = UserSUT::first(array('id = ?', 1), array('*'), 'username', 'DESC', 2);
        $this->assertNull($first);
    }

    /**
     * Tests the vaklidate method.
     * @test
     * @expectedException Slick\Model\Exception\Validation
     */
    public function validate()
    {
        $u = new \Models\User();
        $u->username = 12;
        $u->password = 123456789;
        $u->age = 12;
        $this->assertFalse($u->validate());
        $errors = $u->errors;

        $this->assertTrue(isset($errors['number']));
        $this->assertTrue(isset($errors['username']));
        $this->assertEquals(2, count($errors['username']));
        $this->assertTrue(isset($errors['password']));
        $this->assertEquals(1, count($errors['password']));
        $this->assertFalse(isset($errors['age']));

        $uu = new UserSUT();
        $uu->validate();
    }

}

class UserSUT extends \Models\User
{

    public static $connectorFail = false;

    public function getConnector()
    {
        if (self::$connectorFail) {
            return parent::getConnector();
        }
        return new ModelConnector();
    }

    /**
     * @column
     * @type integer
     * @readwrite
     * @validate unknown
     * @var integer
     */
    protected $_height;
}

class BadUser extends \Slick\Model
{

    /**
     * @column
     * @primary
     * @readwrite
     * @type autonumber
     * @var integer
     */
    protected $_id;
    
    /**
     * @column
     * @type unknown
     * @var string
     */
    protected $_fun;
}

class NoIdUser extends \Slick\Model
{

    /**
     * @column
     * @readwrite
     * @type autonumber
     * @var integer
     */
    protected $_id;
    
    /**
     * @column
     * @type text
     * @var string
     */
    protected $_fun;
}

class ModelConnector extends \Slick\Database\Connector\Mysql
{

    /**
     * a mock query
     */
    public function query()
    {
        return new ModelConnectorQuery(array('connector' => $this));
    }

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    public function connect()
    {
        return $this;
    }

}

class ModelConnectorQuery extends \Slick\Database\Query\Mysql
{

    public static $loadFail = false;

    public function first()
    {
        if (self::$loadFail) {
            return null;
        }
        return array(
            'id' => 1,
            'number' => '20001000',
            'username' => 'fsilva',
            'password' => md5('this is a password!'),
            'age' => 35,
            'birthDate' => '1977-08-25 10:00:00',
            'rank' => 0.86
        );
    }

    public function all($array = array())
    {
        return array(array(
            'id' => 1,
            'number' => '20001000',
            'username' => 'fsilva',
            'password' => md5('this is a password!'),
            'age' => 35,
            'birthDate' => '1977-08-25 10:00:00',
            'rank' => 0.86
        ));
    }

    public function save($data)
    {
        if (!empty($this->_where)) {
            return true;
        }
        return 5;
    }

    public function delete()
    {
        if (self::$loadFail) {
            return 0;
        }
        return 1;
    }

    public function count()
    {
        return 10;
    }
}