<?php

/**
 * HasOne relation test case
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 * @copyright 2013 Filipe Silva
 * @license Apache License, Version 2.0 (the "License")
 * @since Version 0.1.0
 */

namespace Model;

use Codeception\Util\Stub;
use Slick\Model\Relation\HasOne;

/**
 * HasOne
 *
 * @package Slick
 * @subpackage Test\Model
 * @author Filipe Silva <silvam.filipe@gmail.com>
 */
class HasOneTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

/**
     * Relation data
     * @test
     */
    public function checkDefinition()
    {
        $apple = new Apple();
        $relation = new HasOne($apple, 'Banana', array());
        $this->assertEquals('apple_id', $relation->foreignKey);
        $this->assertEquals('bananas', $relation->foreignKeyTable);
        $this->assertEquals($apple, $relation->model);
        $this->assertEquals('join', $relation->queryType);
        $this->assertInstanceOf('\Models\Banana', $relation->related);
        $this->assertTrue($relation->dependent);

        $relationCustom = new HasOne(
            $apple,
            'Banana',
            array(
                'foreignKey' => 'the_banana',
                'dependent' => false
            )
        );
        $this->assertEquals('the_banana', $relationCustom->foreignKey);
        $this->assertFalse($relationCustom->dependent);
    }

    /**
     * Check the relation construction
     * @test
     */
    public function retriveRelations()
    {
        $apple = new Apple();
        $relations = $apple->relations;
        $this->assertTrue(is_array($relations));
        $this->assertTrue(isset($relations['banana']));
        $this->assertInstanceOf('Model\Apple', $relations['banana']->model);
        $this->assertInstanceOf('\Models\Banana', $relations['banana']->related);
        $this->assertEquals('apple_id', $relations['banana']->foreignKey);
        $this->assertTrue($relations['banana']->dependent);

        $orange = new Orange();
        $relOrange = $orange->relations;

        $this->assertTrue(isset($relOrange['banana']));
        $this->assertInstanceOf('Model\Orange', $relOrange['banana']->model);
        $this->assertInstanceOf('\Models\Banana', $relOrange['banana']->related);
        $this->assertEquals('my_orange_id', $relOrange['banana']->foreignKey);
        $this->assertFalse($relOrange['banana']->dependent);
    }

    /**
     * Testing the join on hasone relation
     * @test
     */
    public function queryModel()
    {
        $result = Apple::all();
        $this->assertInstanceOf('\Slick\Model\DataSet', $result);
        $this->assertEquals(2, count($result));
        $first = $result->first();
        $this->assertInstanceOf('Model\Apple', $first);
        $this->assertInstanceOf('\Models\Banana', $first->banana);
    }

    /**
     * Trying to save the relational foreignkey
     * @test
     */
    public function saveRelationalData()
    {
        $apple = new Apple(
            array(
                'id' => '1',
                'type' => 'sweet'
            )
        );
        $apple->save();
        $sql = "UPDATE apples SET `type` = 'sweet' WHERE id = '1'";
        $this->assertEquals($sql, trim(HasOneConnector::$lastQuery));
    }
}

/**
 * A model for tests
 */
class Apple extends \Slick\Model
{

    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * @hasone Banana
     * @dependent true
     * @readwrite
     * @var Banana
     */
    protected $_banana;

    /**
     * @column
     * @type text
     * @readwrite
     * @var string
     */
    protected $_type;

    /**
     * Set the mock connector
     */
    public function getConnector()
    {
        return new HasOneConnector();
    }
}

class Orange extends \Slick\Model
{

    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * @hasone Banana
     * @dependent false
     * @foreignkey my_orange_id
     * @var Banana
     */
    protected $_banana;

}

class DbHasOneResource
{

    public function fetch_array($const)
    {
        static $i;
        if (!isset($i)) {
            $i = 0;
        }
        $data = array(
            array(1, '2', '1'),
            array(2, '1', '2')
        );
        if (isset($data[$i])) {
            return $data[$i++];
        } else {
            unset($i);
            return false;
        }
        
    }

    public $num_rows = 2;

    public function fetch_fields()
    {
        $data = array(
            (Object) array(
                'name' => 'id',
                'table' => 'Apple'
            ),
            (Object) array(
                'name' => 'id',
                'table' => 'Banana'
            ),
            (Object) array(
                'name' => 'apple_id',
                'table' => 'Banana'
            ),
        );
        return  $data;

    }
}

/**
 * Mocked connector
 */
class HasOneConnector extends \Slick\Database\Connector\Mysql
{

    public static $lastQuery = null;

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        self::$lastQuery = $sql;
        return new DbHasOneResource();
    }

}
