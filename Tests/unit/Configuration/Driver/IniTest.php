<?php

/**
 * IniTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Configuration\Driver;

use Codeception\Util\Stub;
use Slick\Configuration\Driver\Ini as IniDriver;

/**
 * Ini configuration driver
 *
 * @package    Slick
 * @subpackage Tests\Unit\Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class IniTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Test for not implemented method exception
     * @test
     * @expectedException Slick\Configuration\Exception\Implementation
     */
    public function methodNotImplemented()
    {
        $conf = new IniDriver();
        $conf->notImplemented();
    }

    /**
     * Test for not implemented method exception
     * @test
     * @expectedException Slick\Configuration\Exception\Argument
     */
    public function parseAnEmptyConfigPath()
    {
        $conf = new IniDriver();
        $conf->parse('');
    }

    /**
     * Test initialization
     * @test
     */
    public function initializeIniConfiguration()
    {
        $conf = new IniDriver();
        $conf = $conf->initialize();
        $this->assertInstanceOf('Slick\Configuration\Driver', $conf);
        $this->assertInstanceOf('Slick\Configuration\Driver\Ini', $conf);
    }

    /**
     * Test empty ini file parsing
     * @test
     * @expectedException Slick\Configuration\Exception\Syntax
     */
    public function parseAnEmptyIniFile()
    {
        $conf = new IniDriver();
        $conf->parse(APP_PATH . '/Configuration/Empty');
    }

    /**
     * Test parsing method.
     * @test
     */
    public function parseConfiguration()
    {
        $conf = new IniDriver();
        $parse = $conf->parse(APP_PATH . '/Configuration/Test');

        $this->assertTrue(is_object($parse));
        $this->assertEquals($parse->name, 'Filipe Silva');
        $this->assertTrue(is_object($parse->book));
        $this->assertEquals($parse->book->application->name, 'Slick');
    }

}