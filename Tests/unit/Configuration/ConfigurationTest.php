<?php

/**
 * ConfigurationTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Configuration;

use Codeception\Util\Stub;
use Slick\Configuration;

/**
 * ConfigurationTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ConfigurationTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Test for not implemented method exception
     * @test
     * @expectedException Slick\Configuration\Exception\Implementation
     */
    public function callAnUnimplementedMethod()
    {
        $conf = new Configuration();
        $conf->notImplemented();
    }

    /**
     * Test for not implemented method exception
     * @test
     * @expectedException Slick\Configuration\Exception\Argument
     */
    public function initializeAnEmptyType()
    {
        $conf = new Configuration();
        $conf->initialize();
    }

    /**
     * Test for not implemented method exception
     * @test
     * @expectedException Slick\Configuration\Exception\Argument
     */
    public function initializeAnInvalidType()
    {
        $conf = new Configuration(array('type' => 'unknow'));
        $conf->initialize();
    }

    /**
     * Tests initializarion returns
     * @test
     */
    public function initializeConfiguration()
    {
        $conf = new Configuration(array('type' => 'ini'));
        $conf = $conf->initialize();
        $this->assertInstanceOf('Slick\Configuration\Driver', $conf);
        $this->assertInstanceOf('Slick\Configuration\Driver\Ini', $conf);
    }

}