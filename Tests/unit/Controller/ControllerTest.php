<?php

/**
 * ControllerTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Controller;

use Codeception\Util\Stub;
use Slick\Controller;
use Slick\Registry;

/**
 * ControllerTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ControllerTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Tests controller construction
     * @test
     * @expectedException \Slick\Controller\Exception\Implementation
     */
    public function runConstructor()
    {
        $con = new Controller(array('willRenderLayoutView' => true));
        $this->assertEquals('Slick\Controller', $con->getName());

        $this->assertInstanceOf('\Slick\View', $con->getActionView());
        $this->assertEquals(
            'Pages/index.html',
            $con->actionView->getFile()
        );
        $con->notImplemented();
    }

    /**
     * Test the content type assigment
     * @test
     */
    public function verifyContentTypes()
    {
        $con = new Controller();
        $this->assertEquals(
            'text/xml',
            $con->setExtension('xml')->getContentType()
        );
        $this->assertEquals(
            'text/html',
            $con->setExtension('html')->getContentType()
        );
        $this->assertEquals(
            'text/html',
            $con->setExtension('xpto')->getContentType()
        );
    }

    /**
     * Tests for settings view vars
     * @test
     * @expectedException \Slick\View\Exception\Data
     */
    public function setViewVars()
    {
        $con = new Controller();
        $con->set('var1', 'one');
        $this->assertTrue(is_array($con->getViewVars()));
        $vars = $con->getViewVars();
        $this->assertEquals('one', $vars['var1']);

        $con->set(array('var1' => 'one+', 'var2' => 'tow'));
        $vars = $con->getViewVars();
        $this->assertEquals('one+', $vars['var1']);
        $this->assertEquals('tow', $vars['var2']);

        //test exception
        $con->set(true);
    }

    /**
     * Tests the disable rendering methos
     * @test
     */
    public function disableRender()
    {
        $con = new Controller();
        $con->disableRendering(false);
        $this->assertTrue($con->willRenderLayoutView);
        $this->assertTrue($con->willRenderActionView);
        $con->disableRendering();
        $this->assertFalse($con->willRenderLayoutView);
        $this->assertFalse($con->willRenderActionView);
    }

    /**
     * Test controller output render
     * @test
     */
    public function renderView()
    {
         
        $mockView = $this->getMock('\Slick\View', array('render'));

        $mockView->expects($this->any())
            ->method('render')
            ->will($this->returnValue('Layout output'));

        $con = new Controller();
        $con->setActionView($mockView);

        $con->willRenderActionView = true;
        $con->willRenderLayoutView = false;
        $this->assertEquals('Layout output', $con->render());

        $con->willRenderActionView = false;
        $con->willRenderLayoutView = false;
        $this->assertEquals(null, $con->render());

    }

    /**
     * Test controller output render
     * @test
     */
    public function renderLayout()
    {
         
        $mockView = $this->getMock('\Slick\View', array('render'));

        $mockView->expects($this->any())
            ->method('render')
            ->will($this->returnValue('Action output'));

        $con = new Controller();
        $con->setLayoutView($mockView);

        $con->willRenderActionView = false;
        $con->willRenderLayoutView = true;
        $this->assertEquals('Action output', $con->render());

    }

    /**
     * Test controller output render
     * @test
     * @expectedException \Slick\View\Exception\Renderer
     */
    public function renderException()
    {
         
        $mockView = $this->getMock('Slick\View', array('render'));

        $mockView->expects($this->any())
            ->method('render')
            ->will($this->throwException(new \Exception()));

        $con = new Controller();
        $con->setLayoutView($mockView);

        $con->willRenderActionView = true;
        $con->willRenderLayoutView = true;

        $con->render();

    }

    /**
     * Setting layout view test
     * @test
     */
    public function setDifferentLayout()
    {
        $con = new Controller();

        $resp = $con->setLayout("Layouts/ajax");
        $file = 'Layouts/ajax.html';
        $this->assertEquals($file, $con->getLayoutView()->file);
        $this->assertInstanceOf('\Slick\View', $con->layoutView);
        $this->assertInstanceOf('\Slick\Controller', $resp);
    }

    /**
     * Setting layout view test
     * test
     */
    public function setDifferentView()
    {
        $con = new Controller();
        $resp = $con->setView("Common/vcard");
        $file = APP_PATH . '/Views/Common/vcard.html';
        $this->assertEquals($file, $con->getActionView()->file);
        $this->assertInstanceOf('\Slick\View', $con->actionView);
        $this->assertInstanceOf('\Slick\Controller', $resp);
    }

    /**
     * Compose an url from array
     * @test
     */
    public function composeAnUrl()
    {
        $expected = 'pages/index/1';
        $this->assertEquals(
            $expected,
            Controller::urlFromArray(
                array(
                    'controller' => 'pages',
                    'action' => 'index',
                    '1'
                )
            )
        );

        $router = Stub::constructEmpty(
            '\Slick\Router',
            array(),
            array(
                'action' => 'read',
                'controller' => 'users'
            )
        );
        \Slick\Registry::set('router', $router);
        $this->assertEquals(
            $expected,
            Controller::urlFromArray(
                array(
                    'controller' => 'pages',
                    'action' => 'index',
                    '1'
                )
            )
        );
        \Slick\Registry::erase('router');
    }

    /**
     * Redirecting the flow to a new request.
     * @test
     */
    public function redirect()
    {
        $controller = new Controller();
        $response = new \Slick\Http\Response();
        \Slick\Registry::set('response', $response);
        $controller->redirect("pages/index");
        $headers = $controller->response->headers;
        $this->assertTrue(isset($headers['Location']));
        $this->assertSame($response, $controller->response);
        $this->assertEquals('/pages/index', $headers['Location']);

        $controller->redirect(
            array(
                'controller' => 'pages',
                'action' => 'home'
            )
        );
        $headers = $response->getHeaders();
        $this->assertEquals('/pages/home', $headers['Location']);
    }

}