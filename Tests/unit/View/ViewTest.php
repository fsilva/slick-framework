<?php

/**
 * ViewTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\View
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace View;

use Codeception\Util\Stub;
use Slick\View;

/**
 * ViewTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\View
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ViewTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT object
     * @var Slick\View
     */
    protected $view;

    /**
     * Set a new object before the test
     */
    protected function _before()
    {
        parent::_before();
        $this->view = new View();
    }

    /**
     * Unsets the view object after each test
     */
    protected function _after()
    {
        unset($this->view);
        parent::_after();
    }

    // tests
    
    /**
     * Tests implementation for view.
     * @test
     * @expectedException \Slick\View\Exception\Implementation
     */
    public function implementaion()
    {
        $this->assertInstanceOf('Slick\Base', $this->view);
        $this->assertInstanceOf('Slick\Template', $this->view->template);
        $this->view->unimplemented();
    }

    /**
     * Tests the get, set and erase data methods.
     * @test
     * @expectedException \Slick\View\Exception\Data
     */
    public function getterSetterErase()
    {
        $this->assertEmpty($this->view->get('unregistered'));
        $this->assertEquals('test', $this->view->get('unregistered', 'test'));
        $value = 'A test';
        $this->view->set('var', $value);
        $this->assertEquals($value, $this->view->get('var'));

        $var1 = 'one'; $var2 = 'two';
        $object = $this->view->set(compact('var1', 'var2'));
        $this->assertEquals('two', $this->view->get('var2'));
        $this->assertInstanceOf('\Slick\View', $object);

        $result = $this->view->erase('var1');
        $this->assertInstanceOf('\Slick\View', $result);
        $this->assertEquals('two', $this->view->get('var2'));
        $this->assertEquals('', $this->view->get('var1'));

        $this->view->set(false, true);

    }

    /**
     * Tests the render method.
     * @test
     */
    public function render()
    {
        $this->view->setFile('test.html');
        $this->view->set('output', "My render test");
        $output = $this->view->render();
        $this->assertEquals('<test>My render test</test>', $output);
    }

    /**
     * Checking exception output
     * @test
     */
    public function verifyExceptionOutput()
    {
        $e = new \Slick\View\Exception\Renderer(
            'Cannot set template',
            null,
            new \Exception('Dum Message')
        );
        $this->assertTrue(is_string($e->getDescription()));
    }
}