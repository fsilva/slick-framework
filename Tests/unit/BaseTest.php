<?php
use Codeception\Util\Stub;
use Slick\Base;


/**
 * This class is not constructed well
 * 
 * @package Slick
 * @subpackage Test\Core
 */
class WrongConstruct extends Slick\Base {
    public function __construct($options = array()) {
        
    }
}

class BaseTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The test base object
     * @var Base
     */
    private $base;

    /**
     * Creates a test object for this test
     */
    protected function _before()
    {
        $this->base = new Libs\BaseTest(array('name' => 'Filipe Silva'));
    }

    protected function _after()
    {
        unset($this->base);
    }
    
    /**
     * Check implementation exception
     * @test
     * @expectedException Slick\Core\Exception\Implementation
     */
    public function callAnUnimplementedMethod()
    {
        $this->base->notImplemented();
    }
    
    /**
     * Check base constructor in class.
     * @test
     * @expectedException Slick\Core\Exception
     */
    public function loadAWrongConstructor()
    {
        $this->assertEquals('Filipe Silva', $this->base->name);
        $test = new WrongConstruct();
        $test->name = '';
    }

    /**
     * Tests the setter mechanism.
     * @test
     */
    public function assignWithMagiSetter()
    {
        $this->assertInstanceOf('Slick\Base', $this->base->setCountry('Brasil'));
        $this->assertEquals($this->base->country, 'Brasil');
    }

    /**
     * Tests the getter mechanism.
     * @test
     */
    public function retrieveWithMagicGetter()
    {
        $this->base->country = 'Brasil';
        $this->assertEquals($this->base->getCountry(), 'Brasil');
        $this->assertNull($this->base->getUndeined());
    }
    
    /**
     * Check implementation exception
     * @test
     * @expectedException Slick\Core\Exception\Readonly
     */
    public function VerifyReadonlyProperty() {
        $this->base->age = 10;
    }

    /**
     * Check implementation exception
     * @test
     * @expectedException Slick\Core\Exception\Writeonly
     */
    public function VerifyWriteonlyProperty()
    {
        $this->base->gender = 'Female';
        $gender = $this->base->gender;
    }

    /**
     * Check the exception thrown when an invalid property is used.
     * @test
     * @expectedException Slick\Core\Exception\Property
     */
    public function VerifyInvalidProperty()
    {
        $this->base->invalid = true;
    }
    
    /**
     * Tests if request property is a Slick\Core\Request object.
     * @test
     */
    public function checkRequestProperty()
    {
        $this->assertInstanceOf('\Slick\Http\Request', $this->base->request); 
    }

    /**
     * Check session property on base class
     * @test
     */
    public function checkSessionProperty()
    {
        $driver = new \Slick\Session(array('type' => 'server'));
        $session = $driver->initialize();

        \Slick\Registry::set("session", $session);
        $this->assertInstanceOf('\Slick\Session\Driver', $this->base->session);
        $this->assertSame($session, $this->base->session);
    }

}