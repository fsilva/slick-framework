<?php
namespace Http;
use Codeception\Util\Stub;
use Slick\Http\Request;

class RequestTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Sets the data for tests.
     */
    protected function _before()
    {
        $_POST['data'] = array(
            'User' => array(
                'name' => 'username',
                'email' => 'username@example.com'
            )
        );
        $_GET = array(
            'search' => '12345',
            'test' => 'tested',
            'nested' => array(
                'param' => 'value'
            )
        );
        $_REQUEST = array_merge($_GET, $_POST);
    }

    /**
     * Cleans up thing for next test.
     */
    protected function _after()
    {
        unset($_POST['data']);
    }

    // tests
    
    /**
     * Test implementation
     * @test
     * @expectedException \Slick\Http\Exception\Implementation
     */
    public function checkImplementation()
    {
        $request = new Request();
        $request->unImplemented();
    }

    /**
     * Check that base use the same referrence to request object.
     * @test
     */
    public function checkReferrence()
    {
        $request = new Request();
        $this->assertInstanceOf('\Slick\http\Response', $request->response);
        \Slick\Registry::set('request', $request);
        $this->assertSame($request, $request->getRequest());
    }

    /**
     * Check formd ata retrieve.
     * @test
     * @expectedException Slick\Core\Exception\Property
     */ 
    public function verifyData()
    {
        $request = new Request();
        $this->assertEquals($_POST['data'], $request->data);
        $this->assertEquals($_REQUEST, $request->params);
        unset($_POST['data']);
        $this->assertEquals(array(), $request->data);
        $this->assertEquals('', $request->input);
        $this->assertEquals('', $request->body);
        $request->testException = null;
    }

    /**
     * Check the retrieving params 
     * @test
     */
    public function retrieveParameters()
    {
        $request = new Request();
        $this->assertEquals('value', $request->param("nested.param"));
        $this->assertEquals('12345', $request->param("search"));
        $this->assertEquals(
            'username@example.com',
            $request->post("data.User.email")
        );
        $this->assertEquals('tested', $request->get("test"));
        $this->assertFalse($request->param('not.known'));
        $this->assertFalse($request->param('unkown'));
    }

}