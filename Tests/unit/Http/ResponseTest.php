<?php

/**
 * ResponseTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Http
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Http;

use Codeception\Util\Stub;
use Slick\Http\Response;

/**
 * ResponseTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Http
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ResponseTest extends \Codeception\TestCase\Test
{

    /**
     * The SUT object
     * @var \Slick\Http\Response
     */
    protected $_response;

    /**
     * A simple response text
     * @var string
     */
    protected $_responseText = <<<EOS
HTTP/1.0 200 OK
status: 200 OK
version: HTTP/1.1
cache-control: private, max-age=0
content-encoding: gzip
content-type: text/html; charset=UTF-8
date: Wed, 07 Aug 2013 18:02:33 GMT

<html>
<body>
    Hello World
</body>
</html>
EOS;

    /**
     * Creates the response for test
     */
    protected function _before()
    {
        parent::_before();
        $this->_response = Response::fromString($this->_responseText);
    }

    /**
     * Unsets the response object 
     */
    protected function _after()
    {
        unset($this->_statusCode);
        parent::_after();
    }

    // tests
    
    /**
     * Try to create a response object from a string.
     * 
     * @test
     */
    public function createResponseFromString()
    {
        $this->assertInstanceOf('\Slick\Http\Response', $this->_response);
        $this->assertEquals(Response::HTTP_10, $this->_response->getVersion());
        $this->assertEquals('HTTP/1.0 200 OK', $this->_response->getStatusLine());
        $this->assertEquals('200', $this->_response->statusCode);
        $body = <<<EOB
<html>
<body>
    Hello World
</body>
</html>
EOB;

        $expected = str_replace("\n", "\r\n", $body);       

        $this->assertEquals($expected, trim($this->_response->body));
        $txt = str_replace("\n", "\r\n", $this->_responseText);
        $this->assertEquals($txt, $this->_response->raw);
        $this->assertEquals('OK', $this->_response->getReasonPhrase());
        $this->assertInstanceOf('\Slick\Http\Response', $this->_response->response);
    }

    /**
     * Set other headers
     *
     * @test
     */
    public function setHeader()
    {
        $headers = array(
            "status" => "200 OK",
            "version" => "HTTP/1.1",
            "cache-control" => "private, max-age=0",
            "content-encoding" => "gzip",
            "content-type" => "text/html; charset=UTF-8",
            "date" => "Wed, 07 Aug 2013 18:02:33 GMT",
            "Pragma" => "no-cache"
        );
        $this->_response->header('Pragma', 'no-cache');
        $this->assertEquals($headers, $this->_response->getHeaders());
    }

    /**
     * change current response
     * 
     * @test
     */
    public function changeResponse()
    {
        $this->_response->setStatusCode(201);
        $this->assertEquals('Created', $this->_response->getReasonPhrase());
        $this->_response->reasonPhrase = 'Updated';
        $this->assertEquals('Updated', $this->_response->getReasonPhrase());
    }

    /**
     * Sendig output headers
     * 
     * @test
     */
    public function sendHeaders()
    {
        $response = @$this->_response->sendHeaders();
        $this->assertSame($response, $this->_response);
        $headers = array(
            "status" => "200 OK",
            "version" => "HTTP/1.1",
            "cache-control" => "private, max-age=0",
            "content-encoding" => "gzip",
            "content-type" => "text/html; charset=UTF-8",
            "date" => "Wed, 07 Aug 2013 18:02:33 GMT",
            "Pragma" => "no-cache"
        );
        $this->_response->header('Pragma', 'no-cache');
        $this->assertEquals($headers, $this->_response->getHeaders());
        $this->assertTrue($this->_response->headersSent);
        $this->_response->sendHeaders();
        $this->assertTrue($this->_response->headersSent);
    }

    /**
     * Sendig output content
     * 
     * @test
     */
    public function sendContent()
    {
        $this->_response->setBody("Hello world");
        ob_start();
        $response = $this->_response->sendContent();
        $content = ob_get_clean();
        $this->assertSame($response, $this->_response);
        $this->assertEquals("Hello world", trim($content));
    }

    /**
     * send response 
     * @test
     */
    public function sentResponse()
    {
        $this->_response->setBody("Hello world 2");
        ob_start();
        $response = @$this->_response->send();
        $content = ob_get_clean();
        $this->assertSame($response, $this->_response);
        $this->assertEquals("Hello world 2", trim($content));
        $resp = $this->_response->send();
        $this->assertSame($resp, $this->_response);
    }

}