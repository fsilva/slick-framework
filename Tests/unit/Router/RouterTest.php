<?php

/**
 * RouterTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Router;


use Slick\Router;
use Codeception\Util\Stub;
use Slick\Router\Route\Simple as SimpleRoute;
use Slick\Router\Route\Regex as RegexRoute;
use Codeception\Module\Slick;

/**
 * RouterTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class RouterTest extends \Codeception\TestCase\Test
{

    /**
     * @var \Slick\Router The SUT isntance
     */
    protected $_router;

    /**
     * @var \Slick\Roters\Route\Simple A simple route
     */
    protected $_route;

    /**
     * Create an instance of a Router.
     */
    protected function _before()
    {
        parent::_before();
        $this->router = new Router();
        $this->route = new SimpleRoute(
            array(
                'pattern' => 'members/:id/read',
                'controller' => 'users',
                'action' => 'read'
            )
        );

        $this->router->addRoute($this->route);
    }

    /**
     * Unsets the router instance after test.
     */
    protected function _after()
    {
        unset($this->router);
        parent::_after();
    }

    // tests
    
    /**
     * Test the base implementation
     * @test
     * @expectedException \Slick\Router\Exception\Implementation
     */
    public function checkImplementation()
    {
        $this->router->_Unimplemented();
    }

    /**
     * Test the ability to add/remove routes
     * @test
     */
    public function addAndRemoveRoutes()
    {
        $this->router->removeRoute($this->route);
        $this->assertEquals(array(), $this->router->routes);
        $this->router->addRoute($this->route);

        $expected = array('members/:id/read' => 'Slick\Router\Route\Simple');
        $this->assertEquals($expected, $this->router->routes);
    }

    /**
     * Route dispatch tests
     * @test
     */
    public function dispatchRoutes()
    {
        $this->router->url = 'news/read/15';
        try {
            $this->router->dispatch();
            $this->fail("This should raise an exception here!");
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Router\Exception\Controller', $e);
        }

        $this->router->url = 'users/edit/15';
        try {
            $this->router->dispatch();
            $this->fail("This should raise an exception here!");
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Router\Exception\Action', $e);
        }

        $this->router->url = 'users/run/15';
        try {
            $this->router->dispatch();
            $this->fail("This should raise an exception here!");
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Router\Exception\Action', $e);
        }

        $this->router->url = 'members/15/read';
        $this->router->dispatch();
        $this->assertEquals(15, \Controllers\Users::$argument);

        $this->assertEquals('run', \Controllers\Users::$before);
        $this->assertEquals('cleanup', \Controllers\Users::$after);

    }

    /**
     * Route dispatch tests
     * @test
     */
    public function dispatchSimpleRouteWithNoKeys()
    {
        $this->router->addRoute(
            new SimpleRoute(
                array(
                    'pattern' => 'users/account',
                    'controller' => 'users',
                    'action' => 'read'
                )
            )
        );
        $this->router->url = 'users/account';
        $this->router->dispatch();
        $this->assertEquals(0, \Controllers\Users::$argument);
    }

    /**
     * Dispatching regular expression routes.
     * @test
     */
    public function dispatchRegExpRoute()
    {
        $regex = new Router\Route\Regex(
            array(
                'pattern' => '/profile/([0-9]+)/contact',
                'keys' => array('id'),
                'controller' => 'users',
                'action' => 'read'
            )
        );

        $this->router->addRoute($regex);
        $this->router->url = '/profile/2/contact';
        $this->router->dispatch();
        $this->assertEquals(2, \Controllers\Users::$argument);

        $this->router->url = '/users/read/2/3';
        $this->router->dispatch();
        $this->assertEquals(2, \Controllers\Users::$argument);
    }

    /**
     * Route implementation test
     * @test
     * @expectedException Slick\Router\Exception\Implementation
     */
    public function checlRouteImplementation()
    {
        $this->route->_Unimplemented();
    }
    
    /**
     * check the exception output
     * @test
     */
    public function checkExceptions()
    {
        $ex = new \Slick\Router\Exception\Controller(
        	"The controller Test was not found",
            ""
        );
        $this->assertTrue(is_string($ex->getDescription()));
        
    }
}