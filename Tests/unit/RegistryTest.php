<?php

/**
 * RegistryTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

use Slick\Registry;
use Codeception\Util\Stub;

class RegistryTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    protected function _after()
    {
        Registry::erase('test');
        Registry::erase('instance');
        parent::_after();
    }

    /**
     * Tests for get method
     * @test
     */
    public function get()
    {
        $this->assertNull(Registry::get('test'));
        $this->assertEquals('Some value', Registry::get('test', 'Some value'));
        Registry::set('test', array(1, 2, 3));
        $this->assertContains(3, Registry::get('test'));
    }

    /**
     * Tests for set method
     * @test
     */
    public function set()
    {
        $obj = new StoreInstance();
        Registry::set('instance', $obj);
        $this->assertEquals('insert', StoreInstance::$method);
        $this->assertSame($obj, Registry::get('instance'));
    }

    /**
     * Tests for erase method
     * @test
     */
    public function erase()
    {
        $obj = new StoreInstance();
        Registry::set('instance', $obj);
        $this->assertEquals('insert', StoreInstance::$method);
        $instances = Registry::getAll();
        Registry::erase('instance');
        $this->assertEquals('remove', StoreInstance::$method);
        $this->assertNull(Registry::get('instance'));
        $this->assertSame($obj, $instances['instance']);
    }

}

class StoreInstance extends \Slick\Base
{

    /**
     * For testing proposes
     * @var string
     */
    public static $method = null;

    /**
     * Callback for registry emtering
     */
    public function registryInsert( )
    {
        self::$method = 'insert';
    }

    /**
     * Callback for registry emtering
     */
    public function registryRemove( )
    {
        self::$method = 'remove';
    }
}
