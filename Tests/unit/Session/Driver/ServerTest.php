<?php

/**
 * ServerTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Session\Driver;

use Codeception\Util\Stub;
use Slick\Session\Driver\Server as ServerDriver;

/**
 * ServerTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ServerTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT instance
     * @var \Slick\Session
     */
    protected $session;

    /**
     * Instanciates the SUT session
     */
    protected function _before()
    {
        parent::_before();
        $this->session = new ServerDriver();
    }

    /**
     * Clear up the session object after 
     */
    protected function _after()
    {
        unset($this->session);
        parent::_after();
    }

    // tests
    
    /**
     * Test the Slick base implementation
     * @test
     * @expectedException \Slick\Session\Exception\Implementation
     */
    public function implementation()
    {
        $driver = $this->session->initialize();
        $this->assertInstanceOf('\Slick\Session\Driver\Server', $driver);
        $this->session->_Unmplemented();
    }

    /**
     * Check get method.
     * @test
     */
    public function getter()
    {
        $_SESSION['slick_test'] = 'one';
        $this->assertNull($this->session->get('two'));
        $this->assertEquals(2, $this->session->get('two', 2));
        $this->assertEquals('one', $this->session->get('test', 2));
    }

    /**
     * Check set method.
     * @test
     */
    public function setterErase()
    {
        $_SESSION['slick_test'] = 'one';
        $obj = $this->session->set('test', 'two');
        $this->assertInstanceOf('\Slick\Session\Driver\Server', $obj);
        $this->assertEquals('two', $_SESSION['slick_test']);

        $this->session->erase('test');
        $this->assertFalse(isset($_SESSION['slick_test']));
    }

}