<?php

/**
 * SessionTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit\Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Session;

use Slick\Session;
use Slick\Session\Message;
use Codeception\Util\Stub;

/**
 * SessionTest
 *
 * @package    Slick
 * @subpackage Tests\Unit\Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class SessionTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * The SUT instance
     * @var \Slick\Session
     */
    protected $session;

    /**
     * Instanciates the SUT session
     */
    protected function _before()
    {
        parent::_before();
        $this->session = new Session();
    }

    /**
     * Clear up the session object after 
     */
    protected function _after()
    {
        unset($this->session);
        parent::_after();
    }

    // tests
    
    /**
     * Test the Slick base implementation
     * @test
     * @expectedException \Slick\Session\Exception\Implementation
     */
    public function implementation()
    {
        $this->session->_Unmplemented();
    }

    /**
     * Session initialization test.
     * @test
     * @expectedException \Slick\Session\Exception\Argument
     */
    public function initialize()
    {
        $driver = $this->session->initialize();
        $this->assertInstanceOf('\Slick\Session\Driver', $driver);

        $this->session->type = null;
        try{
            $this->session->initialize();
            $this->fail("This should raise an exception here");
        } catch(\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Session\Exception\Argument', $e);
        }

        $this->session->type = 'Unknown';
        $this->session->initialize();
    }

    /**
     * Test the session set message.
     * @test
     * @expectedException \Slick\Session\Exception\Argument
     */
    public function setSessionMessage()
    {
        $driver = $this->session->initialize();
        $result = $driver->setMessage('Hello world', Session\Message::TYPE_SUCCESS);
        $this->assertSame($driver, $result);
        $msg = new Session\Message(array('message' => 'This is an info'));
        $driver->setMessage($msg);
        $messages = $driver->getMessages();
        $this->assertEquals(2, count($messages));
        $this->assertTrue(isset($messages[Session\Message::TYPE_SUCCESS]));
        $this->assertInstanceOf(
            '\Slick\Session\Message',
            $messages[Session\Message::TYPE_SUCCESS][0]
        );
        $this->assertEquals(
            Session\Message::TYPE_INFO,
            $messages[Session\Message::TYPE_INFO][0]->getType()
        );
        try {
            $messages[Session\Message::TYPE_INFO][0]->unimplemented();
            $this->fail("An exception should be raised here.");
        } catch (\Slick\Core\Exception $e) {
            $this->assertInstanceOf('\Slick\Session\Exception\Implementation', $e);
        }
        $driver->setMessage(false);
    }

    /**
     * Check get messages and alias methods set*
     * @test
     */
    public function checkAliasMethods()
    {
        $driver = $this->session->initialize();
        $driver->flushMessages();

        $expected = array(
            'info' => array(
                new Message(array('message' => 'info', 'type' => Message::TYPE_INFO))
            ),
            'success' => array(
                new Message(array('message' => 'success', 'type' => Message::TYPE_SUCCESS))
            ),
            'warning' => array(
                new Message(array('message' => 'warning', 'type' => Message::TYPE_WARNING))
            ),
            'error' => array(
                new Message(array('message' => 'error', 'type' => Message::TYPE_ERROR))
            ),
        );

        $info = $driver->setInfo("info");
        $this->assertInstanceOf('\Slick\Session\Driver', $info);

        $success = $driver->setSuccess("success");
        $this->assertInstanceOf('\Slick\Session\Driver', $success);

        $warning = $driver->setWarning("warning");
        $this->assertInstanceOf('\Slick\Session\Driver', $warning);

        $error = $driver->setError("error");
        $this->assertInstanceOf('\Slick\Session\Driver', $error);

        $this->assertEquals($expected, $driver->getMessages());

    }

}