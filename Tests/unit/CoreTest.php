<?php
use Codeception\Util\Stub;
use Slick\Core;
use Slick\Registry;

class CoreTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Application initilization
     * @test
     */
    public function initializeTheApplication()
    {
        $_SERVER['REQUEST_URI'] = '/base/users/login.html?refresh=1';
        $_GET['url'] = 'users/login';
        $_GET['extension'] = 'html';

        $path = dirname(APP_PATH);
        $appPluginsPath = APP_PATH . DIRECTORY_SEPARATOR . 'Plugins';
        $slickPlugins = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'Plugins';

        Core::appInit(APP_PATH);
        $pahts = explode(PATH_SEPARATOR, get_include_path());

        $this->assertContains(APP_PATH, $pahts);
        $this->assertContains($appPluginsPath, $pahts);
        //$this->assertContains($slickPlugins, $pahts);

        $this->assertEquals('/base/', Core::$basePath);
    }

    /**
     * Configuration load by core.
     * @test
     */
    public function loadConfiguration()
    {
        Core::bootstrap();
        $this->assertInstanceOf('Slick\Configuration\Driver\Ini', Registry::get('configuration'));
        $this->assertInstanceOf('Slick\Cache\Driver\Memcached', Registry::get('cache'));
        $this->assertInstanceOf('Slick\Session\Driver\Server', Registry::get('session'));
        $this->assertInstanceOf('Slick\Database\Connector\Sqlite', Registry::get('database_default'));
        $this->assertEquals('2', Core::$debugMode);
    }

    /**
     * Tests the load of a plugin
     * @test
     * @expectedException Slick\Core\Exception\Plugin
     */
    public function loadingPlugin()
    {

        Core::load('Test');
        $expected = new stdClass();
        $expected->name = 'Test plugin';
        $expected->version = '1';
        $plugins = Registry::get('plugins');
        $this->assertEquals($expected, $plugins['Test']);
        

        Core::load('Error');
    }

    /**
     * Testing core exception
     * @test
     */
    public function retrieveExceptioHttpCode()
    {
        $ex = new \Slick\Core\Exception("test");
        $this->assertEquals('500', $ex->getHttpCode());
        $this->assertTrue(is_string($ex->render()));
        $this->assertTrue(is_string($ex->getDescription()));
    }

    /**
     * Testing the core error exception.
     * 
     * @test
     */
    public function checkPhpError()
    {
        $ex = new \Slick\Core\Exception\Error("My test error");

        $this->assertInstanceOf('\Slick\Core\Exception', $ex);
        $this->assertEquals(1, $ex->getSeverity());

        $description = $ex->getDescription();
        $this->assertTrue(is_string($description));
    }

}