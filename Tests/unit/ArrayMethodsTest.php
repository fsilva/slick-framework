<?php

/**
 * ArrayMethodsTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

use Codeception\Util\Stub;
use Slick\ArrayMethods as ArrayMethods;

/**
 * ArrayMethods test case
 * 
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ArrayMethodsTest extends \Codeception\TestCase\Test
{

    /**
     * @test
     */
    public function flattenAnArray()
    {
        $source = array(
            'one' => 'one',
            array(
                'tow' => 'tow',
                array(
                    'three' => 'three'
                )
            )
        );
        $expected = array('one', 'tow', 'three');
        $this->assertEquals($expected, ArrayMethods::flatten($source));
    }

    /**
     * @test
     */
    public function converAnArrayToObject()
    {
        $obj = new stdClass();
        $obj->street = "My street";
        $obj->country = "My country";
        $array = array(
            'firstName' => 'Filipe',
            'lastName' => 'Silva',
            'address' => array(
                'street' => 'My street',
                'country' => "My country"
            )
        );
        $expected = new stdClass();
        $expected->firstName = 'Filipe';
        $expected->lastName = 'Silva';
        $expected->address = $obj;
        $this->assertEquals($expected, ArrayMethods::toObject($array));
    }

    /**
     * Tests the array cleaning method.
     * @test
     */
    public function cleanAnArray()
    {
        $source = array('one', '','tow', "", 'three');
        $expected = array('one', 2 => 'tow', 4 => 'three');
        $this->assertEquals($expected, ArrayMethods::clean($source));
    }

    /**
     * Tests the array element trimming method.
     * @test
     */
    public function trimAnArray()
    {
        $source = array("one\n\t", ' tow ', 'three');
        $expected = array('one', 'tow', 'three');
        $this->assertEquals($expected, ArrayMethods::trim($source));
    }

    /**
     * Tests the retrieve of the first array element.
     * @test
     */
    public function getArrayFirstElement()
    {
        $source = array("one", ' tow ', 'three');
        $expected = 'one';
        $this->assertEquals($expected, ArrayMethods::first($source));
    }

}