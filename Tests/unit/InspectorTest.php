<?php

/**
 * InspectorTest
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

use Slick\Inspector;
use Codeception\Util\Stub;

/**
 * InspectorTest
 *
 * @package    Slick
 * @subpackage Tests\Unit
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class InspectorTest extends \Codeception\TestCase\Test
{
   /**
    * @var \CodeGuy
    */
    protected $codeGuy;

    /**
     * Test class metadata of inspector.
     * @test
     */
    public function classMetadata()
    {
        $dc = new DummyClass();
        $inspector = new Inspector($dc);
        $expected = array(
            '@isDummy' => true,
            '@use' => array('nothing'),
            '@codeCoverageIgnore' => true
        );
        $return = $inspector->getClassMeta();
        $this->assertEquals($expected, $return);

        $ni = new Inspector('NoCommentClass');
        $return = $ni->getClassMeta();
        $this->assertNull($return);
    }

    /**
     * Tests retrieve properties and methods from class.
     * @test
     */
    public function retrievePropertiesAndMethods()
    {
        $expected = array(
            '_name', '_age'
        );
        $dc = new DummyClass();
        $inspector = new Inspector($dc);
        $return = $inspector->getClassProperties();
        $this->assertEquals($expected, $return);
        $expected = array('doSomething', 'doOther');
        $return = $inspector->getClassMethods();
        $this->assertEquals($expected, $return);
    }

    /**
     * Tests property and method metadata of inspector.
     * @test
     */
    public function methodAndPropertyMeta()
    {
        $expected = array(
            '@readwrite' => true,
            '@var' => array('String')
        );
        $dc = new DummyClass();
        $inspector = new Inspector($dc);
        $return = $inspector->getPropertyMeta('_name');
        $this->assertEquals($expected, $return);

        $return = $inspector->getPropertyMeta('_age');
        $this->assertNull($return);

        $expected = array(
            '@return' => array('int The value')
        );
        $return = $inspector->getMethodMeta('doSomething');
        $this->assertEquals($expected, $return);

        $return = $inspector->getMethodMeta('doOther');
        $this->assertNull($return);
    }

}

/**
 * Dummy test classe
 *
 * @isDummy
 * @use nothing
 * @codeCoverageIgnore
 */
class DummyClass
{

    /**
     * This is the name
     * @var String
     * @readwrite
     */
    protected $_name;

    protected $_age;

    /**
     * Do something does nothing
     * @return int The value
     */
    public function doSomething()
    {

    }

    public function doOther()
    {

    }

}

class NoCommentClass
{

}