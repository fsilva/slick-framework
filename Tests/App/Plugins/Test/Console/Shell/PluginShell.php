<?php

/**
 * Slick Framework Slick Console test Shell class file.
 *
 * @package Slick
 * @subpackage Console\Shell
 * @since Version 1.0.0
 * @author Filipe Silva <silvam.filipe@gmail.com>
 *
 * @license Apache License, Version 2.0 (the "License")
 * @copyright 2013 Filipe Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Test\Console\Shell;

use Slick\Base as Base;
use Slick\Console as Console;
use Slick\Console\Colors as Colors;

/**
 * A test shell
 *
 * @package Slick
 * @subpackage Console\Shell
 */
class PluginShell extends Console\Shell {

	/**
	 * This is the main method for this shell.
	 *
	 * This method is called by the console when dispatching the call.
	 */
	public function main() {
	}
}