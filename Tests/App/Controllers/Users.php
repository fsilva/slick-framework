<?php
namespace Controllers;

use Slick\Controller as Controller;

class Users extends Controller {

	public static $argument = 0;
	public static $before = 'none';
	public static $after = 'none';

	/**
	 * @before run
	 * @after cleanup, run
	 */
	public function read($id = 0) {
		self::$argument = $id;
		$this->disableRendering();
	}

	/**
	 * @protected
	 * @once
	 */
	public function run() {
		self::$before = 'run';
	}

	public function cleanup() {
		self::$after = 'cleanup';
	}
}