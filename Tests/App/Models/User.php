<?php
/**
 * Test model
 */
namespace Models;

use Slick\Model as Model;

/**
 * Test user model.
 */
class User extends Model {

	/**
	 * Primary Key 
	 * @readwrite
	 * @column
	 * @primary
	 * @type autonumber
	 * @var integer
	 */
	protected $_id;

	/**
	 * Primary Key 
	 * @readwrite
	 * @column
	 * @type autonumber
	 * @validate required
	 * @var integer
	 */
	protected $_number;

	/**
	 * The username
	 * @column
	 * @type text
	 * @length 128
	 * @index
	 * @readwrite
	 * @validate alpha, min(4)
	 * @var string
	 */
	protected $_username;

	/**
	 * The user password
	 * @column
	 * @type text
	 * @length 256
	 * @readwrite
	 * @validate max(5), alphanumeric
	 * @var string
	 */
	protected $_password;

	/**
	 * User age
	 * @column
	 * @type integer
	 * @readwrite
	 * @validate numeric
	 * @var integer
	 */
	protected $_age;

	/**
	 * Enable/Disable user access
	 * @type boolean
	 * @column
	 * @readwrite
	 * @var boolean
	 */
	protected $_active = true;

	/**
	 * The user birth date
	 * @column
	 * @readwrite
	 * @type datetime
	 * @var string
	 */
	protected $_birthDate;

	/**
	 * The user rank
	 * @column
	 * @type decimal
	 * @readwrite
	 * @var float
	 */
	protected $_rank = 0.0;

	/**
	 * @column
	 * @write
	 * @type text
	 * @var string
	 */
	protected $_type = 'test';
}