<?php

/**
 * Testing model
 */

namespace Models;

use Slick\Model as Model;

/**
 * Tag class
 */
class Tag extends Model
{
    /**
     * Primary Key
     * 
     * @column
     * @primary
     * @type autonumber
     * @readwrite
     * @var integer
     */
    protected $_id;

    /**
     * Tag description
     * 
     * @column
     * @type text
     * @length 256
     * @readwrite
     * @var string
     */
    protected $_description;

    /**
     * The tags
     *
     * @readwrite
     * @hasandbelongstomany Post
     * @var \Slick\Model\DataSet
     */
    protected $_posts;

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        self::$lastQuery = $sql;
        return new TagsConnector();
    }

    /**
     * Returns the database connector.
     * 
     * If no set you, tries to get it from registry.
     *
     * @return Slick\Database\Connector The application database connector.
     *
     * @throws Slick\Model\Exception\Connector If any array accours when
     *   trying to connecto to the database.
     */
    public function getConnector()
    {
        return new TagsConnector();
    }

}



class TagsConnector extends \Slick\Database\Connector\Mysql
{
    public static $lastQuery = null;

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        self::$lastQuery = $sql;
        return new TagsResource();
    }
}

class TagsResource
{
    public function fetch_array($const)
    {
        static $i;
        if (!isset($i)) {
            $i = 0;
        }
        $data = array(
            array(1, 'PHP'),
            array(2, 'Framework'),
            array(3, 'PHP-5'),
            array(4, 'Code'),

        );
        if (isset($data[$i])) {
            return $data[$i++];
        } else {
            unset($i);
            return false;
        }
        
    }

    public $num_rows = 2;

    public function fetch_fields()
    {
        $data = array(
            (Object) array(
                'name' => 'id',
                'table' => 'Tag'
            ),
            (Object) array(
                'name' => 'description',
                'table' => 'Tag'
            ),
        );
        return  $data;

    }
}
