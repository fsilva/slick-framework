<?php

/**
 * Testing model
 */

namespace Models;

use Slick\Model as Model;

/**
 * Post class
 */
class Post extends Model
{
    /**
     * Primary Key
     * 
     * @column
     * @primary
     * @type autonumber
     * @readwrite
     * @var integer
     */
    protected $_id;

    /**
     * The user password
     * 
     * @column
     * @type text
     * @length 256
     * @readwrite
     * @var string
     */
    protected $_title;

    /**
     * The tags
     *
     * @readwrite
     * @hasandbelongstomany Tag
     * @var \Slick\Model\DataSet
     */
    protected $_tags;

    /**
     * Set the mock connector
     */
    public function getConnector()
    {
        return new PostsConnector();
    }
}

class PostsConnector extends \Slick\Database\Connector\Mysql
{
    public static $lastQuery = array();

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * Mocking the execute call to db.
     * 
     * @param string $sql The query to execute.
     * 
     * @return boolean This mocked version returns allways true.
     */
    public function execute($sql)
    {
        self::$lastQuery[] = $sql;
        return new PostsResource();
    }
}

class PostsResource
{
    public function fetch_array($const)
    {
        static $i;
        if (!isset($i)) {
            $i = 0;
        }
        $data = array(
            array(1, 'First Post'),
            array(2, 'Second Post')
        );
        if (isset($data[$i])) {
            return $data[$i++];
        } else {
            unset($i);
            return false;
        }
        
    }

    public $num_rows = 2;

    public function fetch_fields()
    {
        $data = array(
            (Object) array(
                'name' => 'id',
                'table' => 'Post'
            ),
            (Object) array(
                'name' => 'title',
                'table' => 'Post'
            ),
        );
        return  $data;

    }
}
