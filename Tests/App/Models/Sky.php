<?php

/**
 * Test model
 */
namespace Models;

use Slick\Model as Model;

class Sky extends Model
{

	/**
	 * Primary Key 
	 * @readwrite
	 * @column
	 * @primary
	 * @type autonumber
	 * @var integer
	 */
	protected $_id;
}