<?php
/**
 * Test model
 */

namespace Models;

use Slick\Model as Model;

/**
 * The other model in relation
 */
class Banana extends Model
{

	/**
	 * Primary Key 
	 * @readwrite
	 * @column
	 * @primary
	 * @type autonumber
	 * @var integer
	 */
	protected $_id;

}