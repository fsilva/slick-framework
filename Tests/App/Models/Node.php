<?php

/**
 * Test model
 */

namespace Models;

use Slick\Model;

class Node extends Model
{
    /**
     * Primary Key
     * 
     * @column
     * @primary
     * @type autonumber
     * @readwrite
     * @var integer
     */
    protected $_id;

    /**
     * The user password
     * 
     * @column
     * @type text
     * @length 256
     * @readwrite
     * @var string
     */
    protected $_title;

    /**
     * The tags
     *
     * @readwrite
     * @hasandbelongstomany Tag
     * @dependent false
     * @foreignkey article_id
     * @jointable article_tags
     * @relatedforeignkey tag
     * @var \Slick\Model\DataSet
     */
    protected $_tags;
}
