<?php

/**
 * Test model
 */

namespace Models;

class Fruit extends \Slick\Model
{
    
    /**
     * Primary Key 
     * @readwrite
     * @column
     * @primary
     * @type autonumber
     * @var integer
     */
    protected $_id;

    /**
     * Set the mock connector
     */
    public function getConnector()
    {
        return new \Model\HasManyChildsConnector();
    }
}