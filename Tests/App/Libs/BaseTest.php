<?php
/**
 * Unit tests class for Configuration system.
 *
 * @package Slick
 * @subpackage Test\Configuration
 * @since Version 0.1.0
 * @author Filipe Silva <silvam.filipe@gmail.com>
 *
 * @license Apache License, Version 2.0 (the "License")
 * @copyright 2013 Filipe Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Libs;

use Slick\Base as Base;

/**
 * A test class for Slick Base class.
 * 
 * @package Slick
 * @subpackage Test\Configuration
 */
class BaseTest extends Base {
	
	/**
	 * A normal property
	 * @readwrite
	 * @var string
	 */
	protected $_name = '';
	
	/**
	 * A read only property
	 * @read
	 * @var integer
	 */
	protected $_age = 35;

	/**
	 * A write only property
	 * @write
	 * @var string
	 */
	protected $_gender = 'Male';

	/**
	 * Another regular property
	 * @readwrite
	 * @var string
	 */
	protected $_country = 'Portugal';
	
}