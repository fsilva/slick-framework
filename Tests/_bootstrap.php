<?php
// This is global bootstrap for autoloading 

if (session_id() == '') {
    session_start();
}

date_default_timezone_set('UTC');

//Set include path and loads the autoloader class.
$frameworkPath = dirname(dirname(__FILE__));
set_include_path(get_include_path(). PATH_SEPARATOR . $frameworkPath);

if (!defined('FRAMEWORK_APTH')) {
    define('FRAMEWORK_APTH', $frameworkPath);
    define('CORE_PATH', $frameworkPath);
}

require_once CORE_PATH . DIRECTORY_SEPARATOR . 'SplClassLoader.php';
/**
 * A application path for tests.
 */
if (!defined('APP_PATH')) {
    define('APP_PATH', dirname(__FILE__) . '/App');
}

$spl = new SplClassLoader('Slick', CORE_PATH);
$spl->register();

//Initialize the test application
Slick\Core::appInit(APP_PATH);

//Load modules from composer
include 'Vendors/autoload.php';

/**
 * Load Codeception module for Slick functional tests.
 */
require CORE_PATH . '/Tests/Slick/SlickModule.php';