<?php

/**
 * File
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Utility;

use Slick\Base;
use Slick\Utility\Exception;

/**
 * File
 * 
 * File class is an utility for handling file operations
 *  
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class File extends Base
{

    /**
     * The folder fot this file.
     * @readwrite
     * @var \Slick\Utility\Folder
     */
    protected $_folder = null;

    /**
     * The file name
     * @readwrite
     * @var string
     */
    protected $_name = null;

    /**
     * For file existence.
     * @read
     * @var boolean
     */
    protected $_exists = false;

    /**
     * The date/time this file was modified.
     * @read
     * @var DateTime
     */
    protected $_lastModified = null;

    /**
     * Check if file exists and sets the file flags appropriately.
     *
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->refresh();
    }

    /**
     * Reads the content of a file.
     *
     * @throws \Slick\Utility\Exception\Permissions If some error occours when
     *   trying to read it.
     * @return string The file content.
     */
    public function read()
    {
        $this->refresh();
        $contents = @file_get_contents($this->fullPath());
        if ($contents === false) {
            throw new Exception\Permissions(
                "You don't have the necessary permitions " . 
                "to read the file {$this->name} in {$this->_folder}"
            );
        }
        return $contents;
    }

    /**
     * Writes the provided content into the file.
     * 
     * If the file doesn't exists it will be created.
     * 
     * @param string $content The content to write.
     * 
     * @return \Slick\Utility\File A self instance for method chain calls.
     * 
     * @throws \Slick\Utility\Exception\Permissions If some error occours when
     *  trying to write the contents.
     */
    public function write($content)
    {
        $result = @file_put_contents($this->fullPath(), $content);
        if ($result == false) {
            throw new Exception\Permissions(
                "You don't have the necessary permitions to " .
                "write the file {$this->name} into {$this->_folder}"
            );
        }
        return $this;
    }

    /**
     * Adds the content to the end of the file.
     *
     * @param string $content The content to append to the file.
     * 
     * @return \Slick\Utility\File A self instance for method chain calls.
     * 
     * @throws \Slick\Utility\Exception\Permissions If some error occours when
     *  trying to write the contents.
     */
    public function append($content)
    {
        $this->refresh();
        if (!$this->exists) {
            $result = @file_put_contents($this->fullPath(), $content);
        } else {
            $result = @file_put_contents(
                $this->fullPath(),
                $content,
                FILE_APPEND
            );
        }

        if ($result == false) {
            throw new Exception\Permissions(
                "You don't have the necessary permitions to " . 
                "append content to the file {$this->name} in {$this->_folder}"
            );
        }
        return $this;
    }

    /**
     * Returns the full path to the file.
     * 
     * @return string The full path for the file.
     */
    public function fullPath()
    {
        return (String) $this->folder . "/{$this->name}";
    }

    /**
     * Deletes this file from hard disk.
     * 
     * @return \Slick\Utility\File A self instance for method chain calls.
     */
    public function delete()
    {
        $file = $this->fullPath();
        $result = @unlink($file);
        if ($result === false) {
            throw new Exception\Permissions(
                "You don't have the necessary permitions to delete " . 
                "the file {$this->name} in {$this->_folder}"
            );
        }
        $this->_exists = false;
        $this->_lastModified = null;
        return $this;
    }

    /**
     * Refreshes the file information.
     * 
     * @return \Slick\Utility\File A self instance for method chain calls.
     */
    public function refresh()
    {
        clearstatcache();
        if (file_exists($this->fullPath())) {
            $this->_exists = true;
            $fileTime = filemtime($this->fullPath());
            $this->_lastModified = new \DateTime("@{$fileTime}");
        }
        return $this;
    }

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param String $method The method name.
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

}