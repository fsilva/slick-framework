<?php

/**
 * File
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Utility;

use Slick\Base;
use Slick\Inspector;

/**
 * Collection handles collections of objects.
 * This object can be used as an iteraror or an array.
 *
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Collection extends Base implements
    \Iterator, \Countable, \Serializable, \arrayaccess
{

    /**
     * Record position
     * @var integer
     */
    protected $_position = 0;

    /**
     * The list of records of this dataset
     * @write
     * @var array
     */
    protected $_records = array();

    /**
     * General options.
     * @readWrite
     * @var array
     */
    protected $_options;

    /**
     * Takes the pointer back to the beginning of the dataset.
     * to restart the iteration.
     *
     * @return \Slick\Model\DataSet The dataset object for method chain.
     */
    public function rewind()
    {
        $this->_position = 0;
        return $this;
    }

    /**
     * Takes the pointer to the end of the dataset.
     * 
     * @return \Slick\Model\DataSet The dataset object for method chain.
     */
    public function forward()
    {
        $this->_position = $this->count() -1;
        return $this;
    }

    /**
     * Returns the value at the current position of the dataset.
     *
     * @return \Slick\Model The current record
     */
    public function current() 
    {
        return $this->_records[$this->_position];
    }

    /**
     * This should return the current value of the pointer.
     */
    public function key() 
    {
        return $this->_position;
    }

    /**
     * Moves the pointer to the next value in the data set.
     */
    public function next()
    {
        ++$this->_position;
        return $this;
    }

    /**
     * Moves the pointer to the next value in the data set.
     */
    public function previous()
    {
        --$this->_position;
        return $this;
    }

    /**
     * Returns a boolean indicating if the there is data at
     * the current position in the dataset.
     */
    public function valid()
    {
        return isset($this->_records[$this->_position]);
    }

    /**
     * Returns the total recored in the dataset.
     *
     * @return int The number of records in this database
     */
    public function count()
    {
        return count($this->_records);
    }

    /**
     * Serializes current records data.
     *
     * @return string The serialized version of records array.
     */
    public function serialize()
    {
        return serialize($this->_records);
    }

    /**
     * Recovers from serialization
     *
     * @param string $data The serialization data to restore
     */
    public function unserialize($data)
    {
        $this->_records = unserialize($data);
        parent::__construct(array());
    }

    /**
     * Adds a model record to the dataset.
     *
     * @param mixed $object The object to add
     */
    public function add($object)
    {
        $this->_records[] = $object;
    }

    /**
     * Removes the element whit the provided key from the collection.
     * 
     * If no key is provided, the element from the current position
     * will be removed.
     * 
     * @param integer $key The key element to remove.
     * 
     * @return \Slick\Utility\Collection A sef instance for method chain calls.
     */
    public function remove($key = -1)
    {
        if ($key < 0) {
            $key = $this->_position--;
            if ($this->_position < 0) {
                $this->_position = 0;
            }
        }
        unset($this->_records[$key]);
        return $this;
    }

    /**
     * Checks if the dataset is empty
     * 
     * @return boolean [description]
     */
    public function isEmpty()
    {
        return ($this->count() == 0);
    }

    /**
     * Implementing offset assigning for use objbect as an array
     *
     * @param integer      $offset The offset position for the value.
     * @param \Slick\Model $value  The model to add
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->_records[] = $value;
        } else {
            $this->_records[$offset] = $value;
        }
    }

    /**
     * Check if an existing offset exists in the dataset
     *
     * @param integer $offset The offset index to check
     * 
     * @return boolean True if the offset exists in the dataset
     */
    public function offsetExists($offset)
    {
        return isset($this->_records[$offset]);
    }

    /**
     * Removes an element for a give offset.
     *
     * @param integer $offset The offset index to remove
     */
    public function offsetUnset($offset)
    {
        unset($this->_records[$offset]);
    }

    /**
     * Returns the model in the provided offset index.
     *
     * @param integer $offset The offset index to retrieve
     * 
     * @return \Slick\Model The Model object for a given position
     */
    public function offsetGet($offset)
    {
        return isset($this->_records[$offset]) ?
            $this->_records[$offset] :
            null;
    }

    /**
     * Returns the last model from dataset
     * 
     * @return \Slick\Model The Model object in the last position
     */
    public function last()
    {
        if ($this->isEmpty()) {
            return null;
        }

        return $this->_records[$this->count() -1];
    }

    /**
     * Returns the last model from dataset
     * 
     * @return \Slick\Model The Model object in the last position
     */
    public function first()
    {
        if ($this->isEmpty()) {
            return null;
        }

        return $this->_records[0];
    }
}