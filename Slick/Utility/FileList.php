<?php

/**
 * FileList
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Utility;

/**
 * FileList
 * 
 * File List is a collection of object files.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class FileList extends Collection
{

    /**
     * Adds a File to the collection.
     *
     * @param \Slick\Utility\File $object The File to add.
     * 
     * @return \Slick\Utility\FileList The current instance for
     *  method call chain.
     */
    public function add($object) 
    {
        $this->_records[] = $object;
        return $this;
    }

    /**
     * Searches the collection and retrieves the file with the provided name.
     * 
     * @param string $name The file name to search.
     * 
     * @return \Slick\Utility\File The matched file with given name or null
     *  if not found.
     */
    public function findByName($name)
    {
        foreach ($this->_records as $pos => $file) {
            if ($file->name == $name) {
                $this->_position = $pos;
                return $file;
            }
        }
        return null;
    }
}