<?php

/**
 * Folder
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Utility;

use Slick\Base;
use Slick\Utility\Exception;

/**
 * Folder
 * 
 * Folder class is an utility that wrapps a file system folder/directory
 *  
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Folder extends Base
{

    /**
     * Regular expression to validate folder paths.
     */
    const PATH = '/^[\\0-9a-z\/\.\-\_]+$/i';

    /**
     * The folder path
     * @readwrite
     * @var string
     */
    protected $_path;

    /**
     * Flag for directory presence
     * @read
     * @var boolean
     */
    protected $_exists = false;

    /**
     * Flag for folder write permissions
     * @read
     * @var boolean
     */
    protected $_writable = false;

    /**
     * The default mode when craeting new folders
     * @readwrite
     * @var integer
     */
    protected $_defaultMode = 0775;

    /**
     * Tells the system to create is folder isn't found.
     * @readwrite
     * @var boolean
     */
    protected $_createIfNotFound = true;

    /**
     * A collection of files
     * @read
     * @var Slick\Utility\FileList
     */
    protected $_files;

    /**
     * Check if folder exists and sets the folder flags appropriately.
     *
     * By default Slick will try to create recursively the folder if
     * it doesn't exists.
     * 
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     * 
     * @throws \Slick\Utility\Exception\Permissions If folder cant be
     *  created due to invalid or insufficient permissions to create
     *  the folder.
     * @throws \Slick\Utility\Exception\Argument If The path passed to
     *  constructor is an invalid or malformed path string.
     *  Eg. NULL, empty, etc...
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        if (!preg_match(Folder::PATH, $this->_path)) {
            throw new Exception\Argument("Invalid folder path supplied");
        }
        $this->refresh();
    }

    /**
     * Checks if the folder is writeable.
     * 
     * @return boolean True if folder is writable, false otherwise.
     */
    public function isWritable()
    {
        return $this->writable;
    }

    /**
     * Adds a new file to this folder.
     *
     * This accepts a string as a file name or a File object. If File object
     * is given then the internal file folder will be set to this folder and 
     * file contents will be copied for the new file location.
     * 
     * @param \Slick\Utility\File $file A string containing the new file
     *   name to add or an File object.
     *   
     * @return Slick\Utility\Folder A self instance for method chain call.
     */
    public function addFile($file)
    {

        if (is_string($file)) {
            $file = new File(
                array(
                    'name' => $file,
                    'folder' => $this
                )
            );
        } else {
            $file->setFolder($this);
        }

        if (!is_null($this->_files->findByName($file->name))) {
            throw new Exception\FileExists(
                "There's a file with the same name " .
                "in current folder: {$file->name}"
            );
        }
        
        $this->files->add($file);
        return $this;
    }

    /**
     * Adds a folder to current folder path.
     * 
     * @param string $folder The folder name to add.
     * 
     */
    public function addFolder($folder)
    {
        $path = $this->_path . DIRECTORY_SEPARATOR . $folder;
        return new Folder(array('path' => $path));
    }

    /**
     * Checks if a given file exists in this folder.
     * 
     * @param \Slick\Utility\File $file A string containing the file
     *   name or an File object.
     *   
     * @return boolean True if file already exists, false otherwise.
     */
    public function hasFile($file)
    {
        $name = $file;
        if (is_a($name, '\Slick\Utility\File')) {
            $name = $file->name;
        }
        return (boolean) $this->_files->findByName($name);
    }

    /**
     * Deletes a file (fisically) from this folder.
     * 
     * @param \Slick\Utility\File $file A string containing the file
     *   name or an File object.
     *   
     * @return \Slick\Utility\Folder A self instance for method chain call.
     */
    public function deleteFile($file)
    {
        $name = $file;
        if (is_a($name, 'Slick\Utility\File')) {
            $name = $file->name;
        }
        $file = $this->_files->findByName($file->name);
        if (!empty($file)) {
            if ($file->exists) {
                $file->delete();
            }
            $this->files->remove();
        }
        return $this;
    }

    /**
     * Deletes current forlder and all its files.
     * 
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function delete()
    {
        foreach ($this->_files as $file) {
            $this->deleteFile($file);
        }
        return rmdir($this->_path);
    }

    /**
     * Reloads this directory files and settings.
     * 
     * @return \Slick\Utility\Folder A self instance for method chain call.
     */
    public function refresh()
    {

        if (is_dir($this->_path)) {
            $this->_exists = true;
            if (is_writable($this->path)) {
                $this->_writable = true;
            }
        } else if ($this->_createIfNotFound) {
            $old = umask(0);
            if (!@mkdir($this->_path, $this->_defaultMode, true)) {
                throw new Exception\Permissions(
                    "Cannot create folder '{$this->_path}'"
                );
            } else {
                $this->_exists = true;
                $this->_writable = true;
            }
            umask($old);
        }

        $this->_files = new FileList();

        $excludeList = array(".", "..");

        if ($this->_exists) {
            $files = array_diff(scandir($this->_path), $excludeList);
            foreach ($files as $file) {
                $this->_files->add(
                    new File(array('folder' => $this, 'name' => $file))
                );
            }
        }

        return $this;
    }

    /**
     * Returns the path to the folder when requested to conver to string.
     * 
     * @return string The path to the folder.
     */
    public function __toString()
    {
        return $this->_path;
    }

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param String $method The method name.
     * 
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }
}