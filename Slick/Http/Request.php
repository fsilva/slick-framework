<?php

/**
 * Request
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Http
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Http;

use Slick\Base;
use Slick\Http\Exception;

/**
 * Request
 *
 * @package    Slick
 * @subpackage Http
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Request extends Base
{

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * 
     * @return \Slick\Http\Exception\Implementation
     *   The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * A magic method to retrieve known request data.
     * 
     * The properties Request::$data and Request::$params don't exists. They
     * are a simple pointer to the $_Post['data'] and $_GET super globals.
     * The Request::$body or Request::$input will have the request input body.
     * 
     * @return array An array with the containing data, or an empty array if
     *   no data is set in the super globals.
     * 
     * @throws Slick\Core\Exception\Property If the property name you are
     *   requesting doesn't exists or it isn't Request::$data or
     *   Request::$params.
     */
    public function __get($name)
    {
        switch ($name){
            case 'data':
                if (isset($_POST['data'])) {
                    return $_POST['data'];
                }
                return array();

            case 'params':
                return $_REQUEST;

            case 'input':
            case 'body':
                return file_get_contents("php://input");
                
            default:
                return parent::__get($name);
        }
    }

    /**
     * Retrieves the request value with the given name.
     *
     * You can use a '.' notation to retrieve nested values in the $_REQUEST
     * super global. For example Request::param("data.User.name") will points
     * the value in $_REQUEST['data']['User']['name'].
     * 
     * @param string $name The name to retrieve from $_REQUEST super global.
     * 
     * @return mixed The value under the provided name, or false if not found.
     *   Be advised that this method may return a non boolean value that
     *   evaluates false. Allways use the "===" or "!==" operand to evaluate
     *   the results.
     */
    public function param($name)
    {
        return $this->_param($name);
    }

    /**
     * Retrieves the get value with the given name.
     *
     * You can use a '.' notation to retrieve nested values in the $_GET
     * super global. For example Request::param("data.User.name") will points
     * the value in $_GET['data']['User']['name'].
     * 
     * @param string $name The name to retrieve from $_GET super global.
     * 
     * @return mixed The value under the provided name, or false if not found.
     *   Be advised that this method may return a non boolean value that
     *   evaluates false. Allways use the "===" or "!==" operand to evaluate
     *   the results.
     */
    public function get($name)
    {
        return $this->_param($name, $_GET);
    }

    /**
     * Retrieves the post value with the given name.
     *
     * You can use a '.' notation to retrieve nested values in the $_GET
     * super global. For example Request::param("data.User.name") will points
     * the value in $_GET['data']['User']['name'].
     * 
     * @param string $name The name to retrieve from $_GET super global.
     * 
     * @return mixed The value under the provided name, or false if not found.
     *   Be advised that this method may return a non boolean value that
     *   evaluates false. Allways use the "===" or "!==" operand to evaluate
     *   the results.
     */
    public function post($name)
    {
        return $this->_param($name, $_POST);
    }

    /**
     * Searched for the value under the given key.
     *
     * You can use a '.' notation to retrieve nested values in the $value
     * super global.
     * 
     * @param  string $key  The key to retrieve from the super global.
     * @param  string $value The variable where to search in.
     * 
     * @return mixed The value under the provided name, or false if not found.
     *   Be advised that this method may return a non boolean value that
     *   evaluates false. Allways use the "===" or "!==" operand to evaluate
     *   the results.
     */
    protected function _param($key, $value = null)
    {
        $var = (is_null($value)) ? $_REQUEST : $value;

        if (strstr($key, '.')) {
            $parts = explode('.', $key, 2);

            if (!isset($var[$parts[0]])) {
                return false;
            }
            if (is_array($var[$parts[0]])) {
                return $this->_param($parts[1], $var[$parts[0]]);
            }

        } else if (isset($var[$key])) {
            return $var[$key];
        }
        return false;
    }

}