<?php

/**
 * Response
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Http
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Http;

use Slick\Base;

/**
 * Response
 *
 * Response class will allow the developer to interact with an HTTP response.
 *
 * @package    Slick
 * @subpackage Http
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Response extends Base
{

    /**
     * HTTP protocol version 1.0 
     */
    const HTTP_10 = '1.0';

    /**
     * HTTP protocol version 1.1
     */
    const HTTP_11 = '1.1';

    /**
     * The HTTP version used.
     * @readwrite
     * @var string
     */
    protected $_version = '1.1';

    /**
     * The HTTP status code
     * @readwrite
     * @var string
     */
    protected $_statusCode = '200';

    /**
     * The HTTP status line
     * @readwrite
     * @var string
     */
    protected $_statusLine = 'HTTP/1.1 200 OK';

    /**
     * The response raw
     * @readwrite
     * @var string
     */
    protected $_raw = <<<EOS
HTTP/1.0 200 OK
status: 200 OK
version: HTTP/1.0


EOS;

    /**
     * Response HTTP headers
     * @readwrite
     * @var array
     */
    protected $_headers = array(
        'status' => '200 OK',
        'version' => 'HTTP/1.0'
    );

    /**
     * Response body
     * @readwrite
     * @var string
     */
    protected $_body = '';

    /**
     * Response reason phrase
     * @readwrite
     * @var string
     */
    protected $_reasonPhrase = null;

    /**
     * A flag that determine if the headers where sent
     * @read
     * @var boolean
     */
    protected $_headersSent = false;
    
    /**
     * A flag that determine if the content was sent
     * @read
     * @var boolean
     */
    protected $_contentSent = false;

    /**
     * @var array Recommended Reason Phrases
     */
    protected $_recommendedReasonPhrases = array(
        // INFORMATIONAL CODES
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        // SUCCESS CODES
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        // REDIRECTION CODES
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy', // Deprecated
        307 => 'Temporary Redirect',
        // CLIENT ERROR
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        // SERVER ERROR
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    );

    /**
     * Sets the response headers
     *
     * The headers are set as an array of key/value pairs where the key is the
     * header name and the value is the header value.
     * This setter will merge the provided headers with the existing ones.
     * 
     * @param array $headers An array of key/value pairs for header
     *  names and values.
     *
     * @return \Slick\Http\Response A self instance for method call chain.
     */
    public function setHeaders($headers)
    {
        $this->_headers = array_merge(
            $this->_headers,
            $headers
        );
        return $this;
    }

    /**
     * Sets the status line and the version and code for this response.
     * 
     * @param string $statusLine The first line of the http response.
     *
     * @return \Slick\Http\Response A self instance for method call chain.
     */
    public function setStatusLine($statusLine)
    {
        preg_match(
            '/^HTTP\/(1\.0|1\.1) ([0-9]{3}) (.*)/',
            $statusLine,
            $matches
        );

        $this->_statusCode = $matches[2];
        $this->_version = $matches[1];
        $this->_statusLine = $statusLine;

        return $this;
    }

    /**
     * Get HTTP status message
     *
     * @return string
     */
    public function getReasonPhrase()
    {
        if ($this->_reasonPhrase == null) {
            return $this->_recommendedReasonPhrases[$this->statusCode];
        }
        return $this->_reasonPhrase;
    }

    /**
     * Sets a header, overriding if it was already defined.
     * 
     * @param string $name  The header name.
     * @param string $value The header value.
     * 
     * @return \Slick\Http\Response A self instance for method call chain.
     */
    public function header($name, $value)
    {
        $this->_headers[$name] = $value;
        return $this;
    }

    /**
     * Returns current response content.
     * 
     * @return string The content ouput string.
     */
    public function getContent()
    {
        return $this->body;
    }

    /**
     * Send HTTP response
     * 
     * @return \Slick\Http\Response A self instance for method call chain.
     */
    public function sendHeaders()
    {
        if ($this->headersSent) {
            return $this;
        }

        $status = $this->getStatusLine();
        header($status);

        foreach ($this->_headers as $key => $value) {
            header("{$key}: {$value}", true);
        }

        $this->_headersSent = true;
        return $this;
    }

    /**
     * Send content
     *
     * @return \Slick\Http\Response A self instance for method call chain.
     */
    public function sendContent()
    {
        if ($this->contentSent) {
            return $this;
        }

        echo $this->getContent();
        $this->_contentSent = true;
        return $this;
    }

    /**
     * Send HTTP response
     * 
     * @return \Slick\Http\Response A self instance for method call chain.
     */
    public function send()
    {
        $this->sendHeaders()
            ->sendContent();
        return $this;
    }

    /**
     * Creates a response object from a give response text.
     * 
     * @param string $responseText The response raw string to parse.
     * 
     * @return \Slick\Http\Response A Reponse object.
     */
    public static function fromString($responseText)
    {
        $options = array();
        $txt = str_replace("\n", "\r\n", $responseText);
        $options['raw'] = $txt;

        $parts = explode("\r\n\r\n", $txt);
        $rawHeaders = explode("\r\n", $parts[0]);

        $options['statusLine'] = array_shift($rawHeaders);

        $headers = array();
        foreach ($rawHeaders as $h) {
            preg_match('/^(.*)\: (.*)/', $h, $matches);
            $headers[$matches[1]] = trim($matches[2]);
        }
        $options['headers'] = $headers;

        $options['body'] = isset($parts[1]) ? trim($parts[1]) : null;

        return new Response($options);
    }

    /**
     * Sets the status code and status header for the provided code.
     * 
     * @param integer $code The status code to set.
     */
    public function setStatusCode($code)
    {
        $this->_statusCode = $code;
        $this->_reasonPhrase = $this->_recommendedReasonPhrases[$code];
        $this->header('status', "{$this->_statusCode} {$this->_reasonPhrase}");
    }

}   