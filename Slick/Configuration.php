<?php

/**
 * Configuration
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base as Base;
use Slick\Events as Events; 
use Slick\Configuration as Configuration;
use Slick\Configuration\Exception as Exception;

/**
 * Configuration
 * 
 * Configuration is a factory class that initiates cache driver that you can
 * work with. Before initialize the driver you have to set the driver type
 * you want to use. Slick core has implemented the 'ini' type for parsing
 * "*.ini" files.
 *
 * @package    Slick
 * @subpackage Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
Class Configuration extends Base
{

    /**
     * Configuration type.
     * @readwrite
     * @var string
     */
    protected $_type;

    /**
     * A list of options for configuration.
     * @readwrite
     * @var array
     */
    protected $_options;

    /**
     * Overrides the base implementaion exception calling.
     * 
     * @param string $method The method name.
     * 
     * @return Slick\Configuration\Exception\Implementation
     *  The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Creates a self::$_type type configuration driver
     *
     * Creates the configuration driver passing self::$_options to driver
     * constructor. This method fires the "configuration.before.initialize" and
     * "configuration.after.initialize" events before and after driver criation 
     * instructions respectively.
     * "configuration.before.initialize" will pass the $configuration argument
     * - the current configuration instance while 
     * "configuration.after.initialize" will pass the $configuration and the
     * instanciated driver as $driver.
     * 
     * @return Slick\Configuration\Driver A configuration driver object.
     *
     * @throws Slick\Configuration\Exception\Argument If the self::$_type type
     *   driver is not set or cannot be found. 
     */
    public function initialize()
    {
        if (!$this->_type) {
            throw new Exception\Argument("Undefined configuration type");
        }

        Events::fire('configuration.before.initialize', array($this));

        switch ($this->type) {
            case 'ini':
                $driver = new Configuration\Driver\Ini($this->options);
                break;
                
            default:
                throw new Exception\Argument("Invalid configuration type");
        }
        Events::fire('configuration.after.initialize', array($this, $driver));
        return $driver;
    }
}
