<?php

/**
 * Database
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base;
use Slick\Events;
use Slick\Database;
use Slick\Database\Exception;

/**
 * Framework database abstraction layer
 *
 * @package    Slick
 * @subpackage Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Database extends Base
{

    /**
     * Database type name
     * @readwrite
     * @var string
     */
    protected $_type;

    /**
     * Options to pass to connector initialization.
     * @readwrite
     * @var array
     */
    protected $_options;

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * 
     * @return \Slick\Database\Exception\Implementation
     *  The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Initializes an database connecto for self::$_type.
     *
     * It fires the 'database.before.initialize' and
     * 'database.after.initialize' events using parameters:
     *     $database  -> The database instance it self;
     *     $connector -> The instantiated datanase connector.
     *             
     * @return \Slick\Database\Connector The database connector instance.
     */
    public function initialize()
    {
        if (!$this->_type) {
            throw new Exception\Argument("Undefined connector type");
        }

        Events::fire('database.before.initialize', array($this));
        switch ($this->_type) {
            case 'mysql':
                $connector = new Database\Connector\Mysql($this->options);
                break;

            case 'sqlite':
                $connector = new Database\Connector\Sqlite($this->options);
                break;

            default:
                throw new Exception\Argument("Invalid connector type");
        }
        Events::fire('database.after.initialize', array($this, $connector));
        return $connector;
    }
}
