<?php

/**
 * Create command shell
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console\Shell;

use Slick\Console\Shell as Shell;
use Slick\Console\Task\CreateHelp as CreateHelp;
use Slick\Console\Task\CreateApp as CreateApp;

/**
 * Create command shell.
 *
 * Create shell is used to create things with command line console.
 *
 * @package    Slick
 * @subpackage Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Create extends Shell
{

    /**
     * Create help taks
     *
     * @readwrite
     * @var Slick\Console\Task\CreateHelp
     */
    protected $_createHelp = null;

    /**
     * Create help taks
     *
     * @readwrite
     * @var Slick\Console\Task\CreateApp
     */
    protected $_createApp = null;

    /**
     * This is a callback that is called before the Shell::main() call
     */
    public function prepare()
    {
        $this->setDescription(
            "This shell helps you to create things like new applications, "
            . "controllers, models, views, etc."
        );
        $this->_createApp = new CreateApp(array('shell' => $this));
        $this->_createHelp = new CreateHelp(array('shell' => $this));
    }

    /**
     * This is the main method for this shell.
     *
     * This method is called by the console when dispatching the call.
     *
     * @return void
     */
    public function main()
    {
        $task = lcfirst($this->_getTask());
        $this->$task->run();    
    }

    /**
     * Retrieves the requested task.
     * 
     * @return string Task Class name.
     */
    private function _getTask()
    {
        $arguments = $this->console->args;
        $task = isset($arguments[1]) ? $arguments[1] : 'CreateHelp';

        switch ($task) {
            case 'app':
                return 'CreateApp';
            
            default:
                return 'CreateHelp';
        }
    }
}