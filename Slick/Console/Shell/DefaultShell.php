<?php

/**
 * Default command shell
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console\Shell;

use Slick\Console as Console;
use Slick\Console\Colors as Colors;
use Slick\Console\Task\ShellList as ShellList;

/**
 * Default command shell.
 * 
 * This is the default shell executed when no shell is specified.
 * It displays the help informations on how to use the console and
 * its commands. It also display a list of commands that are available
 * in the Slick core and in the application you are runnig the console in.
 *
 * @package    Slick
 * @subpackage Console\Shell
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class DefaultShell extends Console\Shell
{
    /**
     * ShellList task.
     * @read
     * @var Slick\Console\Task\ShellList
     */
    protected $_shellList = null;

    /**
     * Main shell execution method.
     */
    public function main()
    {
        $usage = " Usage: slick «SHELL» [ARGS]... [OPTIONS]...";
        if ($this->console->colors) {
            $usage = Colors::set($usage, 'white');
        }
        $this->out($usage)
            ->bLine();

        $this->shellList->run();

        $this->bLine()
            ->section("Options for all shells:")

            ->out(" -q, --quiet\t\tRuns console in quiet mode. No output.")
            ->out(" -v, --verbose\t\tOutputs all log messages.")
            ->out(" -c, --colors\t\tEnables ANSI colors in output.")
            ->out(" -p, --plugin=<name>\tTells console to use the plugin path")
            ->out("     -p<name>\t\t  especified by the provided <name>.");
    }

    /**
     * Sets the name and decsription for this shell.
     */
    public function prepare()
    {
        parent::prepare();
        $this->_name = "Slick Console";
        $this->_description .= "Here you can find the available shells from "
            ."Slick core and also from your application.";
        $this->_shellList = new ShellList(array('shell' => $this));
    }
}