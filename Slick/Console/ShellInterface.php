<?php

/**
 * Console Shell Interface
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console;

/**
 * Interface for a console shell class.
 *
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
interface ShellInterface
{

    /**
     * This is a callback that is called before the Shell::main() call
     */
    public function prepare();

    /**
     * This is the main method for this shell.
     *
     * This method is called by the console when dispatching the call.
     */
    public function main();
}