<?php

/**
 * Console Task
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console;

use Slick\Base as Base;

/**
 * Slick console task is an abstract console class for all tasks.
 *
 * @package Slick
 * @subpackage Console
 */
abstract class Task extends Base implements TaskInterface
{

    /**
     * The calling shell.
     * 
     * @readwrite
     * @var Slick\Console\Task
     */
    protected $_shell = null;

    /**
     * Prints out a message if the console isn't runnig in quiet mode.
     * 
     * This should always be used for outputing information.
     * 
     * @param string $message The mesage to print out.
     * 
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function out($message = null)
    {
        $this->_shell->out($message);
        return $this;
    }

    /**
     * Prompts the user for input.
     * 
     * @param string $message The message or questions that will be printed
     *   before the prompt cursor.
     *
     * @codeCoverageIgnore
     * @return string The input from the user.
     */
    public function prompt($message = '$')
    {
        return $this->console->prompt($message);
    }

    /**
     * Prints out an horizontal bar.
     *
     * @codeCoverageIgnore
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function hBar()
    {
        $this->console->hBar();
        return $this;
    }

    /**
     * Prints out a blank line.
     * 
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function bLine()
    {
        $this->shell->bLine();
        return $this;
    }

    /**
     * Logs and prints out log messages.
     * 
     * @param string $message The message to log.
     * @param string $level   The log level for this message. Available levels
     *   for console are "info", "debug", "warning" and "error".
     *   Default is "info"
     * @param string $body    A console output color for the message.
     * 
     * @see  Slick\Console\Colors
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function log($message, $level = 'info', $body="light_gray",
        $force = false) 
    {
        $this->shell->log($message, $level, $body, $force);
        return $this;
    }

    /**
     * Logs an info level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function info($message)
    {
        $this->shell->info($message);
        return $this;
    }

    /**
     * Logs a debug level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function debug($message)
    {
        $this->shell->debug($message);
        return $this;
    }

    /**
     * Logs a warning level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function warning($message)
    {
        $this->shell->warning($message);
        return $this;
    }

    /**
     * Logs an error level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Task A self instance for method chain calls.
     */
    public function error($message)
    {
         $this->shell->error($message);
         return $this;
    }   

}