<?php

/**
 * Slick Framework console dispatcher.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console;

use Slick\Base as Base;
use Slick\Console\Colors as Colors;

/**
 * A shell is colections of tasks to do console actions.
 *
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Shell extends Base implements ShellInterface
{

    /**
     * The calling console object
     * @readwrite
     * @var Slick\Console
     */
    protected $_console;

    /**
     * Stores the shell name.
     * @readwrite
     * @var string
     */
    protected $_name = null;

    /**
     * A shor description for this shell
     * @readwrite
     * @var string
     */
    protected $_description = null;

    /**
     * A list of available tasks for this shell.
     * @var array
     */
    protected $_tasks = array();

    /**
     * This is a callback that is called before the Shell::main() call.
     */
    public function prepare() 
    {

    }

    /**
     * Prints out a message if the console isn't runnig in quiet mode.
     * 
     * This should always be used for outputing information.
     * 
     * @param string $message The mesage to print out.
     * @param string $color   The color labels to apply to the message.
     * 
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function out($message = null, $color = false)
    {
        $this->console->out($message);
        return $this;
    }

    /**
     * Prompts the user for input.
     * 
     * @param string $message The message or questions that will be printed
     *   before the prompt cursor.
     *
     * @codeCoverageIgnore
     * @return string The input from the user.
     */
    public function prompt($message = '$')
    {
        return $this->console->prompt($message);
    }

    /**
     * Prints out an horizontal bar.
     *
     * @codeCoverageIgnore
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function hBar()
    {
        $this->console->hBar();
        return $this;
    }

    /**
     * Prints out a blank line.
     * 
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function bLine()
    {
        $this->console->bLine();
        return $this;
    }

    /**
     * Retrieves the shell name.
     * 
     * @return string Returns the shell name.
     */
    public function getName()
    {
        if (is_null($this->_name)) {
            $name = get_class($this);
            $parts = explode('\\', $name);
            $this->_name = end($parts);
        }
        return $this->_name;
    }

    /**
     * Prints out a section header with colors if enabled.
     * 
     * @param string $name The section name to ouput
     * @param string $char A seperator charecter or booelan false if you 
     *   don't want to output a seperator. Default to "-".
     *   
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function section($name, $char = '-')
    {
        $line = '';
        $message = "{$name}";
        if ($char) {
            for ($i=0; $i <= strlen($name); $i++) {
                $line .= $char;
            }
            $message .= "\n{$line}";
        }
        
        if ($this->console->colors) {
            $message = Colors::set($message, 'light_gray');
        }
        return $this->out($message);
    }

    /**
     * Logs and prints out log messages.
     * 
     * @param string $message The message to log.
     * @param string $level   The log level for this message. Available levels
     *   for console are "info", "debug", "warning" and "error".
     *   Default to "info"
     * @param string $body    A console output color for the message.
     * 
     * @see  Slick\Console\Colors
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function log($message, $level = 'info', $body="light_gray",
        $force = false
    )
    {
        $this->console->log($message, $level, $body, $force);
        return $this;
    }

    /**
     * Logs an info level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function info($message)
    {
        $this->console->info($message);
        return $this;
    }

    /**
     * Logs a debug level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function debug($message)
    {
        $this->console->debug($message);
        return $this;
    }

    /**
     * Logs a warning level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function warning($message)
    {
        $this->console->warning($message);
        return $this;
    }

    /**
     * Logs an error level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console\Shell A self instance for method chain calls.
     */
    public function error($message)
    {
         $this->console->error($message);
         return $this;
    }   
    
}