<?php

/**
 * Slick web application script.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    App
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

use Slick\Core as WebApp;

/**
 * Application path
 */
define('APP_PATH', dirname(dirname(__FILE__)));

/**
 * Set the include path for framework core classes.
 */
define('CORE_PATH', '{{ core_path }}');

// Bootstrap framework.
require_once CORE_PATH . DIRECTORY_SEPARATOR . 'AppBootstrap.php';

// Setup application
WebApp::appInit(APP_PATH);

// Run application bootstrap files
if (is_file(APP_PATH.'/Configuration/Bootstrap.php')) {
    include APP_PATH.'/Configuration/Bootstrap.php';
}

// Application bootstrap
WebApp::bootstrap();

// Run the application request
WebApp::run();
    

