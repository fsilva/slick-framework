#Remove Cookie from all static content (except HTML as javascript could use it)
<FilesMatch "\.(html|htm|js|css|gif|jpe?g|png|pdf|txt|zip|7z|gz|jar|war|tar|ear|java|pac)$">
    <IfModule header_module>
        Header unset Cookie
    </IfModule>
</FilesMatch>

<IfModule mod_rewrite.c>
    RewriteEngine on
    RewriteRule    ^$    Webroot/    [L]
    RewriteRule    (.*) Webroot/$1    [L]
</IfModule>