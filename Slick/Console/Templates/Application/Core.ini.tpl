;
; Application core configuration file
;

; Debug mode
; --------------
;   0 => Production: No debugging, log errors and don't show errors;
;   1 => Quality: No debugging, show errors and log errors;
;   2 => Development: Enable debug on errors witch will ouput error
;        messages, stack traces and will output all PHP errors,
;        warnings, infos, strict, etc. Recomended for development.
;
;   Default: 0
;
debugMode = 2

; System Cache
; ----------------
;   Default cache configuration for application core operations.
;   By default the Slick Framework will use 'memcached' if available.
;   If not it will fall back to 'file' cache type.
;
;   If you have more then one Slick application running in the same
;   server, it is recommended that you change the prefix to avoid
;   name colisions between applications.
;
cache.default.type = "memcached"
cache.default.host = "127.0.0.1"
cache.default.prefix = "slick-app:"
cache.default.port = 11211

; Session
; ----------
;   Default session handling configuration.
;   Slick has suppor for default PHP session only.
;
session.default.type = "server"
session.default.name = "ssid"
session.default.domain = null
session.default.lifetime = 0
