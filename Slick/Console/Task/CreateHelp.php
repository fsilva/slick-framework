<?php

/**
 * Create help command task
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console\Task
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console\Task;

use Slick\Console\Task as Task;

/**
 * Create Help shell task.
 * 
 * This task is called whe you run "slick create help" on the console.
 * It displays information on how you can use the create shell and its commands
 * and also all available tasks that create command can do.
 *
 * @package    Slick
 * @subpackage Console\Task
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class CreateHelp extends Task
{

    /**
     * Runs the task.
     * 
     * Run is called by the shell whenever the taks in needed to run.
     * 
     * @param array $args Arguments for this run.
     * 
     * @return boolean True if execution was successfull, false otherwise.
     */
    public function run($args = array())
    {
        $this->out("Create help!");
    }
}