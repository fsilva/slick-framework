<?php

/**
 * Create app command task
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console\Task
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console\Task;

use Slick\Console\Task as Task;
use Slick\Console\Colors as Colors;
use Slick\Console as Console;
use Slick\Utility\Folder as Folder;
use Slick\Utility\File as File;
use Slick\Template as Template;

/**
 * CreateApp task.
 * 
 * This taskis called whe you run the "slick create app" command in the command
 * line. It will create the firectory tree, base files and copy all the CSS
 * and JS files needed to run a Slick application.
 *
 * @package    Slick
 * @subpackage Console\Task
 * @codeCoverageIgnore
 */
class CreateApp extends Task
{

    /**
     * Application folder.
     * 
     * @readwrite
     * @var Slick\Utility\Folder.
     */
    protected $_appPath = null;

    /**
     * A template engine to render the content for files.
     * 
     * @readwrite
     * @var Slick\Template
     */
    protected $_template = null;

    /**
     * Run is called by the shell whenever the taks in needed to run.
     * 
     * @param array $args Arguments for this run.
     * 
     * @return boolean True if execution was successfull, false otherwise.
     */
    public function run($args = array())
    {
        $name = $this->_getAppName();
        if (!$name) {
            return false;
        }
        $path = Console::$workingPath . DIRECTORY_SEPARATOR . $name;
        if (is_dir($path)) {
            $this->error("Directory name already exists");
            $this->out("You need to type a new application name.");
            return false;
        }
        $this->_appPath = new Folder(array('path' => $path));

        $this->_template = new Template(array('type' => 'twig'));

        $this->_createDirectoryTree();
        return true;
    }

    /**
     * Creates the directory estructure for the new app.
     *
     * @return void 
     */
    protected function _createDirectoryTree()
    {
        $this->out("Creating directory tree...");
        $this->_createConfiguration();

        $this->out("Creating default controller...");
        $controllers = $this->appPath->addFolder('Controllers');
        $this->_createControllers($controllers);

        $console = $this->appPath->addFolder('Console');
        $console->addFolder('Shell');
        $console->addFolder('Task');

        $this->appPath->addFolder('Libs');
        $this->appPath->addFolder('Models');
        $this->appPath->addFolder('Tests');

        $tmp = $this->appPath->addFolder('Tmp');
        chmod((string) $tmp, 0777);

        $this->out("Creating default layout and views...");
        $views = $this->appPath->addFolder('Views');
        $this->_createViews($views);

        $this->out("Setting up the web application...");
        $webroot = $this->appPath->addFolder('Webroot');
        $this->_createWebroot($webroot);

        $templateFolder = $this->_templateFolder();
        $this->appPath->addFile('.htaccess');

        $this->appPath->files
            ->findByName('.htaccess')
            ->write(
                $this->template
                    ->parse('Application/htaccess.root.tpl')
                    ->process()
            );

        $this->bLine()
            ->out("Your new application was successfully created!")
            ->out("Change directory to {$this->appPath} to start coding.")
            ->bLine();
    }

    /**
     * Creates the conficuration files.
     */
    protected function _createConfiguration()
    {
        $config = $this->appPath->addFolder('Configuration');
        $config->addFile('Bootstrap.php')
            ->addFile('Core.ini')
            ->addFile('Database.ini');

        $templateFolder = $this->_templateFolder();

        $config->files
            ->findByName('Bootstrap.php')
            ->write(
                $this->template
                    ->parse('Application/Bootstrap.php.tpl')
                    ->process()
            );

        $config->files
            ->findByName('Core.ini')
            ->write(
                $this->template
                    ->parse('Application/Core.ini.tpl')
                    ->process()
            );

        $config->files
            ->findByName('Database.ini')
            ->write(
                $this->template
                    ->parse('Application/Database.ini.tpl')
                    ->process()
            );
    }

    /**
     * Creates the default controller files.
     * 
     * @param Slick\Utility\Folder $ctrl The controllers folder.
     */
    protected function _createControllers($ctrl)
    {
        $templateFolder = $this->_templateFolder();
        $ctrl->addFile('Pages.php');

        $ctrl->files
            ->findByName('Pages.php')
            ->write(
                $this->template
                    ->parse('Application/Pages.php.tpl')
                    ->process()
            );
    }

    /**
     * Creates the default view and layout files.
     * 
     * @param Slick\Utility\Folder $ctrl The views folder.
     */
    protected function _createViews($views)
    {
        $templateFolder = $this->_templateFolder();
        $layouts = $views->addFolder('Layouts');
        $layouts->addFile('default.html');

        $layouts->files
            ->findByName('default.html')
            ->write(
                $this->template
                    ->parse('Application/layout.html.tpl')
                    ->process()
            );

        $pages = $views->addFolder('Pages');
        $pages->addFile('home.html');
        
        $pages->files
            ->findByName('home.html')
            ->write(
                $this->template
                    ->parse('Application/home.html.tpl')
                    ->process()
            );
    }

    /**
     * Creates the default webroot files and copies the css, js and img files.
     * 
     * @param Slick\Utility\Folder $ctrl The webroot folder.
     */
    protected function _createWebroot($webroot)
    {
        $templateFolder = $this->_templateFolder();
        $webroot->addFolder('files');
        $css = $webroot->addFolder('css');
        $js = $webroot->addFolder('js');
        $img = $webroot->addFolder('img');

        $srcCss = $templateFolder->addFolder('css');
        foreach ($srcCss->files as $file) {
            copy($file->fullPath(), $css->getPath().'/'.$file->getName());
        }

        $srcJs = $templateFolder->addFolder('js');
        foreach ($srcJs->files as $file) {
            copy($file->fullPath(), $js->getPath().'/'.$file->getName());
        }

        $srcImg = $templateFolder->addFolder('img');
        foreach ($srcImg->files as $file) {
            copy($file->fullPath(), $img->getPath().'/'.$file->getName());
        }

        copy($templateFolder.'/favicon.ico', $webroot.'/favicon.ico');

        $webroot->addFile('.htaccess');
        
        $webroot->files
            ->findByName('.htaccess')
            ->write(
                $this->template
                    ->parse('Application/htaccess.web.tpl')
                    ->process()
            );

        $webroot->addFile('index.php');
        
        $webroot->files
            ->findByName('index.php')
            ->write(
                $this->template
                    ->parse('Application/index.php.tpl')
                    ->process(array('core_path' => CORE_PATH))
            );

    }

    /**
     * Retrieves the template location folder.
     * 
     * @return Slick\Utility\Folder The template folder.
     */
    protected function _templateFolder()
    {
        return new Folder(
            array(
                'path' => dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR
                    . 'Templates' . DIRECTORY_SEPARATOR
                    . 'Application'
            )
        );
    }

    /**
     * Retrieves the application.
     * 
     * @return string|boolean The application name or boolean false if invalid.
     */
    protected function _getAppName()
    {
        $args = $this->shell->console->args;
        $name = isset($args[2]) ? $args[2] : null;

        if (empty($name)) {
            $this->error("You must enter an application name.");

            $usage = " Usage: slick create app <app-name>";
            if ($this->shell->console->colors) {
                $usage = Colors::set($usage, 'white');
            }
            $this->bLine()->out($usage)
                ->bLine();
            return false;
        }
        return $name;
    }
}