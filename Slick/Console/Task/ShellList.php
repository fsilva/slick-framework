<?php

/**
 * Shell list command task
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console\Task
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Console\Task;

use Slick\Console as Console;

/**
 * Shell names list.
 * 
 * This task is used by default shell to search and display all shell names
 * in the slick core or in the application you are runnig the console in.
 *
 * @package    Slick
 * @subpackage Console\Task
 * @author     Filipe Silva <silvam.filipe@gm
 */
class ShellList extends Console\Task
{

    /**
     * The list of available shells
     * @read
     * @var array
     */
    protected $_shells = array();

    /**
     * Run is called by the shell whenever the taks in needed to run.
     * 
     * @param array $args Arguments for this run.
     * 
     * @return boolean True if execution was successfull, false otherwise.
     */
    public function run($args = array())
    {
        $this->shell->section("Available Shells:");
        $this->_getAppShellNames();
        $this->out(" ". implode(',', $this->_shells));
        return true;
    }

    /**
     * Retrieves the application/core shell names.
     * 
     * @return Slick\Console\Task\ShellList A self instance for 
     *   chaining method calls.
     */
    protected function _getAppShellNames()
    {
        $dir = CORE_PATH . '/Slick/Console/Shell';
        $app = Console::$workingPath . '/Console/Shell';
        $this->_readShellFiles($dir)->_readShellFiles($app);
        
        return $this;
    }

    /**
     * Reads shell files for provided path.
     *
     * This is a simple method that will iterate the provided directory $path
     * and retrieve the file names as used on the slick console calls.
     * It will ignore the DafaultShell.php file as this is an help shell that
     * will be called if no arguments are type in the console request.
     * The file names, or the shell names will be stored in self::$_shells
     * array. So every call to this method will add the available nasmes
     * to the self::$_shells array.
     * 
     * @param  string $path The directory path where to look for the files.
     * 
     * @return Slick\Console\Task\ShellList A self instance for 
     *   chaining method calls.
     */
    protected function _readShellFiles($path)
    {
        if (!is_dir($path)) {
            return $this;
        }
        $files = array_diff(
            scandir($path),
            array('.', '..', 'DefaultShell.php')
        );
        foreach ($files as $file) {
            $parts = explode('.', $file);
            $this->_shells[] = $parts[0];
        }
        return $this;
    }
}