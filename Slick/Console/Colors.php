<?php

/**
 * Slick Framework console dispatcher.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */


namespace Slick\Console;

/**
 * Colors is an utility class to set colored output in commnad line console.
 *
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Colors
{

    /**
     * The ANSI codes.
     * 
     * @var array
     */
    protected static $_ansiCodes = array(
        "off"        => 0,
        "bold"       => 1,
        "italic"     => 3,
        "underline"  => 4,
        "blink"      => 5,
        "inverse"    => 7,
        "hidden"     => 8,
        "black"      => 30,
        "gray"       => '01;30',
        "red"        => '01;31',
        "green"      => '01;32',
        "yellow"     => '01;33',
        "blue"       => '01;34',
        "magenta"    => '01;35',
        "cyan"       => '01;36',
        "white"      => '01;37',
        "light_gray"       => '00;37',
        "light_red"        => '00;31',
        "light_green"      => '00;32',
        "light_yellow"     => '00;33',
        "light_blue"       => '00;34',
        "light_magenta"    => '00;35',
        "light_cyan"       => '00;36',
        "light_white"      => '00;37',
        "black_bg"   => 40,
        "red_bg"     => 41,
        "green_bg"   => 42,
        "yellow_bg"  => 43,
        "blue_bg"    => 44,
        "magenta_bg" => 45,
        "cyan_bg"    => 46,
        "white_bg"   => 47
    );

    /**
     * Sets a color to the provided output message.
     *
     * You can composite the ouptput color using the + (plus) sign.
     * For example Colors::set("Test message", 'red_bg+white+blink')
     * this will ouput the "Test message" with a red background, white
     * foreground and it will blink.
     * 
     * @param string $str   The string message to output.
     * @param string $color The color labels to apply to the message.
     * 
     * @return string The ouput string wwith the ANSI color in it.
     */
    public static function set($str, $color)
    {
        $colorAttrs = explode("+", $color);
        $ansiStr = "";
        foreach ($colorAttrs as $attr) {
            $ansiStr .= "\033[" . self::$_ansiCodes[$attr] . "m";
        }
        $ansiStr .= $str . "\033[" . self::$_ansiCodes["off"] . "m";
        return $ansiStr;
    }
 
    /**
 	 * Sets a color to the matched regular expression for the provided message.
 	 * 
 	 * @param string $fullText     The string message to output.
 	 * @param string $searchRegexp The regular expression to search in
     *   the message.
 	 * @param string $color        The color labels to apply to the message.
     * 
 	 * @return string The ouput string wwith the ANSI color in it.
 	 */
    public static function replace($fullText, $searchRegexp, $color)
    {
        $newText = preg_replace_callback(
            "/($searchRegexp)/",
            function ($matches) use ($color) {
                return Colors::set($matches[1], $color);
            },
            $fullText
        );
        return is_null($newText) ? $fullText : $newText;
    }

    /**
     * Checks if current system can output ANSI colors.
     *
     * @codeCoverageIgnore
     * @return boolean True if system can ouput ANSI colors, false otherwise.
     */
    public static function capable()
    {
        $ds = DIRECTORY_SEPARATOR;
        if ($ds === '\\' && !(bool)$_ENV['ANSICON']) {
            return false;
        }
        return true;
    }

}