<?php

/**
 * Simple
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Router\Route
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Router\Route;

use Slick\Router;
use Slick\ArrayMethods;

/**
 * Simple
 *
 * @package    Slick
 * @subpackage Router\Route
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Simple extends Router\Route
{

    /**
     * Creates the correct regular expression search string and returns
     * any matches to the provided $url.
     *
     * @param string $url The URL string to check.
     * @return boolean True if the url matches, false otherwise.
     */
    public function matches($url)
    {
        $pattern = $this->pattern;

        //get keys
        preg_match_all("#:([a-zA-Z0-9]+)#", $pattern, $keys);

        if (sizeof($keys) && sizeof($keys[0]) && sizeof($keys[1])) {
            $keys = $keys[1];
        } else {
            //no keys in the pattern, return a simple match
            return (boolean) preg_match("#^{$pattern}$#", $url);
        }

        //normalize route pattern
        $pattern = preg_replace(
            "#(:[a-zA-Z0-9]+)#",
            "([a-zA-Z0-9-_]+)",
            $pattern
        );

        //check values
        preg_match_all("#^{$pattern}$#", $url, $values);

        if (sizeof($values) && sizeof($values[0]) && sizeof($values[1])) {
            //unset the matched url
            unset($values[0]);
            //values found, modify parameters  and return
            $deriverd = array_combine($keys, ArrayMethods::flatten($values));
            $this->parameters = array_merge($this->parameters, $deriverd);
            return true;
        }

        return false;
    }

}
