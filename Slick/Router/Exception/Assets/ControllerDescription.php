<?php

return array("You are trying to call a controller that doesn't exist.<br />
To create a controller copy the following code to
<code>APP_PATH/Controllers/#NAME.php</code> file:
<pre>
#CODE
</pre>",
'<?php
namespace Controllers;
use Slick\Controller;

class #NAME extends Controller
{
    /**
     * Your actions here.
     */
}'
);
