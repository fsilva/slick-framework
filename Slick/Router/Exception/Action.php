<?php

/**
 * Action
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Router\Exception;

use Slick\Core as Core;

/**
 * Action
 * 
 * Action exception is throwed when a call for an unimplemented
 * controller method occours.
 *
 * @package    Slick
 * @subpackage Exception\Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Action extends Core\Exception
{

    /**
     * Overrides default constructor to set the default HTTP code.
     *
     * @param string $message $the message error.
     * @param integer $code HTTP response code.
     */
    public function __construct($message, $code = 404)
    {
        parent::__construct($message, $code);
    }
}
