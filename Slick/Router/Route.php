<?php

/**
 * Route
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Router;

use Slick\Base;
use Slick\Router\Exception;

/**
 * Route
 * Route is an abstract definition of a route in the router.
 * 
 *
 * @package    Slick
 * @subpackage Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Route extends Base
{

    /**
     * Route pattern
     * @readwrite
     * @var string
     */
    protected $_pattern;

    /**
     * Correspondent controller
     * @readwrite
     * @var string
     */
    protected $_controller;

    /**
     * Controller action/method
     * @readwrite
     * @var string
     */
    protected $_action;

    /**
     * Request parameters
     * @readwrite
     * @var array
     */
    protected $_parameters = array();

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Matches the URL against the list of routes
     * 
     * Creates the correct regular expression search string and returns
     * any matches to the provided $url.
     *
     * @param string $url The URL string to check.
     * 
     * @return boolean True if the url matches, false otherwise.
     */
    abstract public function matches($url);
}
