<?php

/**
 * Base
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Text;
use Slick\Registry;
use Slick\Inspector;
use Slick\ArrayMethods;
use Slick\Core\Request;
use Slick\Http\Response;
use Slick\Core\Exception;

/**
 * Base
 * 
 * Slick Base class where almost all framework classes will henerit.
 *
 * @package    Slick
 * @subpackage Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Base
{

    /**
     * The inspector for this class.
     * @var Inspector
     */
    private $_inspector;
    
    /**
     * Client request data.
     * @readwrite
     * @var Slick\Http\Request
     */
    protected $_request = null;

    /**
     * Http response for this request
     * @readwrite
     * @var \Slick\Http\Response
     */
    protected $_response = null;

    /**
     * Session driver
     * @readwrite
     * @var \Slick\Session\Driver
     */
    protected $_session = null;

    /**
     * Constructor assign ptoperties based on the array or object given.
     *
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     */
    public function __construct($options = array())
    {
        $this->_inspector = new Inspector($this);
        if (is_array($options) || is_object($options)) {
            foreach ($options as $key => $value) {
                $key = ucfirst($key);
                $method = "set{$key}";
                $this->$method($value);
            }
        }
    }

    /**
     * Sets the response object by reference
     * 
     * @param \Slick\Http\Response $response Response object
     *
     * @return \Slick\Base A self instance for method call chain.
     */
    public function setResponse(\Slick\Http\Response &$response)
    {
        $this->_response = $response;
        return $this;
    }

    /**
     * Returns the response object for this request.
     * 
     * @return \Slick\Http\Response Response object
     */
    public function getResponse()
    {
        if (is_null($this->_response)) {
            $response = Registry::get('response', new \Slick\Http\Response());
            $this->response = $response;
            Registry::set('response', $response);
        }
        return $this->_response;
    }

    /**
     * Sets the request object by reference
     * 
     * @param \Slick\Http\Resquest $resquest Resquest object
     *
     * @return \Slick\Base A self instance for method call chain.
     */
    public function setRequest(\Slick\Http\Request &$request)
    {
        $this->_request = $request;
        return $this;
    }

    /**
     * Returns the request object for this request.
     * 
     * @return \Slick\Http\Request Request object
     */
    public function getRequest()
    {
        if (is_null($this->_request)) {
            $request = Registry::get('request', new \Slick\Http\Request());
            $this->request = $request;
            Registry::set('request', $request);
        }
        return $this->_request;
    }

    public function setSession(\Slick\Session\Driver $session)
    {
        $this->_session = $session;
        return $this;
    }

    public function getSession()
    {
        if (is_null($this->_session)) {
            $this->session = Registry::get('session');
        }
        return $this->_session;
    }

    /**
     * Handles the call for unimplemented methods.
     *
     * If called methos is of type getProperty or setProperty this method
     * will check if the property exists and if the meta data flags, like
     * @read, @write or @readwrite are set to proper set or get the property.
     *
     * If the property can't be read or written an execption will be trown.
     *
     * If the methos isn't in for of getProperty or setProperty an exception
     * will be trown say that the method isn't implemented.
     *
     * @param string $name      The calling method.
     * @param array  $arguments An array with the arguments passed to the
     *  method calling.
     * 
     * @return void|mixed Will return the property value or the current
     *  instance for chain calls if the calling method was of type setProperty.
     */
    public function __call($name, $arguments)
    {
        if (empty($this->_inspector)) {
            throw new Exception('Call parent::__construct!');
        }

        $method = null;

        //check if the call is a getter
        $getMatches = Text::match($name, "^get([a-zA-Z0-9\_]+)$");
        if (sizeof($getMatches) > 0) {
            $method = 'getter';
        }

        //check if the call is a setter
        $setMatches = Text::match($name, '^set([a-zA-Z0-9\_]+)$');
        if (sizeof($setMatches) > 0) {
            $method = 'setter';
        }

        switch ($method) {
            case 'getter':
                return $this->_getter($getMatches[0]);

            case 'setter':
                return $this->_setter($setMatches[0], $arguments[0]);
        }

        throw $this->_getExceptionForImplementation($name);
    }

    /**
     * Retrieves the value for a given magic getter call.
     * 
     * @param string $name The property name to get the value.
     * 
     * @return mixed The property value.
     */
    protected function _getter($name)
    {
        $normalized = lcfirst($name);
        $property = "_{$normalized}";
        if (property_exists($this, $property)) {
            $meta = $this->_inspector->getPropertyMeta($property);

            if (empty($meta['@readwrite']) && empty($meta['@read'])) {
                throw $this->_getExceptionForWriteonly($normalized);
            }

            return $this->$property;
        }
        return null;
    }

    /**
     * Sets the value of a given property.
     * 
     * @param string $name The property name to set the value.
     * @param  mixed $value The value to assign to property.
     * 
     * @return Slick\Base A self instance for method chain call.
     */
    protected function _setter($name, $value)
    {
        $normalized = lcfirst($name);
        $property = "_{$normalized}";
        if (property_exists($this, $property)) {
            $meta = $this->_inspector->getPropertyMeta($property);

            if (empty($meta['@readwrite']) && empty($meta['@write'])) {
                throw $this->_getExceptionForReadonly($normalized);
            }

            $this->$property = $value;
            return $this;
        }
        
        throw $this->_getExceptionForProperty($name);
    }

    /**
     * Handles the call for unimplemented or invisible properties.
     *
     * This will result in a call to getName method handled with the
     * magic method Base::__call().
     *
     * @param string $name The calling property name.
     * 
     * @return mixed The property value or null, if property isn't set.
     * @see Base::__call()
     */
    public function __get($name)
    {
        $function = "get".ucfirst($name);
        return $this->$function();
    }

    /**
     * Handles the call to assign values to invisible or
     * unimplemented properties.
     *
     * This will result in a call to setName method handled with the
     * magic method Base::__call().
     *
     * @param string $name  The calling property name.
     * @param mixed  $value The value to assign to the property.
     * 
     * @return Object The current instance for chain calls.
     * @see Base::__call()
     */
    public function __set($name, $value)
    {
        $function = "set".ucfirst($name);
        return $this->$function($value);
    }

    /**
     * Creates and returns an ReadOnly Exception.
     *
     * @param string $property The read-only property name called.
     * 
     * @return \Slick\Core\Exception\ReadOnly A new ReadOnly Exception object.
     */
    protected function _getExceptionForReadonly($property)
    {
        return new Exception\ReadOnly("{$property} is read-only");
    }

    /**
     * Creates and returns an WriteOnly Exception.
     *
     * @param string $property The write-only property name called.
     * 
     * @return \Slick\Core\Exception\WriteOnly A new WriteOnly
     *  Exception object.
     */
    protected function _getExceptionForWriteonly($property)
    {
        return new Exception\WriteOnly("{$property} is write-only");
    }

    /**
     * Creates and returns an Property Exception.
     *
     * @param string $name The property name you are trying to use.
     * 
     * @return \Slick\Core\Exception\Property A new Property Exception object.
     */
    protected function _getExceptionForProperty($name)
    {
        return new Exception\Property("Invalid property {$name}");
    }

    /**
     * Creates and returns an Implementation Exception.
     *
     * @param string $method The called method name.
     * @return \Slick\Core\Exception\Implementation A new Implementation
     *  Exception object.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation("{$method} not implemented");
    }

}
