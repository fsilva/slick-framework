<?php

/**
 * Renderer
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\View
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\View\Exception;

use Slick\Core as Core;

/**
 * Renderer
 * 
 * Renderer exception is throwed when an error occurs in
 * template parsing/processing.
 *
 * @package    Slick
 * @subpackage Exception\View
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Renderer extends Core\Exception
{

    /**
     * Error full description
     * @var string
     */
    protected $_description = "Parser/Render exception thrown by template engine.";

    /**
     * Previous exception.
     * Normaly the engine exception.
     * @var \Exception
     */
    protected $_previous = null;

    /**
     * Overrides supper class to receive an HTTP code.
     *
     * @param string  $message The message error.
     * @param integer $code    HTTP response code.
     */
    public function __construct($message, $code = 500,
        \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_httpCode = $code;
        $this->_previous = $previous;
    }

    /**
     * Returns current description
     * 
     * @return string Error description.
     */
    public function getDescription()
    {
        $description = $this->_description;
        $description .= "<br /><b>Engine exception:</b><br />";
        $message = $this->_previous->getMessage();
        $description .= "<pre>{$message}</pre>";
        return $description;
    }
}
