<?php

/**
 * Core Exception.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Core;

use Slick\View;
use \Exception as PhpException;

/**
 * Core Exception.
 *
 * This class is the supper class for all framework exception.
 * It extents the native PHP's Exception class to acomodate an
 * HTTP code that will be used to set the HTTP response in case
 * of exception throwed during the request execution.
 *
 * @package    Slick
 * @subpackage Exception\Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Exception extends PhpException
{

    /**
     * HTTP error code
     * 
     * @var integer
     */
    protected $_httpCode;

    /**
     * The template to use when ouputing the error as HTML
     * @var string
     */
    protected $_template = 'Errors/general.html';

    /**
     * Error full description
     * @var string
     */
    protected $_description = 'This is a general Slick exception. Please check
    the stack trace for more information.<br />
    If this exception is defined by your code you can change this description
    by overriding the <code>Slick\Core\Exception::$_description</code>
    property.';

    /**
     * Overrides supper class to receive an HTTP code.
     *
     * @param string  $message The message error.
     * @param integer $code    HTTP response code.
     */
    public function __construct($message, $code = 500,
        \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_httpCode = $code;
    }

    /**
     * Returns the http error code for current exception
     * 
     * @return integer HTTP 1.0 Response error.
     */
    public function getHttpCode()
    {
        return $this->_httpCode;
    }

    /**
     * Renders this error as HTML
     * 
     * @return string The Html ouput string
     */
    public function render()
    {
        $view = new View(array('file' => $this->_template));
        $view->set('exception', $this);
        return $view->render();
    }

    /**
     * Returns current description
     * 
     * @return string Error description.
     */
    public function getDescription()
    {
        return $this->_description;
    }

}
