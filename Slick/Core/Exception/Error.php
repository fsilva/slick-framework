<?php

/**
 * Error Exception
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Core\Exception;

use Slick\Core as Core;

/**
 * Error exception
 * 
 * Error exception is throwed when a PHP error occours.
 *
 * @package    Slick
 * @subpackage Exception\Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Error extends Core\Exception
{

    /**
     * Error severity
     * @var int
     */
    protected $_severity ;

    /**
     * List of known error costants
     * @var array
     */
    protected $_level = array(
        1    => "ERROR",
        2    => "WARNING",
        4    => "PARSE",
        8    => "NOTICE",
        16   => "CORE ERROR",
        32   => "CORE WARNING",
        64   => "COMPILE ERROR",
        128  => "COMPILE WARNING",
        256  => "USER ERROR",
        512  => "USER WARNING",
        1024 => "USER NOTICE",
        6143 => "ALL",
        2048 => "STRICT",
        4096 => "RECOVERABLE ERROR",
    );

    /**
     * Constructs an error exception
     * 
     * @param string     $message  The Exception message to throw.
     * @param integer    $code     The Exception code.
     * @param integer    $severity The severity level of the exception.
     * @param string     $filename The filename where the exception
     *  is thrown.
     * @param integer    $lineno   The line number where the exception
     *  is thrown.
     * @param \Exception $previous The previous exception used for the
     *  exception chaining.
     */
    public function __construct ($message = "", $code = 0, $severity = 1,
        $filename = __FILE__, $lineno = __LINE__, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_severity = $severity;
        $this->file = $filename;
        $this->line = $lineno;
    }

    /**
     * Returns error severity
     * 
     * @return integer The error severity. 
     */
    public final function getSeverity()
    {
        return $this->_severity;
    }

    /**
     * Returns current description
     * 
     * @return string Error description.
     */
    public function getDescription()
    {
        $str = '';
        $level = ucfirst(strtolower($this->_level[$this->_severity]));
        $str .= "<strong>[{$level}]</strong> " .
            "Check error at {$this->file} ($this->line)";
        return (string) $str;
    }
}