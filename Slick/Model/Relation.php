<?php

/**
 * Relation
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Model;

use Slick\Base as Base;

/**
 * Relation.
 *
 * This class represents a relationship between two models. It is the base
 * class for Slick defined model relations like HasOne or BelongsTo.
 *
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Relation extends Base
{

    /**
     * The current model
     *
     * @readwrite
     * @var Slick\Model
     */
    protected $_model = null;

    /**
     * The model being associated to the current model.
     *
     * @readwrite
     * @var Slick\Model
     */
    protected $_related = null;

    /**
     * The foreign key column name
     *
     * @readwrite
     * @var string
     */
    protected $_foreignKey = null;

    /**
     * Cascade parameter for delete
     *
     * @readwrite
     * @var boolean
     */
    protected $_dependent = true;

    /**
     * The query type performed in this relation
     *
     * Values can be 'join' or 'select'
     *
     * @readwrite
     * @var string
     */
    protected $_queryType = 'join';

    /**
     * The table that has to have the foreign key.
     *
     * @readwrite
     * @var string
     */
    protected $_foreignKeyTable = null;

    /**
     * Prepares the data for model binding.
     * 
     * @param string $property The model property in the relation.
     * @param array  $data     The database connector result set.
     * 
     * @return void
     */
    abstract public function bindData($property, &$data);

    /**
     * Returns foreign key data to be saved with model
     * 
     * @param stdClass $object The model property object.
     * 
     * @return array A key/value pair with field name and data.
     */
    public function getSaveData($object)
    {
        return array();
    }

    /**
     * Called to perform after save operations.
     * 
     * @param stdClass $object The model property object.
     */
    public function afterSave($object)
    {
        //Do nothing
    }
}