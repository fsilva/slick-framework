<?php

/**
 * Validation
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Model;

use Slick\Text as Text;

/**
 * Validation
 *
 * Validation is an utility class to validate data used on model 
 * save operation.
 *
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Validation
{

    /**
     * A list of validators and its definitions
     * @read
     * @var array
     */
    protected static $_validators = array(
        'required' => array(
            'handler' => 'required',
            'message' => "The {0} field is required"
        ),
        'alpha' => array(
            'handler' => 'alpha',
            'message' => "The {0} field can only contain letters"
        ),
        'numeric' => array(
            'handler' => 'numeric',
            'message' => "The {0} field can only contain numbers"
        ),
        'alphanumeric' => array(
            'handler' => 'alphaNumeric',
            'message' => "The {0} field can only contain letters and numbers"
        ),
        'max' => array(
            'handler' => 'max',
            'message' => "The {0} field must contain less then {2} characters"
        ),
        'min' => array(
            'handler' => 'min',
            'message' => "The {0} field must contain more then {2} characters"
        ),
    );

    /**
     * Avoid the creation of an ArrayMethods instance.
     * @codeCoverageIgnore
     */
    private function __construct()
    {
        // do nothing
    }

    /**
     * Avoid the clonation of an ArrayMethods instance.
     * @codeCoverageIgnore
     */
    private function __clone()
    {
        // do nothing
    }

    /**
     * Returns th current list of validators
     * 
     * @return array The list of validators
     */
    public static function getValidators()
    {
        return self::$_validators;
    }

    /**
     * Validation handler for required fields.
     *
     * @param string $value The value to validate
     * 
     * @return boolean True if vaules passes validation, false otherwise.
     */
    public static function required($value)
    {
        return !empty($value);
    }

    /**
     * Validation handler for alphabetic fields.
     *
     * @param string $value The value to validate
     * 
     * @return boolean True if vaules passes validation, false otherwise.
     */
    public static function alpha($value)
    {
        return Text::match($value, "#^([a-zA-Z]+)$#");
    }

    /**
     * Validation handler for numeric fields.
     *
     * @param string $value The value to validate
     * 
     * @return boolean True if vaules passes validation, false otherwise.
     */
    public static function numeric($value)
    {
        return Text::match($value, "#^([0-9]+)$#");
    }

    /**
     * Validation handler for alpha-numeric fields.
     *
     * @param string $value The value to validate
     * 
     * @return boolean True if vaules passes validation, false otherwise.
     */
    public static function alphaNumeric($value)
    {
        return Text::match($value, "#^([0-9a-zA-Z]+)$#");
    }

    /**
     * Validation handler for max chars in fields.
     *
     * @param string $value The value to validate
     * 
     * @return boolean True if vaules passes validation, false otherwise.
     */
    public static function max($value, $number)
    {
        return strlen($value) <= (int) $number;
    }

    /**
     * Validation handler for min chars in fields.
     *
     * @param string $value The value to validate
     * 
     * @return boolean True if vaules passes validation, false otherwise.
     */
    public static function Min($value, $number)
    {
        return strlen($value) >= (int) $number;
    }
}