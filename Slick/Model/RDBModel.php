<?php

/**
 * RDBModel
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Model;

use Slick\Base as Base;
use Slick\Text as Text;
use Slick\Inspector as Inspector;
use Slick\Model\Relation\HasOne as HasOne;
use Slick\Model\Relation\HasMany as HasMany;
use Slick\Model\Relation\HasAndBelongsToMany as HasAndBelongsToMany;
use Slick\Model\Relation\BelongsTo as BelongsTo;

/**
 * RDBModel
 *
 * Relational DataBase Model is an abstract model that deal with the models
 * that can work with database systems like MySQL.
 *
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class RDBModel extends Base
{

    /**
     * The list of table columns defined by this model.
     * @var array
     */
    protected $_columns;

    /**
     * List of model relations
     * @var array
     */
    protected $_relations = null;

        /**
     * A list of columns to ignore
     * 
     * @var array
     */
    protected static $_ignoredColumns = array();


    /**
     * The maping database table for this model.
     * @readwrite
     * @var string
     */
    protected $_table;

    /**
     * The defined data types.
     * @read
     * @var array
     */
    protected $_types = array(
        'autonumber',
        'text',
        'integer',
        'decimal',
        'boolean',
        'datetime'
    );

    /**
     * Model constructor
     *
     * Changes the default base object behavior to set model columns.
     * 
     * @param array|Object $options The properties for the object
     *   beeing constructed.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->getColumns();
    }

    /**
     * Returns the model table name
     * 
     * If no set it will infalte form the model name.
     * Example: User -> users, Person -> people, etc...
     * 
     * @return string The table name that this model is linked with.
     */
    public function getTable()
    {
        if (empty($this->_table)) {
            $parts = explode('\\', get_class($this));
            $name = end($parts);
            $this->_table = Text::plural(strtolower($name));
        }
        return $this->_table;
    }

    /**
     * Returns the model alias use in select queries.
     * 
     * @return string The model alias.
     */
    public function getAlias()
    {
        if (empty($this->_alias)) {
            $parts = explode('\\', get_class($this));
            $name = end($parts);
            $this->_alias = $name;
        }
        return $this->_alias;
    }

    /**
     * Retrieves the table columns from model.
     * 
     * Inspects the class properties to retrieve the DB columns and
     * its meta data.
     *
     * @return array The model DB columns.
     */
    public function getColumns()
    {
        if (empty($this->_columns)) {
            $primaries = 0;
            $columns = array();
            $class = get_class($this);
            $types = $this->_types;

            $inspector = new Inspector($this);
            $properties = $inspector->getClassProperties();

            foreach ($properties as $property) {
                $propertyMeta = $inspector->getPropertyMeta($property);
                if (!empty($propertyMeta['@column'])) {
                    $name = preg_replace('#^_#', '', $property);
                    $primary = !empty($propertyMeta['@primary']);
                    $type = $this->_firstVal($propertyMeta, '@type');
                    $length = $this->_firstVal($propertyMeta, '@length');
                    $index = !empty($propertyMeta['@index']);
                    $readwrite = !empty($propertyMeta['@readwrite']);
                    $write = !empty($propertyMeta['@write']) || $readwrite;
                    $read = !empty($propertyMeta['@read']) || $readwrite;
                    $validate = !empty($propertyMeta['@validate']) ?
                        $propertyMeta['@validate'] : false;
                    $label = $this->_firstVal($propertyMeta, '@label');

                    if (!in_array($type, $types)) {
                        throw new Exception\Type(
                            "{$type} is not a valid type"
                        );
                    }

                    if ($primary) {
                        $primaries++;
                    }

                    $columns[$name] = array(
                        "raw" => $property,
                        "name" => $name,
                        "primary" => $primary,
                        "type" => $type,
                        "length" => $length,
                        "index" => $index,
                        "read" => $read,
                        "write" => $write,
                        "validate" => $validate,
                        "label" => $label
                    );
                }
            }

            if ($primaries !== 1) {
                throw new Exception\Primary(
                    "{$class} must have exactly one @primary column"
                );
            }

            $this->_columns = $columns;
        }

        return $this->_columns;
    }

    /**
     * Returns the list of relations.
     *
     * This is a lazy method that only checks the relations upon request.
     * The properties that have set the tags @hasone, @hasmany and @belongsto
     * are mapped in this arary list with the relation object.
     * 
     * @return array A list of relations this model has with other models.
     */
    public function getRelations()
    {
        if (is_null($this->_relations)) {
            $this->_relations = array();
            $inspector = new Inspector($this);
            $properties = $inspector->getClassProperties();

            foreach ($properties as $property) {
                $propertyMeta = $inspector->getPropertyMeta($property);
                $name = preg_replace('#^_#', '', $property);
                
                if (!empty($propertyMeta['@hasone'])) {
                    $this->_addHasOne($name, $propertyMeta);
                }

                if (!empty($propertyMeta['@hasmany'])) {
                    $this->_addHasMany($name, $propertyMeta);
                }

                if (!empty($propertyMeta['@belongsto'])) {
                    $this->_addBelongsTo($name, $propertyMeta);
                }

                if (!empty($propertyMeta['@hasandbelongstomany'])) {
                    $this->_addHasAndBelongsToMany($name, $propertyMeta);
                }
            }
        }
        return $this->_relations;
    }

    /**
     * Returns the individual column from defined columns by name.
     *
     * @param string $name Column name to search.
     * 
     * @return array Column meta definitions.
     */
    public function getColumn($name)
    {
        $columns = $this->columns;
        if (!empty($this->columns[$name])) {
            return $this->columns[$name];
        }
        return null;
    }

    /**
     * Retrieves the column metadata defined as primary.
     *
     * @return array Column meta definitions.
     */
    public function getPrimaryColumn()
    {
        if (!isset($this->_primary)) {
            $primary;
            foreach ($this->getColumns() as $column) {
                if ($column['primary']) {
                    $primary = $column;
                    break;
                }
            }
            $this->_primary = $primary;
        }

        return $this->_primary;
    }

    /**
     * Adds an ignored column name.
     *
     * Ignored columns are the column names that came from relations queries
     * and will raise an error when binding data to the model object.
     * 
     * @param string $name The column name to be ignored
     */
    public function addIgnoredColumn($name)
    {
        self::$_ignoredColumns[] = $name;
    }

    /**
     * Retrives the value of a tag.
     * 
     * @param array $array The property list.
     * @param string $key  The tag to retrieve the valeu.
     * 
     * @return string The tag value.
     */
    protected function _firstVal($array, $key)
    {
        $val = null;
        if (!empty($array[$key]) && sizeof($array[$key]) == 1) {
            $val = $array[$key][0];
        }
        return $val;
    }

    /**
     * Adds an "HasOne" relation to current model.
     * 
     * @param string $name Property name
     * @param array $meta Property meta data.
     */
    protected function _addHasOne($name, $meta)
    {
        $related = $this->_firstVal($meta, '@hasone');
        $options = $this->_parseRelationProperties($meta);
        $this->_relations[$name] = new HasOne($this, $related, $options);
    }

    /**
     * Adds an "HasMany" relation to current model.
     * 
     * @param string $name Property name
     * @param array $meta Property meta data.
     */
    protected function _addHasMany($name, $meta)
    {
        $related = $this->_firstVal($meta, '@hasmany');
        $options = $this->_parseRelationProperties($meta);
        $this->_relations[$name] = new HasMany($this, $related, $options);
    }

    /**
     * Adds an "BelongsTo" relation to current model.
     * 
     * @param string $name Property name
     * @param array $meta Property meta data.
     */
    protected function _addBelongsTo($name, $meta)
    {
        $related = $this->_firstVal($meta, '@belongsto');
        $options = $this->_parseRelationProperties($meta);
        $this->_relations[$name] = new BelongsTo($this, $related, $options);
    }

    /**
     * Adds an "HasAndBelongsToMany" relation to current model.
     * 
     * @param string $name Property name
     * @param array $meta Property meta data.
     */
    protected function _addHasAndBelongsToMany($name, $meta)
    {
        $related = $this->_firstVal($meta, '@hasandbelongstomany');
        $options = $this->_parseRelationProperties($meta);
        $this->_relations[$name] = new HasAndBelongsToMany(
            $this,
            $related,
            $options
        );
    }

    /**
     * Parse and retrive the options for relation.
     *
     * @param array $meta Property meta data.
     * 
     * @return array Relation properties.
     */
    protected function _parseRelationProperties($meta)
    {
        $options = array();
        if (!empty($meta['@dependent'])) {
            $val = $this->_firstVal($meta, '@dependent');
            $options['dependent'] = ($val == 'true') ? true : false;
        }

        if (!empty($meta['@foreignkey'])) {
            $val = $this->_firstVal($meta, '@foreignkey');
            $options['foreignKey'] = $val;
        }

        if (!empty($meta['@jointable'])) {
            $val = $this->_firstVal($meta, '@jointable');
            $options['joinTable'] = $val;
        }

        if (!empty($meta['@relatedforeignkey'])) {
            $val = $this->_firstVal($meta, '@relatedforeignkey');
            $options['relatedForeignKey'] = $val;
        }
        return $options;
    }

    /**
     * Retrieves the value for a given magic getter call.
     * 
     * @param string $name The property name to get the value.
     * @return mixed The property value.
     */
    protected function _getter($name)
    {
        $value = parent::_getter($name);
        $normalized = lcfirst($name);
        $property = "_{$normalized}";
        $relations = $this->getRelations();
        $primary = $this->getPrimaryColumn();
        $id = $primary['name'];

        $isNull = empty($this->$property);
        $isDefined = isset($relations[$normalized]) && 
            is_a(
                $relations[$normalized],
                '\Slick\Model\Relation'
            );

        if ($isNull && $isDefined) {
            $data = $relations[$normalized]->getChilds($this->$id);
            $this->$property = $data;
            return $data;
        }
        return $value;
    }

    /**
     * Sets the value of a given property.
     * 
     * @param string $name The property name to set the value.
     * @param  mixed $value The value to assign to property.
     * @return Slick\Base A self instance for method chain call.
     */
    protected function _setter($name, $value)
    {
        $normalized = lcfirst($name);
        if (in_array($normalized, self::$_ignoredColumns)) {
            return $this;
        }

        $columns = $this->getColumns();
        $relations = $this->getRelations();

        if (
            !isset($columns[lcfirst($name)]) &&
            !isset($relations[lcfirst($name)])
        ) {
            return $this;
        }

        return parent::_setter($name, $value);
    }

    /**
     * Retrive an array of data to be saved.
     *
     * This method is called by the Slick\Model::save() method before 
     * it sends data to query. Here the relations that are defined may
     * add data (foreign keys) tho this save operation.
     * 
     * @return array Array of data to be saved.
     */
    protected function _getRelationsData()
    {
        $data = array();
        $relations = $this->getRelations();
        foreach ($relations as $property => $relation) {
            $data = array_merge(
                $data,
                $relation->getSaveData($this->$property)
            );
        }
        return $data;
    }

    /**
     * Runs after save method on all relations.
     * @return [type] [description]
     */
    protected function _notifyRelations()
    {
        $relations = $this->getRelations();
        foreach ($relations as $property => $relation) {
            $relation->afterSave($this->$property);
        }
    }
}