<?php

/**
 * HasAndBelongsToMany relation
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model\Relation
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Model\Relation;

use Slick\Model as Model;
use Slick\Text as Text;

/**
 * HasAndBelongsToMany
 *
 * @package    Slick
 * @subpackage Model\Relation
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class HasAndBelongsToMany extends Model\Relation
{

    /**
     * The join table
     *
     * @readwrite
     * @var string
     */
    protected $_joinTable = null;

    /**
     * The related foreign key
     *
     * @readwrite
     * @var string
     */
    protected $_relatedForeignKey = null;

    /**
     * HasMany relation constructor.
     * 
     * @param \Slick\Model $model   The current model.
     * @param string       $related The related model class name.
     * @param array|Object $options The properties for the object beeing
     *   constructed.
     */
    public function __construct(\Slick\Model $model, $related,
        $options = array())
    {
        $name = 'Models\\' . $related;
        $this->_related = new $name();
        $this->_model = $model;
        $this->_foreignKey = $this->_setForeignKey();
        $this->_relatedForeignKey = $this->_setRelatedForeignKey();
        $this->_queryType = 'select';
        $this->_setJoinTable();
        parent::__construct($options);
        $this->_foreignKeyTable = $this->_related->getTable();

    }

    /**
     * Returns the related childs for current model.
     * 
     * @param integer $id The primary key id.
     * 
     * @return \Slick\Model\DataSet A related model data list.
     */
    public function getChilds($id)
    {
        $this->related->addIgnoredColumn($this->foreignKey);
        $this->related->addIgnoredColumn($this->relatedForeignKey);
        $query = $this->related->query();
        $table = $this->related->getTable();
        $relPrimary = $this->related->getPrimaryColumn();
        $modPrimary = $this->model->getPrimaryColumn();
        $rel = "Rel.{$this->_relatedForeignKey} = ";
        $rel .= "{$this->related->alias}.{$relPrimary['name']}";

        $query->from("{$table} AS {$this->related->alias}")
            ->join(
                "{$this->joinTable} AS Rel",
                $rel
            )
            ->where("Rel.{$this->_foreignKey} = ?", $id);


        $relations = $this->related->relations;
        $alias = $this->related->getAlias();
        $class = get_class($this->related);
        $ds = new Model\DataSet();

        foreach ($query->all() as $row) {
            $ds[] = new $class($row[$alias]);
        }

        return $ds;
    }

    /**
     * Prepares the data for model binding.
     * 
     * @param string $property The model property in the relation.
     * @param array  $data     The database connector result set.
     * 
     * @return void
     */
    public function bindData($property, &$data)
    {
        return null;
    }

    /**
     * Called to perform after save operations.
     * 
     * @param stdClass $object The model property object.
     */
    public function afterSave($object)
    {   

        if (
            is_a($object, '\Slick\Model\DataSet') &&
            count($object) > 0
        ) {
            $primary = $this->model->getPrimaryColumn();
            $property = $primary['name'];
            $value = $this->model->$property;
            $sql = "DELETE FROM {$this->joinTable} ";
            $sql .= "WHERE {$this->foreignKey} = {$value}";
            $insert = "INSERT INTO {$this->joinTable} ";
            $insert .= "({$this->foreignKey}, {$this->relatedForeignKey}) ";
            $insert .= "VALUES ";
            foreach ($object as $model) {
                $p = $model->getPrimaryColumn();
                $n = $p['name'];
                $v = $model->$n;
                $inserts[] = "($value, $v)";
            }
            $insert .= implode(", ", $inserts);
            $connector = $this->model->connector;
            $connector->execute($sql);
            $connector->execute($insert);
        }

    }

    /**
     * Sets the default foreign key name.
     */
    protected function _setForeignKey()
    {
        $modelName = $this->_model->getTable();
        return Text::singular($modelName) . '_id';
    }

    /**
     * Sets the default foreign key name for related model.
     */
    protected function _setRelatedForeignKey()
    {
        $modelName = $this->_related->getTable();
        return Text::singular($modelName) . '_id';
    }

    /**
     * Sets the default join table.
     *
     * The join table by convention is the concatenation of the two related
     * tables, ordered alphabetically by name. For example: User has and
     * belongs to many Tag will result in tags_users table name.
     */
    protected function _setJoinTable()
    {
        $tables = array();
        array_push($tables, $this->_model->getTable());
        array_push($tables, $this->_related->getTable());
        asort($tables);
        $this->_joinTable = implode('_', $tables);
    }
}