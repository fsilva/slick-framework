<?php

/**
 * HasMany
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model\Relation
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Model\Relation;

use Slick\Model as Model;
use Slick\Text as Text;

/**
 * HasMany relation
 *
 * @package    Slick
 * @subpackage Model\Relation
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class HasMany extends Model\Relation
{

    /**
     * HasMany relation constructor.
     * 
     * @param \Slick\Model $model   The current model.
     * @param string       $related The related model class name.
     * @param array|Object $options The properties for the object beeing
     *   constructed.
     */
    public function __construct(\Slick\Model $model, $related,
        $options = array())
    {
        $name = 'Models\\' . $related;
        $this->_related = new $name();
        $this->_model = $model;
        $this->_foreignKey = $this->_setForeignKey();
        $this->_queryType = 'select';
        parent::__construct($options);
        $this->_foreignKeyTable = $this->_related->getTable();

    }

    /**
     * Prepares the data for model binding.
     * 
     * @param string $property The model property in the relation.
     * @param array  $data     The database connector result set.
     * 
     * @return void
     */
    public function bindData($property, &$data)
    {
        return null;
    }
    
    /**
     * Returns the related childs for current model.
     * 
     * @param integer $id The primary key id.
     * 
     * @return \Slick\Model\DataSet A related model data list.
     */
    public function getChilds($id)
    {
        $this->_related->addIgnoredColumn($this->foreignKey);
        return $this->related->all(array("$this->foreignKey = ?", $id));
    }

    /**
     * Sets the default foreign key name.
     */
    protected function _setForeignKey()
    {
        $modelName = $this->_model->getTable();
        return Text::singular($modelName) . '_id';
    }
}