<?php

/**
 * BelongsTo relation
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model\Relation
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Model\Relation;

use Slick\Model as Model;
use Slick\Text as Text;

/**
 * BelongsTo relation
 *
 * @package    Slick
 * @subpackage Model\Relation
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class BelongsTo extends Model\Relation
{

    /**
     * HasOne relation constructor.
     * 
     * @param \Slick\Model $model   The current model.
     * @param string       $related The related model class name.
     * @param array|Object $options The properties for the object beeing
     *   constructed.
     */
    public function __construct(\Slick\Model $model, $related,
        $options = array())
    {
        $name = 'Models\\' . $related;
        $this->_related = new $name();
        $this->_model = $model;
        $this->_foreignKey = $this->_setForeignKey();

        parent::__construct($options);
        $this->_foreignKeyTable = $this->_related->getTable();
    }

    /**
     * Changes the query.
     *
     * This method is call by the model that is performing an "all select"
     * and is meant to set the necessary joins to the query.
     * The $uqery param is passed by referrence so all changes to the query
     * will be preserved when iterating through al the model relations.
     * 
     * @param  \Slick\Database\Query $query The query object raht eill be
     *   affected with this relation.
     *   
     * @return void
     */
    public function changeQuery(\Slick\Database\Query &$query)
    {
        $table = $this->related->getTable();
        $alias = $this->related->getAlias();
        $modelAlias = $this->model->getAlias();
        $primary = $this->related->getPrimaryColumn();
        

        $join = "{$table} AS {$alias}";
        $on = "{$alias}.{$primary['name']} = ";
        $on .= "{$modelAlias}.{$this->_foreignKey}";
        $query->join($join, $on);
    }

    /**
     * Prepares the data for model binding.
     * 
     * @param string $property The model property in the relation.
     * @param array  $data     The database connector result set.
     * 
     * @return void
     */
    public function bindData($property, &$data)
    {
        if (isset($data[$this->_related->alias])) {
            $modelName = $this->model->alias;
            $class = get_class($this->_related);
            $relatedData = $data[$this->_related->alias];
            unset($data[$modelName][$this->_foreignKey]);
            unset($data[$this->_related->alias]);           
            $data[$modelName][$property] = new $class($relatedData);
        }
    }

    /**
     * Returns foreign key data to be saved with model
     * 
     * @param \Slick\Model $object The model property object.
     * 
     * @return array A key/value pair with field name and data.
     */
    public function getSaveData($object)
    {
        $data = array();
        if (is_a($object, '\Slick\Model\RDBModel')) {
            $primary = $object->getPrimaryColumn();
            $prop = $primary['name'];

            $data = array($this->foreignKey => $object->$prop);
        }
        return $data;
    }

    /**
     * Sets the default foreign key name.
     */
    protected function _setForeignKey()
    {
        $modelName = $this->_related->getTable();
        return Text::singular($modelName) . '_id';
    }
}