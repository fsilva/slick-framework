<?php

/**
 * Template
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Template
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */
 
namespace Slick;

use Slick\Base;
use Slick\Template\Exception;

/**
 * Template
 *
 * @package    Slick
 * @subpackage Template
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Template extends Base
{

    /**
     * Cache type.
     *
     * @readwrite
     * @var String
     */
    protected $_type = 'twig';

    /**
     * A list of options for cache.
     *
     * @readwrite
     * @var array
     */
    protected $_options;

    /**
     * Stores the used template engine.
     * @read
     * @var Framework\Template\Engine
     */
    protected $_engine;

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * 
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Overrides the Base constructor to initialize the default engine.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->_initialize();
    }

    /**
     * Creates a template engine for the given type.
     * 
     * @return Framework\Template\Engine A template enginer object.
     */
    protected function _initialize()
    {
        if (!$this->_type) {
            throw new Exception\Argument("Undefined engine type");
        }

        switch ($this->type) {
            case 'twig':
                $this->_engine = new Template\Engine\Twig($this->options);
                break;

            default:
                throw new Exception\Argument("Invalid engine type");
        }
    }

    /**
     * Sets the internal template system type
     * 
     * @param String $type The template engine type.
     */
    public function setType($type)
    {
        $this->_type = $type;
        $this->_initialize();
    }

    /**
     * Parses the source tempalte code.
     *
     * @param String $source The tempalte to parse
     * 
     * @return \Slick\Template\Engine Returns this instance for
     *  chainnig method calls.
     */
    public function parse($source)
    {
        return $this->_engine->parse($source);
    }

    /**
     * Processes the template with data to produce the final output.
     *
     * @param mixed $data The data that will be used to process the view.
     * 
     * @return String Returns processed output string.
     */
    public function process($data)
    {
        return $this->_engine->process($data);
    }

}
