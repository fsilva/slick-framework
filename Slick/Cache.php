<?php

/**
 * Cache
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base as Base;
use Slick\Cache as Cache;
use Slick\Events as Events;
use Slick\Cache\Exception as Exception;

/**
 * Cache
 * 
 * Cache is a factory class that initiates cache driver that you can work with.
 * Before initialize the driver you have to set the driver type you want to
 * use. Slick core has two drivers: 'file' and 'memcached';
 * By default your application is bootstrapped with 'memcached' cache driver 
 * type but you can change this settings on the "Configuration/Core.ini" file.
 *
 * @package    Slick
 * @subpackage Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Cache extends Base
{

    /**
     * Cache type.
     *
     * @readwrite
     * @var string
     */
    protected $_type;

    /**
     * A list of options for cache.
     *
     * @readwrite
     * @var array
     */
    protected $_options;

    /**
     * Overrides the base implementaion exception calling.
     * 
     * @param string $method The method name.
     * 
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation("{$method} method not implemented");
    }

    /**
     * Creates a self::$_type type cache driver
     *
     * Creates the cache driver passing self::$_options to driver constructor.
     * This method fires the "cache.before.initialize" and 
     * "cache.after.initialize" events before and after driver criation 
     * instructions respectively.
     * "cache.before.initialize" will pass the $cache argument - the current
     * cache instance while "cache.after.initialize" will pass the $cache 
     * and the instanciated driver as $driver.
     *
     * @return Slick\Cache\Driver A cache driver object.
     *
     * @throws Slick\Cache\Exception\Argument If the self::$_type type driver
     *   is not set or cannot be found. 
     */
    public function initialize()
    {
        if (!$this->_type) {
            throw new Exception\Argument("Undefined cache type");
        }

        Events::fire('cache.before.initialize', array($this));

        switch ($this->type) {
            case 'memcached':
                $driver = new Cache\Driver\Memcached($this->options);
                break;

            case 'file':
                $driver = new Cache\Driver\File($this->options);
                break;

            default:
                throw new Exception\Argument("Invalid cache type");
        }
        Events::fire('cache.after.initialize', array($this, $driver));
        return $driver;
    }
}
