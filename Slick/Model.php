<?php

/**
 * Model
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Model\RDBModel as RDBModel;
use Slick\Registry as Registry;
use Slick\Model\Validation as Validation;
use Slick\Model\DataSet as DataSet;
use Slick\Model\Exception as Exception;


/**
 * Model
 * 
 * Slick framework model the "M" in the MVC pattern.
 *
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Model extends RDBModel
{

    /**
     * The database connector.
     * @readwrite
     * @var Slick\Database\Connector
     */
    protected $_connector;

    /**
     * The database default connection name
     * @readwrite
     * @var string
     */
    protected $_connection = 'default';

    /**
     * A list of validation errors
     * @readwrite
     * @var array
     */
    protected $_errors = array();

    /**
     * The primary key field.
     * @var string
     */
    protected $_primary;

    /**
     * Model alias for queries.
     *
     * @readwrite
     * @var string
     */
    protected $_alias;

    /**
     * Returns the database connector.
     * 
     * If no set you, tries to get it from registry.
     *
     * @return Slick\Database\Connector The application database connector.
     *
     * @throws Slick\Model\Exception\Connector If any array accours when
     *   trying to connecto to the database.
     */
    public function getConnector()
    {
        if (empty($this->_connector)) {
            $database = Registry::get("database_{$this->connection}");

            if (!$database) {
                throw new Exception\Connector(
                    "No database connector available"
                );
            }

            $this->_connector = $database->initialize();
            if (!$this->_connector->isConnected) {
                $this->_connector->connect();
            }
        }
        return $this->_connector;
    }

    /**
     * Loads the record that has the primary key set.
     */
    public function load()
    {
        $primary = $this->primaryColumn;

        $raw = $primary['raw'];
        $name = $primary['name'];

        if (!empty($this->$raw)) {
            $previous = $this->connector
                ->query()
                ->from($this->table)
                ->where("{$name} = ?", $this->$raw)
                ->first();

            if ($previous == null) {
                throw new Exception\Primary("Primary key value is invalid");
            }

            foreach ($previous as $key => $value) {
                $prop = "_{$key}";
                if (!empty($previous[$key]) && !isset($this->$prop)) {
                    $this->$key = $previous[$key];
                }
            }
        }
    }

    /**
     * Saves current object data on db.
     *
     * @return integer The last inserted id or 0 for an update.
     */
    public function save()
    {
        $primary = $this->primaryColumn;

        $raw = $primary['raw'];
        $name = $primary['name'];

        $query = $this->connector
            ->query()
            ->from($this->table);

        if (!empty($this->$raw)) {
            $query->where("{$name} = ?", $this->$raw);
        }

        $data = array();
        foreach ($this->columns as $key => $column) {
            if (!$column['read']) {
                $prop = $column['raw'];
                $data[$key] = $this->$prop;
                continue;
            }

            if ($column != $this->primaryColumn && $column) {
                $method = "get".ucfirst($key);
                $data[$key] = $this->$method();
                continue;
            }
        }

        $data = array_merge($data, $this->_getRelationsData());

        $result = $query->save($data);
        if ($result > 0) {
            $this->$raw = $result;
        }

        $this->_notifyRelations();

        return $result;
    }

    /**
     * Deletes the record that has the primary key set.
     *
     * @return integer The total rows affected by delete operation.
     */
    public function delete()
    {
        $primary = $this->primaryColumn;

        $raw = $primary['raw'];
        $name = $primary['name'];

        if (!empty($this->$raw)) {
            return $this->connector
                ->query()
                ->from($this->table)
                ->where("{$name} = ?", $this->$raw)
                ->delete();
        }

        return 0;
    }

    /**
     * Returns a new query object from connector.
     * 
     * @return Slick\Database\Query The query object.
     */
    public function query()
    {
        return $this->connector->query();
    }

    /**
     * Deletes the rows that satisfy the given where clause(s).
     *
     * @param array $where A list of conditions and values for the where clause
     * 
     * @return integer The total rows affected by delete operation.
     */
    public static function deleteAll($where = array())
    {
        $instance = new static();
        $query = $instance->connector
            ->query()
            ->from($instance->table);

        foreach ($where as $clause => $value) {
            $query->where($clause, $value);
        }

        return $query->delete();
    }

    /**
     * Counts the number of rows for a given where cluase.
     *
     * @param array $where A list of where clauses and its values.
     * 
     * @return interger The number of rows.
     */
    public static function count($where = array())
    {
        $model = new static();
        return $model->_count($where);
    }    

    /**
     * Retrives the list of models for a given query conditions.
     *
     * @param array   $where     A list of where clauses and its values.
     * @param array   $fields    The list of fields to retrieve (defaul all)
     * @param string  $order     The query order clasuse.
     * @param string  $direction The order direction clasuse (default ASC)
     * @param integer $limit     The limit for the returned rows.
     * @param integer $page      The limit offset page.
     * 
     * @return array A list of records that are model instances.
     */
    public static function all($where = array(), $fields = array('*'),
        $order = null, $direction = null, $limit = null, $page = null)
    {

        $model = new static();
        return $model->_all(
            $where,
            $fields,
            $order,
            $direction,
            $limit,
            $page
        );
    }

    
    /**
     * Retrieves the first matching row for a given where parameters.
     *
     * @param array  $where     A list of where clauses and its values.
     * @param array  $fields    The list of fields to retrieve (defaul all)
     * @param string $order     The query order clasuse.
     * @param string $direction The order direction clasuse (default ASC)
     * 
     * @return Slick\Model The model object for the retrieved row of null.
     */
    public static function first($where = array(), $fields = array('*'),
        $order = null, $direction = null)
    {

        $model = new static();
        return $model->_first($where, $fields, $order, $direction);
    }

    /**
     * Validates data based on the validate tag in comment blocks.
     *
     * @return boolean True if all fields are valid, false otherwise.
     *
     * @throws Slick\Model\Exception\Validation If you set an undefined
     *   validator in the property comment.
     */
    public function validate()
    {
        $this->_errors = array();
        $columns = $this->getColumns();
        $error = false;

        foreach ($columns as $column) {
            if ($column['validate']) {
                
                $pattern = "#[a-z]+\(([a-zA-Z0-9, ]+)\)#";

                $raw = $column['raw'];
                $name = $column['name'];
                $validators = $column['validate'];
                $label = $column['label'];

                $defined = Validation::getValidators();

                foreach ($validators as $validator) {
                    $function = $validator;
                    $arguments = array(
                        $this->$raw
                    );

                    $match = Text::match($validator, $pattern);
                    if (count($match) > 0) {
                        $matches = Text::split($match[0], ",\s*");
                        $arguments = array_merge($arguments, $matches);
                        $offset = Text::indexOf($validator, "(");
                        $function = substr($validator, 0, $offset);
                    }

                    if (!isset($defined[$function])) {
                        throw new Exception\Validation(
                            "The {$function} validator is not defined"
                        );
                    }

                    $template = $defined[$function];

                    if (
                        !call_user_func_array(
                            "\Slick\Model\Validation::{$template['handler']}",
                            $arguments
                        )
                    ) {
                        $replacements = array_merge(
                            array($label ? $label : $raw),
                            $arguments
                        );

                        $message = $template['message'];

                        foreach ($replacements as $i => $replacement) {
                            $message = str_replace(
                                "{{$i}}",
                                $replacement,
                                $message
                            );
                        }

                        if (!isset($this->_errors[$name])) {
                            $this->_errors[$name] = array();
                        }
                        $this->_errors[$name][] = $message;
                        $error = true;
                    }
                }
            }
        }
        return !$error;
    }

    

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Retrives the list of models for a given query conditions.
     *
     * @param array   $where     A list of where clauses and its values.
     * @param array   $fields    The list of fields to retrieve (defaul all)
     * @param string  $order     The query order clasuse.
     * @param string  $direction The order direction clasuse (default ASC)
     * @param integer $limit     The limit for the returned rows.
     * @param integer $page      The limit offset page.
     * 
     * @return \Slick\Model\DataSet A list of records that are model instances.
     */
    protected function _all($where = array(), $fields = array('*'),
        $order = null, $direction = null, $limit = null, $page = null)
    {

        $from = "{$this->table} AS {$this->alias}";
        $query = $this
            ->connector
            ->query()
            ->from($from, $fields);

        foreach ($where as $clause => $value) {
            $query->where($clause, $value);
        }

        $relations = $this->getRelations();

        foreach ($relations as $property => $relation) {
            if ($relation->queryType == 'join') {
                $relation->changeQuery($query);
            }
        }

        if ($order != null) {
            $query->order($order, $direction);
        }

        if ($limit != null) {
            $query->limit($limit, $page);
        }

        $class = get_class($this);
        $ds = new DataSet();

        $relations = $this->relations;
        $alias = $this->getAlias();
        foreach ($query->all() as $row) {


            foreach ($relations as $key => $relation) {
                $relation->bindData($key, $row);
            }

            if (isset($row[$this->alias])) {
                $obj = new $class($row[$this->alias]);
            } else {
                $obj = new $class($row);
            }
            $ds[] = $obj;
            
        }

        return $ds;
    }

    /**
     * Retrieves the first matching row for a given where parameters.
     *
     * @param array  $where     A list of where clauses and its values.
     * @param array  $fields    The list of fields to retrieve (defaul all)
     * @param string $order     The query order clasuse.
     * @param string $direction The order direction clasuse (default ASC)
     * 
     * @return Slick\Model The model object for the retrieved row of null.
     */
    protected function _first($where = array(), $fields = array('*'),
        $order = null, $direction = null)
    {

        $query = $this
            ->connector
            ->query()
            ->from("{$this->table} AS {$this->alias}", $fields);

        foreach ($where as $clause => $value) {
            $query->where($clause, $value);
        }

        if ($order != null) {
            $query->order($order, $direction);
        }

        $first = $query->first();
        $class = get_class($this);

        if ($first) {

            return (isset($first[$this->alias])) ?
                new $class($first[$this->alias]) :
                new $class($first);
        }

        return null;
    }

    /**
     * Counts the number of rows for a given where cluase.
     *
     * @param array $where A list of where clauses and its values.
     * 
     * @return interger The number of rows.
     */
    protected function _count($where = array())
    {
        $query = $this
            ->connector
            ->query()
            ->from($this->table);

        foreach ($where as $clause => $value) {
            $query->where($clause, $value);
        }

        return $query->count();
    }
}