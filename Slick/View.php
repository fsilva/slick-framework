<?php

/**
 * View
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage View
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base;
use Slick\Template;
use Slick\Events;
use Slick\View\Exception;

/**
 * View
 * 
 * View is a façade tho deal with the templating system.
 * The "V" in MCV framework.
 *
 * @package    Slick
 * @subpackage View
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class View extends Base
{

    /**
     * Template file to use
     * @readwrite
     * @var string
     */
    protected $_file;

    /**
     * The template engine that will render the view.
     * @read
     * @var \Slick\Template\Engine
     */
    protected $_template;

    /**
     * The data that will populate the template.
     * @read
     * @var array
     */
    protected $_data = array();

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * @return \Slick\View\Exception\Implementation
     *   The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Overrides the constructor to set the template engine.
     *
     * It fires 'view.before.construct' event with parameters:
     *     $view -> A self instance reference
     *     $file -> The template file that should be rendered 
     *
     * It fired 'view.after.construct' event with parameters:
     *     $view -> A self instance reference
     *     $file -> The template file that should be rendered 
     *     $template -> The template egine that was instantiated
     *     
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        Events::fire('view.before.construct', array($this, $this->file));
        $this->_template = new Template(array('type' => 'twig'));
        Events::fire(
            'view.before.construct',
            array($this, $this->file, $this->_template)
        );
    }

    /**
     * Renders this view.
     *
     * It fires 'view.before.render' and 'view.after.render' with parameters:
     *     $view -> The View instance it self
     *     $output -> The redered output string.
     *
     * @return string The rendered output
     */
    public function render()
    {
        Events::fire('view.before.render', array($this));
        /*if (!file_exists($this->getFile())) {
            return "";
        }
        $content = file_get_contents($this->getFile());*/
        $this->_template->parse($this->getFile());
        $output = $this->_template->process($this->_data);
        Events::fire('view.after.render', array($this, $output));
        return $output;
    }

    /**
     * Returns a previout setted data value for provided key.
     *
     * @param string $key     The key used to store the data value.
     * @param string $default The default value returned for not found key.
     * 
     * @return mixed The previously setted value for the given key.
     */
    public function get($key, $default = "") 
    {
        if (isset($this->_data[$key])) {
            return $this->_data[$key];
        }
        return $default;
    }

    /**
     * Sets a value to a single key.
     *
     * @param string $key   The key used to set the data value.
     * @param mixed  $value The value to set.
     */
    protected function _set($key, $value)
    {
        if (!is_numeric($key) && !is_string($key)) {
            throw new Exception\Data("Key must be a string or a number");
        }
        $this->_data[$key] = $value;
    }

    /**
     * Sets a value or an array of values to the data that will be rendered.
     *
     * @param string|array $key   The key used to set the data value. If an
     *  array is given it will iterate thru all the elements and set the
     *  values of the array.
     * @param mixed        $value The value to add to set.
     * 
     * @return \Slick\View A self instance for chain method calls.
     */
    public function set($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $_key => $value) {
                $this->_set($_key, $value);
            }
            return $this;
        }
        $this->_set($key, $value);
        return $this;
    }

    /**
     * Removes the value setted with provided key.
     *
     * @param string $key $key The key used to set the data value.
     * 
     * @return \Slick\View A self instance for chain method calls.
     */
    public function erase($key)
    {
        unset($this->_data[$key]);
        return $this;
    }
}
