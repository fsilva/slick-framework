<?php

/**
 * Log
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Log
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base;
use Slick\Utility\Folder;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\ChromePHPHandler;

/**
 * Log
 *
 * The log mechanism uses Jordi Boggiano's Monolog library.
 * @see  https://github.com/Seldaek/monolog
 *
 * @package    Slick
 * @subpackage Log
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Log extends Base
{

    /**
     * A list of available loggers.
     * @read
     * @var array
     */
    protected $_loggers = array();

    /**
     * A stack of handlers
     * @read
     * @var array
     */
    protected $_handlers = array();

    /**
     * The default log file path.
     * @readwrite
     * @var string
     */
    protected $_logFilePath = null;

    /**
     * Sets the name for default logger.
     * @readwrite
     * @var string
     */
    protected $_defaultLogger = 'general';

    /**
     * Creates a log manager object.
     * 
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     */
    public function __construct($options = array())
    {
        $this->_logFilePath = APP_PATH . '/Tmp/Logs';
        parent::__construct($options);
    }

    /**
     * Gets the logger for the channel with the provided name.
     * 
     * @param string $name The loggers's channel name to retreive.
     * 
     * @return \Monolog\Logger The logger object for the given channel name.
     */
    public function getLogger($name = null)
    {
        $name = is_null($name) ? $this->defaultLogger : $name;
        if (!isset($this->_loggers[$name])) {
            $this->_loggers[$name] = new Logger($name);
            $this->_setDefaultHandlers($this->_loggers[$name]);
        }
        return $this->_loggers[$name];
    }

    /**
     * Gets the logger for the channel with the provided name.
     * 
     * @param string $name The loggers's channel name to retreive.
     * 
     * @return \Monolog\Logger The logger object for the given channel name.
     */
    public static function logger($name = null)
    {
        $log = new Static();
        return $log->getLogger($name);
    }

    /**
     * Adds the default log handlers to the provided logger.
     * 
     * @param MonologLogger $logger The logger object to add the handlers.
     */
    protected function _setDefaultHandlers(\Monolog\Logger $logger)
    {
        if (empty($this->_handlers)) {

            //Verify folder
            new Folder(array('path' => $this->_logFilePath));

            array_push(
                $this->_handlers,
                new StreamHandler(
                    $this->_logFilePath . '/error.log',
                    Logger::WARNING
                )
            );
            array_push(
                $this->_handlers,
                new StreamHandler(
                    $this->_logFilePath . '/debug.log',
                    Logger::DEBUG
                )
            );
            array_push($this->_handlers, new ChromePHPHandler());
        }

        foreach ($this->_handlers as $handler) {
            $logger->pushHandler($handler);
        }
    }

}