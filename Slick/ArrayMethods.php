<?php

/**
 * ArrayMethods
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

/**
 * ArrayMethods
 * 
 * ArrayMethods is an utility class that encapsulates handy string operations
 *
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class ArrayMethods
{

    /**
     * Avoid the creation of an ArrayMethods instance.
     * @codeCoverageIgnore
     */
    private function __construct()
    {
        // do nothing
    }

    /**
     * Avoid the clonation of an ArrayMethods instance.
     * @codeCoverageIgnore
     */
    private function __clone()
    {
        // do nothing
    }

    /**
     * Converts a multidimensional array into a unidimensional array.
     *
     * @param array $array  The source array to iterate.
     * @param array $return The return values, for recursive proposes.
     * 
     * @return array A unidirectional array from source array.
     */
    public static function flatten($array, $return = array())
    {
        foreach ($array as $value) {
            if (is_array($value) || is_object($value)) {
                $return = self::flatten($value, $return);
            } else {
                $return[] = $value;
            }
        }
        return $return;
    }

    /**
     * Converts recursivly an array to an object.
     *
     * @param array $array The source array to convert.
     * 
     * @return Object An object with the values of given array.
     */
    public static function toObject($array)
    {
        $result = new \stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result->{$key} = self::toObject($value);
            } else {
                $result->{$key} = $value;
            }
        }
        return $result;
    }

    /**
     * Returns a copy of the given array without empty items.
     *
     * @param array $array The data array with empty items to clean.
     * 
     * @return array A copy of given array without empty items.
     */
    public static function clean($array)
    {
        return array_filter(
            $array, 
            function($item){
                return !empty($item);
            }
        );
    }

    /**
     * Trims every element of the given array.
     *
     * @param array $array The source array with items to trim.
     * 
     * @return array A copy of the given array with all items trimmed.
     */
    public static function trim($array)
    {
        return array_map(
            function($item) {
                return trim($item);
            },
            $array
        );
    }

    /**
     * Return the first element of the provided array.
     *
     * @param array $array The array to retreive the first element.
     * 
     * @return mixed First element of the provided array.
     */
    public static function first($array)
    {
        return reset($array);
    }

}
