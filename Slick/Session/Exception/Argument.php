<?php

/**
 * Argument
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Session\Exception;

use Slick\Core;

/**
 * Argument
 * 
 * Argument exception is throwed when an invalid argument is used.
 *
 * @package Slick
 * @subpackage Exception\Session
 */
class Argument extends Core\Exception
{

    /**
     * Error full description
     * @var string
     */
    protected $_description = "Trying to
            initialize the session with an unset driver type or the driver type
            that you have set isn't found.";
}
