<?php

/**
 * Driver
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Session;

use Slick\Base;
use Slick\Session\Exception;

/**
 * Generic Driver for framework Session handling.
 *
 * @package    Slick
 * @subpackage Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Driver extends Base
{

    /**
     * Store parser options from Session file.
     * @var array
     */
    protected $_parsed = array();

    /**
     * List of session messages
     *
     * @read
     * @var array
     */
    protected $_messages = array();

    /**
     * Returns an instance of the driver.
     *
     * @return \Slick\Session\Driver
     */
    public function initialize()
    {
        return $this;
    }

    /**
     * Overrides the base implementaion exception calling.
     * 
     * @param String $method The method name.
     * 
     * @return \Slick\Session\Exception\Implementation
     *  The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Sets a session message
     *
     * Sets a session message that can be displayed using the flushMessages
     * method. You can use a text and message type arguments or create a
     * Slick\Session\Message object to set a message.
     * 
     * @param string|\Slick\Session\Message $message The message text or object
     * @param string                        $type    The message type
     *
     * @return \Slick\Session\Driver A self instance for method call chain.
     *
     * @throws \Slick\Session\Exception\Argument If $message isn't a string or
     * a Slick\Session\Message object.
     */
    public function setMessage($message, $type = Message::TYPE_INFO)
    {
        if ($message instanceof Message || is_string($message)) {
            $message = (is_string($message)) ?
                new Message(
                    array('message' => $message, 'type' => $type)
                ) :
                $message;
        } else {
            throw new Exception\Argument(
                "Invalid argument for Session\Driver::setMessage(). " .
                "Use a string or a Slick\Session\Message"
            );
        }

        $this->_messages[$type][] = $message;
        $this->set('_messages_', $this->_messages);
        return $this;
    }

    /**
     * Set an information message to flash messages
     * 
     * @param string $message The message text or object
     *
     * @return \Slick\Session\Driver A self instance for method call chain.
     *
     * @throws \Slick\Session\Exception\Argument If $message isn't a string or
     * a Slick\Session\Message object.
     */
    public function setInfo($message)
    {
        return $this->setMessage($message, Message::TYPE_INFO);
    }

    /**
     * Set a seccess message to flash messages
     * 
     * @param string $message The message text or object
     *
     * @return \Slick\Session\Driver A self instance for method call chain.
     *
     * @throws \Slick\Session\Exception\Argument If $message isn't a string or
     * a Slick\Session\Message object.
     */
    public function setSuccess($message)
    {
        return $this->setMessage($message, Message::TYPE_SUCCESS);
    }

    /**
     * Set a warning message to flash messages
     * 
     * @param string $message The message text or object
     *
     * @return \Slick\Session\Driver A self instance for method call chain.
     *
     * @throws \Slick\Session\Exception\Argument If $message isn't a string or
     * a Slick\Session\Message object.
     */
    public function setWarning($message)
    {
        return $this->setMessage($message, Message::TYPE_WARNING);
    }

    /**
     * Set an error message to flash messages
     * 
     * @param string $message The message text or object
     *
     * @return \Slick\Session\Driver A self instance for method call chain.
     *
     * @throws \Slick\Session\Exception\Argument If $message isn't a string or
     * a Slick\Session\Message object.
     */
    public function setError($message)
    {
        return $this->setMessage($message, Message::TYPE_ERROR);
    }

    /**
     * Returns current flash messages
     * 
     * @return array A list of flash messages
     */
    public function getMessages()
    {
        return $this->get('_messages_', $this->_messages);
    }

    /**
     * Returns and deletes all flash messages.
     * 
     * @return array A list of flash messages
     */
    public function flushMessages()
    {
        $messages = $this->messages;
        $this->_messages = array();
        $this->set('_messages_', $this->_messages);
        return $messages;
    }

}

