<?php

/**
 * Message
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Session;

use Slick\Base;

/**
 * Message
 *
 * The message encapsulates a session message used on
 * Slick\Session::setMessage() method.
 *
 * @package    Slick
 * @subpackage Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Message extends Base
{
    /**#@+
     * @const string TYPE constant names
     */
    const TYPE_INFO     = 'info';
    const TYPE_WARNING  = 'warning';
    const TYPE_ERROR    = 'error';
    const TYPE_SUCCESS  = 'success';
    /**#@-*/

    /**
     * Message text
     *
     * @readwrite
     * @var string
     */
    protected $_message = '';

    /**
     * Message type
     *
     * @readwrite
     * @var string
     */
    protected $_type = self::TYPE_INFO;

    /**
     * Overrides the base implementaion exception calling.
     * 
     * @param String $method The method name.
     * 
     * @return \Slick\Session\Exception\Implementation
     *  The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }
}