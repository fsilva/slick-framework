<?php

/**
 * Server
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Session\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Session\Driver;

use Slick\Session;

/**
 * Server
 * 
 * Server class uses PHP's basic session implementation.
 *
 * @package    Slick
 * @subpackage Session\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Server extends Session\Driver
{

    /**
     * The session prefix
     * @readwrite
     * @var string
     */
    protected $_prefix = "slick_";

    /**
     * Sessin cookie name
     * @readwrite
     * @var string
     */
    protected $_name = 'PHPSESSID';

    /**
     * Sessin cookie domain
     * @readwrite
     * @var string
     */
    protected $_domain = null;

    /**
     * Sessin cookie lifetime
     * @readwrite
     * @var integer
     */
    protected $_lifetime = 0;

    /**
     * Session options
     * @var array
     */
    protected $_options = array(
        'domain' => null,
        'lifetime' => 0,
        'name' => 'PHPSESSID'
    );

    /**
     * Class constructor
     * 
     * Overrides Slick\Base::__construct to set the default session
     * parameters.
     *
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);

        session_set_cookie_params($this->lifetime, '/', $this->domain);
        session_name($this->name);
        @session_start();

    }

    /**
     * Returns the value store with provided key or the default value.
     *
     * @param string $key The key used to store the value in session.
     * @param string $default The default value if no value was stored.
     * 
     * @return mixed The stored value or the default value if key
     *  was not found.
     */
    public function get($key, $default = null)
    {
        $prefix = $this->prefix;

        if (isset($_SESSION[$prefix.$key])) {
            return $_SESSION[$prefix.$key];
        }

        return $default;
    }

    /**
     * Set/Stores a provided values with a given key.
     *
     * @param string $key The key used to store the value in session.
     * @param mixed $value The value to store under the provided key.
     * 
     * @return Slick/Session/Driver/Server Self instance for chain calls.
     */
    public function set($key, $value)
    {
        $prefix = $this->prefix;
        $_SESSION[$prefix.$key] = $value;
        return $this;
    }

    /**
     * Erases the values stored with the given key.
     *
     * @param string $key The key used to store the value in session.
     * 
     * @return Slick/Session/Driver/Server Self instance for chain calls.
     */
    public function erase($key)
    {
        $prefix = $this->prefix;
        unset($_SESSION[$prefix.$key]);
        return $this;
    }

    /**
     * Save session values before destruction.
     * @codeCoverageIgnore
     */
    public function __destruct()
    {
        session_commit();
    }
}
