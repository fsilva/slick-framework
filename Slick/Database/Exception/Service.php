<?php

/**
 * Service
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database\Exception;

use Slick\Core;

/**
 * Service
 * 
 * Service exception throwed when cannot connect to the service.
 *
 * @package    Slick
 * @subpackage Exception\Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Service extends Core\Exception
{

    /**
     * Stores the service error for this exception.
     * @var string
     */
    protected $_error;

    /**
     * Overrides the parent constructor to add the service error.
     * 
     * @param string $message Exception error message.
     * @param string $error Service error description.
     */
    public function __construct($message, $error = null)
    {
        parent::__construct($message);
        $this->_error = $error;
    }
}
