<?php

/**
 * Sql
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database\Exception;

use Slick\Core;

/**
 * Sql
 * 
 * Sql exception is throwed when the service returns an error for
 * the provided query.
 *
 * @package    Slick
 * @subpackage Exception\Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Sql extends Core\Exception
{

    /**
     * Stores the service error for this exception.
     * @var string
     */
    protected $_error;

    /**
     * Stores the sql query.
     * @var string
     */
    protected $_sql;

    /**
     * Error full description
     * @var string
     */
    protected $_description = "Check your SQL query for the given error.";

    /**
     * Overrides the parent constructor to add the service error and sql.
     *
     * @param string $message Exception error message.
     * @param string $error   Service error description.
     * @param string $sql T   he requested sql query.
     */
    public function __construct($message, $error = null, $sql = null)
    {
        parent::__construct($message);
        $this->_error = $error;
        $this->_sql = $sql;
    }

    /**
     * Returns current description
     * 
     * @return string Error description.
     */
    public function getDescription()
    {
        $description = $this->_description;
        if (Core::$debugMode > 1) {
            $description .= "<br /><strong>Error: </strong>" .
                "{$this->_error}<br />";
            $sql = \SqlFormatter::format($this->_sql);
            $description .= "<strong>Query: </strong>{$sql}";
        }
        return $description;
    }
}
