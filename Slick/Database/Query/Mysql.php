<?php

/**
 * Mysql
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database\Query;

use Slick\Database;
use Slick\Database\Exception;

/**
 * Mysql
 * 
 * Mysql query implementation.
 *
 * @package    Slick
 * @subpackage Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Mysql extends Database\Query
{

    /**
     * Returns a variable number of rows based on the select query performed.
     *
     * @return array A list of rows.
     */
    public function all()
    {
        $sql = $this->_buildSelect();

        $result = $this->_connector->execute($sql);

        if ($result === false) {
            throw new Exception\Sql(
                "Error when trying to execute query",
                $this->_connector->lastError,
                $sql
            );
        }

        $rows = array();
        $fields = $result->fetch_fields();
        // @codingStandardsIgnoreStart
        for ($i = 0; $i < $result->num_rows; $i++) {
            // @codingStandardsIgnoreEnd
            $row = $result->fetch_array(MYSQLI_NUM);
            $data = array();
            foreach ($fields as $key => $field) {
                $data[$field->table][$field->name] = $row[$key];
            }
            $rows[] = $data;
        }

        return $rows;
    }
}
