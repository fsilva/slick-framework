<?php

/**
 * Sqlite
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database\Query;

use Slick\Database as Database;
use Slick\Database\Exception as Exception;

/**
 * Sqlite
 * 
 * Sqlite query implementation.
 *
 * @package    Slick
 * @subpackage Database\Query
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Sqlite extends Database\Query
{

    /**
     * Cretates an INSERT query for the given data.
     *
     * @param array|Object $data The data to insert.
     * 
     * @return string The INSERT query statement for given data.
     */
    protected function _buildInsert($data)
    {
        $fields = array();
        $value = array();
        $template = "INSERT INTO %s (%s) VALUES (%s)";

        foreach ($data as $field => $value) {
            $fields[] = $field;
            $values[] = $this->_quote($value);
        }

        $fields = join(", ", $fields);
        $values = join(', ', $values);

        return sprintf($template, $this->from, $fields, $values);
    }

    /**
     * Creates an UPDATE query for the given data.
     *
     * @param array|Object $data The data to update.
     * 
     * @return string The UPDATE query statement for given data.
     */
    protected function _buildUpdate($data)
    {
        $parts = array();
        $where = '';
        $template = "UPDATE %s SET %s %s";

        foreach ($data as $field => $value) {
            $parts[] = "{$field} = " . $this->_quote($value);
        }

        $parts = join(", ", $parts);

        $where = $this->_getWhereClause();

        return sprintf($template, $this->from, $parts, $where);
    }

    /**
     * Creates the DELETE query.
     *
     * @return string The DELETE query statement.
     */
    protected function _buildDelete()
    {
        $where = '';
        $template = "DELETE FROM %s %s";

        $where = $this->_getWhereClause();

        return sprintf($template, $this->from, $where);
    }

    /**
     * Returns a variable number of rows based on the select query performed.
     *
     * @return array A list of rows.
     */
    public function all()
    {
        $sql = $this->_buildSelect();

        $result = $this->_connector->execute($sql);

        if ($result === false) {
            throw new Exception\Sql(
                "Error when trying to execute query",
                $this->_connector->lastError,
                $sql
            );
        }

        $rows = array();

        while ($res = $result->fetchArray(SQLITE3_ASSOC)) {
            $rows[] = $res;
        }

        return $rows;
    }
}
