<?php

/**
 * Connector
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database;

use Slick\Base;
use Slick\Database\Exception;

/**
 * Connector
 * 
 * Abstract class defining a database connector type.
 *
 * @package    Slick
 * @subpackage Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Connector extends Base implements ConnectorInterface
{

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param String $method The method name.
     * 
     * @return Slick\DatabaseException\Implementation
     *   The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Returns a self instance of connetor.
     *
     * @return \Slick\Database\Connector
     */
    public function initialize()
    {
        return $this;
    }

}
