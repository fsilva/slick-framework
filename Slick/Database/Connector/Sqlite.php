<?php

/**
 * Sqlite
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database\Connector;

use Slick\Database;
use Slick\Database\Exception;

/**
 * Sqlite
 * 
 * SQLite3 database connecor
 *
 * @package    Slick
 * @subpackage Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Sqlite extends Database\Connector
{

    /**
     * SQLite3 resource connector
     * @readwrite
     * @var  \SQLite3
     */
    protected $_service;

    /**
     * The database file
     * @readwrite
     * @var string
     */
    protected $_file = ':memory:';

    /**
     * Database open mode flags
     * @readwrite
     * @var int
     */
    protected $_flags;

    /**
     * The encryption key
     * @readwrite
     * @var string
     */
    protected $_key;

    /**
     * The mysql connection state.
     * @readwrite
     * @var string
     */
    protected $_isConnected = false;

    /**
     * Overrides the constructor to add the default flags property.
     * 
     * @param array $options An array of key/value pairs of values. 
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->flags = SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE;
    }

    /**
     * Checks if connected to database.
     *
     * @return boolean The connection state. True if connected,
     *   false otherwise.
     */
    protected function _isValidService()
    {
        $isEmpty = empty($this->_service);
        $isInstance = $this->_service instanceof \SQLite3;

        if ($this->isConnected && $isInstance && !$isEmpty) {
            return true;
        }

        return false;
    }

    /**
     * Connects to SQLite3 service.
     *
     * @return \Slick\Database\Connector\Sqlite A self instance for
     *   chain method calls.
     */
    public function connect()
    {
        if (!$this->_isValidService()) {
            try {
                $this->_service = new \SQLite3(
                    $this->_file,
                    $this->_flags,
                    $this->_key
                );
            } catch(\Exception $e) {
                throw new Exception\Service(
                    "Unable to connect to database service",
                    $e->getMessage()
                );
            }

            $this->_isConnected = true;
        }
        return $this;
    }

    /**
     * Disconnects from SQLite3 service.
     *
     * @return \Slick\Database\Connector\Sqlite A self instance
     *   for chain method calls.
     */
    public function disconnect()
    {
        if ($this->_isValidService()) {
            $this->isConnected = false;
            $this->_service->close();
        }
        return $this;
    }

    /**
     * Returns a corresponding query instance.
     *
     * @return \Slick\Database\Connector\Sqlite
     */
    public function query()
    {
        return new Database\Query\Sqlite(
            array(
                'connector' => $this
            )
        );
    }

    /**
     * Executes the provided SQL statement.
     *
     * @param string $sql The SQL statment to execute.
     * 
     * @return mixed The \SQLIte3::query() result.
     * @see \SQLIte3::query()
     */
    public function execute($sql)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        $regex = "#(select)#i";
        if (preg_match($regex, $sql)) {
            $result =@ $this->_service->query($sql);
        } else {
            $result =@ $this->_service->exec($sql);
        }

        if (!$result) {
            return false;
        }

        return $result;

    }

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        return $this->_service->escapestring($value);
    }

    /**
     * Returns the ID of the last row to be inserted.
     *
     * @return integer The last insertd ID value.
     */
    public function getLastInsertId()
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        return $this->_service->lastInsertRowID();
    }

    /**
     * Returns the number of rows affected by the last SQL query executed.
     *
     * @return integer The number of rows affected by last query.
     */
    public function getAffectedRows()
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        return $this->_service->changes();
    }

    /**
     * Returns the last error of occur.
     *
     * @return string The last error of occur.
     */
    public function getLastError()
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }

        return $this->_service->lastErrorMsg();
    }

    /**
     * Creates the table if it
     * @param  Slick\Model $model The mobdel object to sync.
     * 
     * @return \Slick\Database\Connector
     */
    public function sync(\Slick\Model $model)
    {
        $lines = array();
        $indices = array();
        $columns = $model->getColumns();
        $template = "CREATE TABLE '%s' (\n%s)";

        foreach ($columns as $column) {
            $raw = $column['raw'];
            $name = $column['name'];
            $type = $column['type'];
            $length = $column['length'];

            if ($column['index']) {
                $indices[] = "KEY '{$name}' ('{$name}')";
            }

            switch ($type) {
                case 'autonumber':
                    if ($column['primary']) {
                        $lines[] = 
                            "'{$name}' INTEGER PRIMARY KEY AUTOINCREMENT";
                    } else {
                        $lines[] = "'{$name}' INTEGER NOT NULL";
                    }
                    break;

                case 'text':
                    if ($length !== null && $length <= 255) {
                        $lines[] = 
                            "'{$name}' VARCHAR({$length}) DEFAULT NULL";
                    } else {
                        $lines[] = "'{$name}' TEXT";
                    }
                    break;

                case 'integer':
                case 'boolean':
                    $lines[] = "'{$name}' INTEGER DEFAULT NULL";
                    break;

                case 'decimal':
                    $lines[] = "'{$name}' REAL DEFAULT NULL";
                    break;

                case 'datetime':
                    $lines[] = "'{$name}' DATETIME DEFAULT NULL";
                    break;

            }
        }

        $ind = (!empty($indices)) ? sprintf(
            ",\n%s\n",
            join(",\n", $indices)
        ) : '';
        $table = $model->table;
        $sql = sprintf(
            $template,
            $table,
            join(",\n", $lines),
            $ind
        );

        $result = $this->execute("DROP TABLE IF EXISTS {$table}");
        if ($result === false) {
            throw new Exception\Sql(
                "There was an error executing the query",
                $this->lastError,
                "DROP TABLE IF EXISTS {$table}"
            );
        }

        $result = $this->execute($sql);
        if ($result === false) {
            throw new Exception\Sql(
                "There was an error executing the query",
                $this->lastError, $sql
            );
        }

        return $this;
    }

}
