<?php

/**
 * Mysql
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database\Connector;

use Slick\Database as Database;
use Slick\Database\Exception as Exception;

/**
 * Mysql
 * 
 * Mysql connector
 *
 * @package    Slick
 * @subpackage Database\Connector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Mysql extends Database\Connector
{

    /**
     * Mysql resource connector
     * @readwrite
     * @var  \MySQLi
     */
    protected $_service;

    /**
     * The mysql host name
     * @readwrite
     * @var string
     */
    protected $_host;

    /**
     * The mysql user name
     * @readwrite
     * @var string
     */
    protected $_username;

    /**
     * The mysql password
     * @readwrite
     * @var string
     */
    protected $_password;

    /**
     * The mysql schema name
     * @readwrite
     * @var string
     */
    protected $_schema;

    /**
     * The mysql port
     * @readwrite
     * @var string
     */
    protected $_port = '3306';

    /**
     * The mysql charset to use
     * @readwrite
     * @var string
     */
    protected $_charset = 'utf8';

    /**
     * The mysql engine to use.
     * @readwrite
     * @var string
     */
    protected $_engine = 'InnoDB';

    /**
     * The mysql connection state.
     * @readwrite
     * @var string
     */
    protected $_isConnected = false;

    /**
     * Checks if connected to database.
     *
     * @return boolean The connection state. True if connected,
     * false otherwise.
     */
    protected function _isValidService()
    {
        $isEmpty = empty($this->_service);
        $isInstance = $this->_service instanceof \MySQLi;

        if ($this->isConnected && $isInstance && !$isEmpty) {
            return true;
        }

        return false;
    }

    /**
     * Connects to MySQL service.
     *
     * @return \Slick\Database\Connector\Mysql A self instance for
     *   chain method calls.
     */
    public function connect()
    {
        if (!$this->_isValidService()) {
            //ignore the PHP fatal error, we will test the connection
            //imidiatly after this instruction.
            $this->_service =@ new \MySQLi(
                $this->_host,
                $this->_username,
                $this->_password,
                $this->_schema,
                $this->_port
            );

            if ($this->_service->connect_errno) {
                throw new Exception\Service(
                    "Unable to connect to database service",
                    $this->_service->connect_error
                );
            }

            $this->_isConnected = true;
        }
        return $this;
    }

    /**
     * Disconnects from MySQL service
     * 
     * @return \Slick\Database\Connector\Mysql A self instance for
     *   chain method calls.
     */
    public function disconnect()
    {
        if ($this->_isValidService()) {
            $this->isConnected = false;
            $this->_service->close();
        }
        return $this;
    }

    /**
     * Returns a corresponding query instance
     * 
     * @return \Slick\Database\Query\Mysql
     */
    public function query()
    {
        return new Database\Query\Mysql(
            array(
                'connector' => $this
            )
        );
    }

    /**
     * Executes the provided SQL statement.
     *
     * @param string $sql The SQL statment to execute.
     * 
     * @return mixed The \MySQL::query() result.
     * @see \MySQL::query()
     */
    public function execute($sql)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service(
                "Not connected to a valid service"
            );
        }
        return $this->_service->query($sql);
    }

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        return $this->_service->real_escape_string($value);
    }

    /**
     * Returns the ID of the last row to be inserted.
     *
     * @return integer The last insertd ID value.
     */
    public function getLastInsertId()
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        return $this->_service->insert_id;
    }

    /**
     * Returns the number of rows affected by the last SQL query executed.
     *
     * @return integer The number of rows affected by last query.
     */
    public function getAffectedRows()
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }
        return $this->_service->affected_rows;
    }

    /**
     * Returns the last error of occur.
     *
     * @return string The last error of occur.
     */
    public function getLastError()
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Not connected to a valid service");
        }

        return $this->_service->error;
    }

    /**
     * Synconizes the model definition with database table.
     *
     * @param  Slick\Model $model The mobdel object to sync.
     * @return Slick\Database\Connector
     */
    public function sync(\Slick\Model $model)
    {
        $lines = array();
        $indices = array();
        $columns = $model->getColumns();
        $template = 
            "CREATE TABLE %s (\n%s,\n%s\n) ENGINE=%s DEFAULT CHARSET=%s";

        foreach ($columns as $column) {
            $raw = $column['raw'];
            $name = $column['name'];
            $type = $column['type'];
            $length = $column['length'];

            if ($column['primary']) {
                $indices[] = "PRIMARY KEY (`{$name}`)";
            }

            if ($column['index']) {
                $indices[] = "KEY `{$name}` (`{$name}`)";
            }

            switch ($type) {
                case 'autonumber':
                    $lines[] = "`{$name}` int(11) NOT NULL AUTO_INCREMENT";
                    break;

                case 'text':
                    if ($length !== null && $length <= 255) {
                        $lines[] = "`{$name}` varchar({$length}) DEFAULT NULL";
                    } else {
                        $lines[] = "`{$name}` text";
                    }
                    break;

                case 'integer':
                    $lines[] = "`{$name}` int(11) DEFAULT NULL";
                    break;

                case 'boolean':
                    $lines[] = "`{$name}` int(4) DEFAULT NULL";
                    break;

                case 'decimal':
                    $lines[] = "`{$name}` float DEFAULT NULL";
                    break;

                case 'datetime':
                    $lines[] = "`{$name}` datetime DEFAULT NULL";
                    break;

            }
        }

        $table = $model->getTable();
        $sql = sprintf(
            $template,
            $table,
            join(",\n", $lines),
            join(",\n", $indices),
            $this->_engine,
            $this->_charset
        );

        $result = $this->execute("DROP TABLE IF EXISTS {$table}");
        if ($result === false) {
            throw new Exception\Sql(
                "There was an error executing the query",
                $this->lastError,
                "DROP TABLE IF EXISTS {$table}"
            );
        }

        $result = $this->execute($sql);
        if ($result === false) {
            throw new Exception\Sql(
                "There was an error executing the query",
                $this->lastError,
                $sql
            );
        }

        return $this;
    }
}
