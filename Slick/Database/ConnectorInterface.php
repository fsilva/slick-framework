<?php

/**
 * ConnectorInterface
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Database;

/**
 * ConnectorInterface
 * 
 * Interface for database interface connectors.
 *
 * @package    Slick
 * @subpackage Database
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
interface ConnectorInterface
{

    /**
     * Connects to database service.
     *
     * @return \Slick\Database\Connector
     *   A self instance for chain method calls.
     */
    public function connect();

    /**
     * Disconnects from database service.
     * 
     * @return \Slick\Database\Connector
     *   A self instance for chain method calls.
     */
    public function disconnect();

    /**
     * Returns a corresponding query instance.
     * 
     * @return \Slick\Database\Query
     */
    public function query();

    /**
     * Executes the provided SQL statement.
     *
     * @param string $sql The SQL statment to execute.
     * 
     * @return mixed The \Slick\Database\Connector::query() result.
     * @see \Slick\Database\Connector::query()
     */
    public function execute($sql);

    /**
     * Escapes the provided value to make it safe for queries.
     *
     * @param string $value The value to escape.
     * 
     * @return string A safe string for queries.
     */
    public function escape($value);

    /**
     * Returns the ID of the last row to be inserted.
     *
     * @return integer The last insertd ID value.
     */
    public function getLastInsertId();

    /**
     * Returns the number of rows affected by the last SQL query executed.
     *
     * @return integer The number of rows affected by last query.
     */
    public function getAffectedRows();

    /**
     * Returns the last error of occur.
     *
     * @return string The last error of occur.
     */
    public function getLastError();

    /**
     * Synconizes the model definition with database table.
     *
     * @param \Slick\Model $model The mobdel object to sync.
     * 
     * @return \Slick\Database\Connector
     */
    public function sync(\Slick\Model $model);
}