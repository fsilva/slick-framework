<?php

/**
 * Session
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base;
use Slick\Events;
use Slick\Session;
use Slick\Registry;
use Slick\Session\Exception;

/**
 * Session handling factory for Slick framework.
 *
 * @package    Slick
 * @subpackage Session
 * @author     Filipe Silva <silvam.filipe@gmail.com>n
 */
class Session extends Base
{

    /**
     * Session type.
     *
     * @readwrite
     * @var string
     */
    protected $_type = 'server';

    /**
     * A list of options for Session.
     *
     * @readwrite
     * @var array
     */
    protected $_options;

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * @return \Slick\Session\Exception\Implementation
     *   The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Returns a new session driver based on the type property.
     *
     * It fires the 'session.before.initialize' and 'session.after.initialize'
     * events using parameters: 
     *     $type    -> Type of the driver required,
     *     $options -> Initialization options for the session resource.
     *
     * @return /Slick/Session/Driver A new Session driver based on type.
     */
    public function initialize()
    {
        $type = $this->getType();

        if (empty($type)) {
            throw new Exception\Argument("Invalid session type");
        }

        Events::fire('session.before.initialize', array($this));

        switch ($type) {
            case 'server':
                $driver = new Session\Driver\Server($this->getOptions());
                break;

            default:
                throw new Exception\Argument("Unimplemented session type");
        }
        Events::fire('session.after.initialize', array($this, $driver));
        return $driver;
    }
}
