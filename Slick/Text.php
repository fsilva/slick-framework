<?php

/**
 * Text
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

/**
 * Text
 * 
 * Text is an utility class that encapsulates handy string operations
 *
 * @package    Slick
 * @subpackage Utility
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Text
{

    /**
     * Avoid the creation of an Text instance.
     * @codeCoverageIgnore
     */
    private function __construct()
    {
        // do nothing
    }

    /**
     * Avoid the clonation of an Text instance.
     * @codeCoverageIgnore
     */
    private function __clone()
    {
        // do nothing
    }

    /**
     * Patter delimiter character
     *
     * @var char
     */
    private static $_delimiter = '#';

    /**
     * English singular form rules
     *
     * @var array
     */
    private static $_singular = array(
        '(matr)ices$' => "\\1ix",
        '(vert|ind)ices$' => "\\1ex",
        '^(ox)en' => "\\1",
        '(alias)es$' => "\\1",
        '([octop|vir])i$' => "\\us",
        '(cris|ax|test)es$' => "\\1is",
        '(shoe)s$' => "\\1",
        '(o)es$' => "\\1",
        '(bus|campus)es$' => "\\1",
        '([m|l])ice$' => "\\1ouse",
        '(x|ch|ss|sh)es$' => "\\1",
        '(m)ovies$' => "\\1\\2ovie",
        '(s)eries$' => "\\1\\2eries",
        '([^aeiouy]|qu)ies$' => "\\1y",
        '([lr])ves$' => "\\1f",
        '(tive)s$'=> "\\1",
        '(hive)s$'=> "\\1",
        '([^f])ves$' => "\\1fe",
        '(^analy)ses$' => "\\sis",
        '((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$'
            => "\\1\\2sis",
        '([ti])a$' => "\\1um",
        '(p)eople$' => "\\1\\2erson",
        '(m)en$' => "\\1an" ,
        '(s)tatuses$' => "\\1\\2tatus",
        '(c)hildren$' => "\\1\\2hild",
        '(n)ews$' => "\\1\\2ews",
        '([^u])s$' => "\\1"
    );

    /**
     * English plural form rules
     *
     * @var array
     */
    private static $_plural = array(
        '^(ox)$' => "\\1\\2en",
        '([m|l])ouse$' => "\\1ice",
        '(matr|vert|ind)ix|ex$' => "\\1ices",
        '(x|ch|ss|sh)$' => "\\1es",
        '([^aeiouy]|qu)y$' => "\\1ies",
        '(hive)$' => "\\1s",
        "(?:([^f])fe|([lr])f)$" => "\\1\\2ves",
        'sis$' => "ses",
        '([ti])um$' => "\\1a",
        '(p)erson$' => "\\1eople",
        '(m)an$' => "\\1en",
        '(c)hild$' => "\\1hildren",
        '(buffal|tomat)o$' => "\\1\\2oes",
        '(bu|campu)s$' => "\\1\\2ses",
        '(alias|status|virus)' => "\\1es",
        '(octop)us$' => "\\1i",
        '(ax|cris|test)is' => "\\1es",
        's$' => 's',
        '$' => 's'
    );

    /**
     * Returns the singular version of the given string.
     *
     * @param string $string Singular string to evaluate.
     * 
     * @return string The singular version of the provided string.
     */
    public static function singular($string)
    {
        $result = $string;
        foreach (self::$_singular as $rule => $replacement) {
            $rule = self::_normalize($rule);
            if (preg_match($rule, $string)) {
                $result = preg_replace($rule, $replacement, $string);
                break;
            }
        }
        return $result;
    }

    /**
     * Returns the plural version of the given string.
     *
     * @param string $string Plural string to evaluate.
     * 
     * @return string The plural version of the provided string.
     */
    public static function plural($string)
    {
        $result = $string;
        foreach (self::$_plural as $rule => $replacement) {
            $rule = self::_normalize($rule);
            if (preg_match($rule, $string)) {
                $result = preg_replace($rule, $replacement, $string);
                break;
            }
        }
        return $result;
    }

    /**
     * Sanitize the given string escaping the mask items on it.
     *
     * @param string       $string The string that will be sanitized.
     * @param array|string $mask   The list of characters to escape.
     * 
     * @return string The original string sanitized.
     */
    public static function sanitize($string, $mask)
    {
        if (is_array($mask)) {
            $parts = $mask;
        } else if (is_string($mask)) {
            $parts = str_split($mask);
        } else {
            return $string;
        }

        foreach ($parts as $part) {
            $normalize = self::_normalize("\\{$part}");
            $string = preg_replace("{$normalize}m", "\\{$part}", $string);
        }

        return $string;
    }

    /**
     * Remove duplicate characters of a given string.
     *
     * @param string $string Source string to check.
     * 
     * @return string The unique characters ot the given string.
     */
    public static function unique($string)
    {
        $unique = "";
        $parts = str_split($string);
        foreach ($parts as $part) {
            if (!strstr($unique, $part)) {
                $unique .= $part;
            }
        }
        return $unique;
    }

    /**
     * Find the numeric position of the first occurrence of needle in
     * the haystack string.
     *
     * It will return -1 if the substring wasn't found within the given string.
     * This is a wrapper to PHP strpos that will always return an
     * integer value.
     *
     * @param string  $haystack The string to search in.
     * @param string  $needle   If needle is not a string, it is converted to
     *  an integer and applied as the ordinal value of a character.
     * @param integer $offset   If specified, search will start this number
     *  of characters counted from the beginning of the string. Unlike
     *  strrpos() and strripos(), the offset cannot be negative.
     *  
     * @return integer Returns the position of where the needle exists relative
     *  to the beginning of the haystack string (independent of offset),
     *  or -1 if needle wasn't found in the haystack.
     */
    public static function indexOf($haystack, $needle, $offset = 0)
    {
        $position = strpos($haystack, $needle, $offset);
        if (!is_int($position)) {
            return -1;
        }
        return $position;
    }

    /**
     * Normalize the given pattern so that the remaining methods can operate
     * on it without first having to check or normalize it.
     *
     * @param string $pattern The pattern string to normalize.
     * 
     * @return string A normalized pattern string.
     */
    private static function _normalize($pattern)
    {
        return self::$_delimiter . trim($pattern, self::$_delimiter)
            . self::$_delimiter;
    }

    /**
     * Set delimiter character.
     * 
     * @param char $delimiter The character to set delimiter.
     */
    public static function setDelimiter($delimiter)
    {
        self::$_delimiter = $delimiter;
    }

    /**
     * Returns current delimiter character.
     *
     * @return char The delimiter charecter.
     */
    public static function getDelimiter()
    {
        return self::$_delimiter;
    }

    /**
     * Perform a global regular expression match less formal then
     * preg_match_all() function, returning the predictable matches.
     *
     * @param string $string  The string to match against.
     * @param string $pattern The regular expression pattern string.
     * 
     * @return array|null An array with matches from the given string. If no
     * match is found, null will be returned.
     */
    public static function match($string, $pattern)
    {
        preg_match_all(
            self::_normalize($pattern),
            $string,
            $matches,
            PREG_PATTERN_ORDER
        );

        if (!empty($matches[1])) {
            return $matches[1];
        }

        if (!empty($matches[0])) {
            return $matches[0];
        }

        return null;
    }

    /**
     * Split string by a regular expression.
     *
     * This is a less formal method the the preg_split() function.
     *
     * @param string  $string The string to split.
     * @param string  $pattern The regular expression for split operation.
     * @param integer $limit If specified, then only substrings up to limit
     *  are returned with the rest of the string being placed in the
     *  last substring.
     *  
     * @return array Returns an array containing substrings of subject
     *  split along boundaries matched by pattern.
     */
    public static function split($string, $pattern, $limit = null)
    {
        $flags = PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE;
        return preg_split(self::_normalize($pattern), $string, $limit, $flags);
    }

    /**
     * Get the index of the last occurence in a string.
     * 
     * @param string $haystack The string to search in.
     * @param string $needle If needle is not a string, it is converted to
     *  an integer and applied as the ordinal value of a character.
     *  
     * @return int Returns the position of where the needle exists relative
     *  to the beginning of the haystack string (independent of offset),
     *  or -1 if needle wasn't found in the haystack.
     */
    public static function lastIndexOf($needle, $haystack)
    { 
        if (strstr($haystack, $needle)!="") { 
            return(strlen($haystack)-strpos(strrev($haystack), $needle)); 
        } 
        return(-1); 
    } 

    /**
     * Truncate text
     *
     * Cuts a string to the length of $length and replaces the last characters
     * with the ellipsis if the text is longer then length.
     * 
     * @param string  $text String to truncate.
     * @param integer $length Length of returned string, including ellipsis.
     * @param string  $elipsis The terminator text if text was truncated.
     * 
     * @return string Trimmed string.
     */
    public static function truncate($text, $length = 200, $elipsis = '...')
    {
        if (strlen($text) <= $length) {
            return $text;
        }

        $end = self::lastIndexOf(' ', substr($text, 0, $length));
        return trim(substr($text, 0, $end)) . $elipsis;
    }

    /**
     * Splits a whole text into an array of lines with a given $length
     * 
     * @param string  $text String to split.
     * @param integer $length Length for each line.
     * 
     * @return array An array of lines from the provided text.
     */
    public static function splitInLines($text, $length = 120)
    {
        $lines = array();

        //Text is smaller then the max length
        if (strlen($text) <= $length) {
            return array($text);
        }

        while (strlen($text) > 0) {
            $line = self::truncate($text, $length, '');
            $lines[] = trim($line);
            $text = str_replace($line, '', $text);
        }
        return $lines;
    }
}
