<?php

/**
 * Registry
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

/**
 * Registry
 * 
 * Registry will store framework instances.
 *
 * @package    Slick
 * @subpackage Model
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Registry
{

    /**
     * List of stored instances.
     *
     * @var array
     */
    private static $_instances = array();

    /**
     * Avoid the creation of an Registry instance.
     * @codeCoverageIgnore
     */
    private function __construct()
    {
        // do nothing
    }

    /**
     * Avoid the clonation of an Registry instance.
     * @codeCoverageIgnore
     */
    private function __clone()
    {
        // do nothing
    }

    /**
     * Retrieves an instance of a class stored with a given key.
     * 
     * If the key is not fount in the internal instances list it will
     * return the default value, if passed, or null.
     *
     * @param string $key     The key that was used to store the class
     *   instance.
     * @param Object $default The default value to return if key was
     *   not in the list.
     * 
     * @return Object The stored class instance.
     */
    public static function get($key, $default = null)
    {
        if (isset(self::$_instances[$key])) {
            return self::$_instances[$key];
        }
        return $default;
    }

    /**
     * Stores an object instance in the registry.
     * 
     * It will check if instance had 'registryInsert' callback method defined
     * and will call it after adding it to the list.
     *
     * @param string $key      The key used to store the object instance.
     * @param object $instance The object instance to store.
     */
    public static function set($key, $instance = null)
    {
        self::$_instances[$key] = $instance;
        if (method_exists($instance, 'registryInsert')) {
            $instance->registryInsert();
        }
    }

    /**
     * Erase an class instance stored with a give key.
     * 
     * It will check if instance had 'registryRemove' callback method defined
     * and will call it after removing it to the list.
     *
     * @param string $key The key that was used to store the class instance.
     */
    public static function erase($key)
    {
        if (isset(self::$_instances[$key])) {
            $i = self::$_instances[$key];
            unset(self::$_instances[$key]);
            if (method_exists($i, 'registryRemove')) {
                $i->registryRemove();
            }
        }
    }

    /**
     * Retrives a key/value pairs array for stored objects.
     * 
     * @return array The list of stored objects.
     */
    public static function getAll()
    {
        return self::$_instances;
    }

}
