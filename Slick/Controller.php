<?php

/**
 * Controller
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Controller
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Core;
use Slick\Base;
use Slick\View;
use Slick\Events;
use Slick\Router;
use Slick\Registry;
use Slick\Controller\Exception;

/**
 * Controller
 *
 * @package    Slick
 * @subpackage Controller
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Controller extends Base
{

    /**
     * The controller name.
     * @read
     * @var string
     */
    protected $_name;

    /**
     * Request parameters
     * @readwrite
     * @var array
     */
    protected $_parameters;

    /**
     * The layout view
     * @readwrite
     * @var Slick\View
     */
    protected $_layoutView;

    /**
     * The action view
     * @readwrite
     * @var Slick\View
     */
    protected $_actionView;

    /**
     * A flag to tell framework to render or not the layout view.
     * @readwrite
     * @var boolean
     */
    protected $_willRenderLayoutView = false;

    /**
     * A flag to tell framework to render or not the action view.
     * @readwrite
     * @var boolean
     */
    protected $_willRenderActionView = true;

    /**
     * The default layout to use in action rendering.
     * @readwrite
     * @var string
     */
    protected $_defaultLayout = 'Layouts/default';

    /**
     * The default request extension.
     * @readwrite
     * @var string
     */
    protected $_defaultExtension = 'html';

    /**
     * The requested extension
     * @readwrite
     * @var string
     */
    protected $_extension;

    /**
     * The default content type header text;
     * @readwrite
     * @var string
     */
    protected $_defaultContentType = 'text/html';

    /**
     * List of available content types.
     * @read
     * @var array
     */
    protected $_contentTypes = array(
        'html' => 'text/html',
        'xml' => 'text/xml'
    );

    /**
     * Data for the views
     * @read
     * @var array
     */
    protected $_viewVars = array();

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param String $method The method name.
     * 
     * @return \Slick\Controller\Exception\Implementation
     *  The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Overrides the base constructor to set the views for action and layout.
     *
     * it fires the 'controller.before.construct' and
     * 'controller.after.construct' events using
     * parameter: $nams -> the controller class name.
     *
     * @param array|Object $options The properties for the object
     *  beeing constructed.
     */
    public function __construct($options = array())
    {
        Events::fire('controller.before.construct', array($this->getName()));
        parent::__construct($options);

        $defaultLayout = $this->getDefaultLayout();
        $defaultExtension = $this->getExtension();

        if ($this->getWillRenderLayoutView()) {
            $this->setLayout($this->_defaultLayout);
        }

        if ($this->getWillRenderActionView()) {
            $router = Registry::get("router", new Router());
            $controller = ucfirst($router->getController());
            $action = $router->getAction();

            $view = new View(
                array(
                    'file' => 
                        "{$controller}/" . "{$action}.{$defaultExtension}"
                )
            );

            $this->setActionView($view);
        }
        Events::fire(
            'controller.after.construct',
            array($this, $this->getName())
        );
    }


    /**
     * Renders the action and/or template view(s).
     *
     * @return string The rendered ouput string
     * 
     * @throws \Slick\View\Exception\Renderer if template fires
     *  any parse/process errors.
     */
    protected function _render()
    {
        $defaultContentType = $this->getContentType();
        $results = null;

        $doAction = $this->getWillRenderActionView() && $this->getActionView();
        $doLayout = $this->getWillRenderLayoutView() && $this->getLayoutView();
        $this->_set('_session', Registry::get('session'));

        try {

            if ($doAction) {
                $view = $this->getActionView();
                $view->set($this->_viewVars);
                $results = $view->render();
            }

            $this->response->header('Content-type', $defaultContentType);

            if ($doLayout) {
                $view = $this->getLayoutView();
                $view->set("layout_data", $results);
                $view->set($this->_viewVars);
                $results = $view->render();

                return  $results;

            } else if ($doAction) {
                $this->disableRendering();
                return $results;
            }

        }catch (\Exception $e) {
            $message = $e->getMessage();
            throw new View\Exception\Renderer(
                "Error while rendering view",
                0,
                $e
            );
        }
        return $results;
    }

    /**
     * Disables all ouput rendering.
     * 
     * @param boolean $set A flag to disable/enable the rendering operations.
     */
    public function disableRendering($set = true)
    {
        if ($set) {
            $this->willRenderLayoutView = false;
            $this->willRenderActionView = false;
        } else {
            $this->willRenderLayoutView = true;
            $this->willRenderActionView = true;
        }
    }

    /**
     * Renders the action and/or template view(s).
     *
     * It fires the 'controller.before.render' and 'controller.after.render'
     * events using parameter: $controller -> The controller it self;
     *     $output -> The render output.
     *
     * @return string The rendered ouput string
     * 
     * @throws \Slick\View\Exception\Renderer if template fires
     *  any parse/process errors.
     */
    public function render()
    {
        Events::fire('controller.before.render', array($this));
        $output = $this->_render();
        Events::fire('controller.after.render', array($this, $output));
        return $output;
    }

    /**
     * Sets a different layout file.
     * 
     * @param string $layout The layout file name without the extension
     *
     * @return \Slick\Controller A self instance for method call chain.
     */
    public function setLayout($layout)
    {
        $defaultExtension = $this->getExtension();
        $view = new View(
            array(
                'file' =>
                    "{$layout}.{$defaultExtension}"
            )
        );
        $this->willRenderLayoutView = true;
        $this->setLayoutView($view);
        return $this;
    }

    /**
     * Returns the response content type header based on the extension.
     *
     * @return string The content type text.
     */
    public function getContentType()
    {
        if (isset($this->_contentTypes[$this->extension])) {
            return $this->_contentTypes[$this->extension];
        }
        return $this->getDefaultContentType();
    }

    /**
     * Returns the extension for view files.
     *
     * @return string The extension string.
     */
    public function getExtension()
    {
        if (empty($this->_extension)) {
            return $this->getDefaultExtension();
        }
        return $this->_extension;
    }

    /**
     * Sets the values to be used in the views.
     *
     * @param string $key The variable name for the view.
     * @param midex $value The value that will be available in the views
     *  by the key name.
     */
    public function set($key, $value = "")
    {
        if (is_array($key)) {
            foreach ($key as $_key => $value) {
                $this->_set($_key, $value);
            }
            return $this;
        }
        $this->_set($key, $value);
        return $this;
    }

    /**
     * Sets the values to be used in the views.
     *
     * @param string $key The variable name for the view.
     * @param midex $value The value that will be available in the views
     *  by the key name.
     */
    private function _set($key, $value)
    {
        if (!is_string($key)) {
            throw new View\Exception\Data("Key must be a string or a number");
        }
        $this->_viewVars[$key] = $value;
    }

    /**
     * Sets and retrives the controller name.
     * 
     * @return string This controller class name.
     */
    public function getName()
    {
        if (empty($this->_name)) {
            $this->_name = get_class($this);
        }
        return $this->_name;
    }

    /**
     * Sends a redirection header and exite execution.
     * 
     * @param array|string $url The url to redirect to.
     */
    public function redirect($url)
    {
        if (is_array($url)) {
            $url = self::urlFromArray($url);
        }
        $base = Core::$basePath;
        $this->response->setStatusCode(302);
        $this->response->header('Location', "{$base}{$url}");
        $this->disableRendering();
    }

    /**
     * Returns an URL with base path
     * 
     * Creates an url using the current path from router and
     * assigning values from $url.
     * 
     * @param  array $url The array of conponents for the url.
     * 
     * @return string The resultant url.
     */
    public static function urlFromArray($url)
    {
        $_url = array();
        $router = Registry::get("router");
        if (is_object($router)) {
            $_url = array(
                'controller' => $router->controller,
                'action' => $router->action
            );
        }
        $url = array_merge($_url, $url);
        return implode('/', $url);
    }

}
