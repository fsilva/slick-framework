<?php

/**
 * Configuration Argument exception
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Configuration\Exception;

use Slick\Core as Core;

/**
 * Configuration Argument exception
 * 
 * Argument exception is throwed when using an invalid argument with the 
 * Configuration\Driver::parse() method.
 *
 * @package    Slick
 * @subpackage Exception\Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Argument extends Core\Exception
{
    protected $_description = "Trying to initialize the configuration with
            an unset driver type or the driver type
            that you have set isn't found.";
}