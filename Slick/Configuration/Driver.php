<?php

/**
 * Configuration Driver
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Configuration;

use Slick\Base as Base;
use Slick\Configuration\Exception as Exception;

/**
 * Configuration Driver
 *
 * This is an abstract class that defines a configuration driver. All 
 * configuration drivers must extend this class.
 *
 * @package    Slick
 * @subpackage Configuration
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Driver extends Base
{

    /**
     * Store parser options from configuration file.
     * 
     * @var array
     */
    protected $_parsed = array();

    /**
     * Returns an instance of the driver.
     * 
     * @return Slick\Configuration\Driver
     */
    public function initialize()
    {
        return $this;
    }

    /**
     * Overrides the base implementaion exception calling.
     * 
     * @param String $method The method name.
     * 
     * @return Slick\Configuration\Exception\Implementation 
     *  The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }
}