<?php

/**
 * Configuration Ini (*.ini) driver
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Configuration/Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Configuration\Driver;

use Slick\ArrayMethods as ArrayMethods;
use Slick\Configuration as Configuration;
use Slick\Configuration\Exception as Exception;

/**
 * Configuration Ini (*.ini) driver
 * 
 * This driver loads and parses *.ini files.
 *
 * @package    Slick
 * @subpackage Configuration/Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Ini extends Configuration\Driver
{

    /**
     * Parses the .ini file passed in the $path argument.
     *
     * @param string $path The path to the ini file without the extension.
     * 
     * @return Object An object with all options from the configuration file.
     *
     * @throws Slick\Configuration\Exception\Argument If the $path argument is
     *   an empty or null value.
     * @throws Slick\Configuration\Exception\Syntax If any error occour when
     *   trying to parse the file provided in $path argument. Even if the
     *   file is not found it will throw this exception.
     */
    public function parse($path)
    {
        if (empty($path)) {
            throw new Exception\Argument("\$path argument is not valid");
        }

        if (!isset($this->_parsed[$path])) {
            $config = array();

            ob_start();
                @include("{$path}.ini");
                $string = ob_get_contents();
            ob_end_clean();

            $pairs = parse_ini_string($string);

            if (empty($pairs)) {
                throw new Exception\Syntax(
                    "Could not parse configuration file \"{$path}.ini\""
                );
            }

            foreach ($pairs as $key => $value) {
                $config = $this->_pair($config, $key, $value);
            }

            $this->_parsed[$path] = ArrayMethods::toObject($config);
        }

        return $this->_parsed[$path];
    }

    /**
     * Deconstructs the dot notation in config keys into an associative array.
     *
     * @param array  $config The array that will store the configurarion value.
     * @param string $key    The key of configuration entry.
     * @param mixed  $value  The value of the configuration entry.
     * 
     * @return array An array containing the value with in the
     *   correct hierarchy.
     */
    protected function _pair($config, $key, $value)
    {
        if (strstr($key, '.')) {
            $parts = explode('.', $key, 2);
            if (empty($config[$parts[0]])) {
                $config[$parts[0]] = array();
            }
            $config[$parts[0]] = $this->_pair(
                $config[$parts[0]],
                $parts[1],
                $value
            );
        } else {
            $config[$key] = $value;
        }
        return $config;
    }
}
