<?php

/**
 * Inspector
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Inspector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Text;
use Slick\ArrayMethods;

/**
 * Inspector
 * 
 * Inspector uses PHP reflection to inspect classes or objects.
 *
 * @package    Slick
 * @subpackage Inspector
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Inspector
{

    /**
     * The class name or object to inspect.
     * @var string|Object
     */
    protected $_class;

    /**
     * Class metadata.
     * @var array
     */
    protected $_meta = array(
        "class" => array(),
        "properties" => array(),
        "methods" => array()
    );

    /**
     * Array of class properties
     * @var array
     */
    protected $_properties = array();

    /**
     * Array of class methods.
     * @var array
     */
    protected $_methods = array();

    /**
     * Constructs an inspector for a given class.
     *
     * @param string|Object $class The class name or object to inspect.
     */
    public function __construct($class)
    {
        $this->_class = $class;
    }

    /**
     * Retrieves the comment docblock of the class.
     *
     * @return string The class docblock comment or false.
     */
    protected function _getClassComment()
    {
        $reflection = new \ReflectionClass($this->_class);
        return $reflection->getDocComment();
    }

    /**
     * Retrieves the class properties.
     *
     * @return array An array of ReflectionProperty objects.
     */
    protected function _getClassProperties()
    {
        $reflection = new \ReflectionClass($this->_class);
        return $reflection->getProperties();
    }

    /**
     * Retrieves an array with class methods.
     *
     * @return array An array of ReflectionMethod objects
     *   reflecting each method.
     */
    protected function _getClassMethods()
    {
        $reflection = new \ReflectionClass($this->_class);
        return $reflection->getMethods();
    }

    /**
     * Retrieves the property docblock comment.
     *
     * @param string $property Property to retrieve the comment.
     * 
     * @return string The docblock comment from property or false.
     */
    protected function _getPropertyComment($property)
    {
        $reflection = new \ReflectionClass($this->_class);
        return $reflection->getProperty($property)->getDocComment();
    }

    /**
     * Retrieves the method docblock comment.
     *
     * @param string $method The method to retrieve the comment.
     * 
     * @return string The docblock comment from method or false.
     */
    protected function _getMethodComment($method)
    {
        $reflection = new \ReflectionClass($this->_class);
        return $reflection->getMethod($method)->getDocComment();
    }

    /**
     * Parses the docblock comment to retrieve key/value pairs in it.
     *
     * If it finds no value component on comment it sets the key to true.
     * This is useful for flag keys as @readwrite or @once.
     * 
     * @param string $comment The comment to parse.
     * 
     * @return array A key/value(s) associative array from comment.
     */
    protected function _parse($comment)
    {
        $meta = array();
        $pattern = "(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_]*)";
        $matches = Text::match($comment, $pattern);
        if ($matches != null) {
            foreach ($matches as $match) {
                $parts = ArrayMethods::clean(
                    ArrayMethods::trim(Text::split($match, "[\s*]", 2))
                );

                $meta[$parts[0]] = true;

                if (sizeof($parts) > 1) {
                    $meta[$parts[0]] = ArrayMethods::clean(
                        ArrayMethods::trim(Text::split($parts[1], ","))
                    );
                }
            }
        }
        return $meta;
    }

    /**
     * Returns the class meta data.
     *
     * @return array A key/value(s) associative array from class comment.
     */
    public function getClassMeta()
    {
        if (!isset($_meta['class'])) {
            $comment = $this->_getClassComment();

            if (!empty($comment)) {
                $_meta['class'] = $this->_parse($comment);
            } else {
                $_meta['class'] = null;
            }
        }
        return $_meta['class'];
    }

    /**
     * Retrives the list of class properties.
     * 
     * @return array An array with property names.
     */
    public function getClassProperties()
    {
        if (!isset($_properties)) {
            $properties = $this->_getClassProperties();
            foreach ($properties as $property) {
                $_properties[] = $property->getName();
            }
        }
        return $_properties;
    }

    /**
     * Retrieves the list of class methods
     * 
     * @return array An array with method names.
     */
    public function getClassMethods()
    {
        if (!isset($_methods)) {
            $methods = $this->_getClassMethods();
            foreach ($methods as $method) {
                $_methods[] = $method->getName();
            }
        }
        return $_methods;
    }

    /**
     * Returns property meta data.
     *
     * @param string $property The property name to retrieve the meta data.
     * 
     * @return array A key/value(s) associative array from property comment.
     */
    public function getPropertyMeta($property)
    {
        if (!isset($this->_meta['properties'][$property])) {
            $comment = $this->_getPropertyComment($property);
            if (!empty($comment)) {
                $_meta['properties'][$property] = $this->_parse($comment);
            } else {
                $_meta['properties'][$property] = null;
            }
        }
        return $_meta['properties'][$property];
    }

    /**
     * Returns method meta data.
     *
     * @param string $method The method name to retrieve the meta data.
     * 
     * @return array A key/value(s) associative array from method comment.
     */
    public function getMethodMeta($method)
    {
        if (!isset($this->_meta['methods'][$method])) {
            $comment = $this->_getMethodComment($method);
            if (!empty($comment)) {
                $this->_meta['methods'][$method] = $this->_parse($comment);
            } else {
                $this->_meta['methods'][$method] = null;
            }
        }
        return $this->_meta['methods'][$method];
    }

}
