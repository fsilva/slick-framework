<?php

/**
 * Core class.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Text;
use Slick\Router;
use Slick\Registry;
use Slick\Core\Exception;
use Slick\Core\ErrorException;

/**
 * Core class.
 *
 * This class initializes the Slick framework ecosystem.
 * It will register the autoloader for framework and application ans set
 * the initial settings for all framework needs.
 *
 * @package    Slick
 * @subpackage Core
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Core
{

    /**
     * Slick version
     * @var string
     */
    public static $version = "1.0.0";

    /**
     * Stores core configuration debug mode.
     * @var integer
     */
    public static $debugMode = 0;

    /**
     * Base path where app lives
     * @var string
     */
    public static $basePath = '/';

    /**
     * Initializes a web application
     * @param string $appPath The path to the application.
     */
    public static function appInit($appPath)
    {
        $path = dirname($appPath);
        $appPluginsPath = $appPath . DIRECTORY_SEPARATOR . 'Plugins';

        /*$slickPlugins = dirname(dirname(__FILE__))
            . DIRECTORY_SEPARATOR
            . 'Plugins';*/

        set_include_path(
            $appPath. PATH_SEPARATOR .          //Application path
            $appPluginsPath. PATH_SEPARATOR .   //Application plugins path
            //$slickPlugins . PATH_SEPARATOR .    //Slick plugins path
            //CORE_PATH . PATH_SEPARATOR .        //Slick core path
            get_include_path()
        );


        $appPluginLoader = new \SplClassLoader(null, $appPluginsPath);
        $appPluginLoader->register();

        /*$corePluginLoader = new \SplClassLoader(null, $slickPlugins);
        $corePluginLoader->register();*/

        $appLoader = new \SplClassLoader(null, $appPath);
        $appLoader->register();

        self::_getbasePath();
    }

    /**
     * Retrieves the base path from request.
     */
    protected static function _getbasePath()
    {
        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        $qmark = Text::indexOf($uri, '?');
        if ($qmark > -1) {
            $uri = substr($uri, 0, $qmark);
        }
        $url = (isset($_GET['url'])) ? $_GET['url'] : '';
        $ext = (isset($_GET['extension'])) ? $_GET['extension'] : '';
        self::$basePath = str_replace(array('.', $url, $ext), '', $uri) . '/';
        self::$basePath = str_replace('//', '/', self::$basePath);
    }

    /**
     * Bootstrap the application
     */
    public static function bootstrap()
    {
        //Set configuration
        self::setConfiguration();

        //Set the Cache driver (does not conncet)
        self::setSession();

        //Set the Database connector (does not conncet)
        self::setDatabase();

        //Set the Cache driver (does not conncet)
        self::setCache();

        self::_setErrorLevel();
    }

    /**
     * Run the application
     * @codeCoverageIgnore
     */
    public static function run()
    {
        $router = new Router();
        if (is_file(APP_PATH.'/Configuration/Routes.php')) {
            include APP_PATH.'/Configuration/Routes.php';
        }
        $url = isset($_GET['url']) ? $_GET['url'] : "pages/home";
        $ext = isset($_GET['extension']) ? $_GET['extension'] : "html";
        $router->setUrl($url)->setExtension($ext);

        Registry::set('router', $router);
        $router->dispatch();

        $router->response->send();
        unset($router);
    }

    /**
     * Loads a plugin from framwork or application.
     *
     * @param string $pluginName The plugin name.
     */
    public static function load($pluginName)
    {
        $plugins = Registry::get('plugins', array());
        $className = '\\'.$pluginName.'\\Initialize';
        try {
            call_user_func_array(array($className, 'bootstrap'), array());
        } catch (\Exception $e) {
            $error = $e->getMessage();
            throw new Exception\Plugin(
                "Error loading plugin {$pluginName}: {$error}"
            );
        }
        $p = new \stdClass();
        $p->name = $className::$name;
        $p->version = $className::$version;
        $plugins[$pluginName] = $p;
        Registry::set('plugins', $plugins);
    }

    /**
     * Sets the application configuration data.
     */
    public static function setConfiguration()
    {
        $conf = new \Slick\Configuration(array('type' => 'ini'));
        Registry::set('configuration', $conf->initialize());
        $parse = self::_getConfFor('Core');
        if (!empty($parse->debugMode)) {
            self::$debugMode = $parse->debugMode;
        }
    }

    /**
     * Loads database connector with base configuration
     */
    public static function setDatabase()
    {
        $database = new \Slick\Database();
        $parse = self::_getConfFor('Database');
        foreach ($parse->database as $key => $db) {
            if (!empty($db) && !empty($db->type)) {
                $type = $db->type;
                unset($db->type);
                $database->setType($type)->setOptions($db);
            }

            Registry::set('database_'.$key, $database->initialize());
        }
    }

    /**
     * Loads cache connector with base configuration
     */
    public static function setCache()
    {
        $cache = new \Slick\Cache();
        $parse = self::_getConfFor('Cache');

        if (
            !empty($parse->cache->default) 
            && !empty($parse->cache->default->type)
        ) {
            $type = $parse->cache->default->type;
            unset($parse->cache->default->type);
            $cache->setType($type)->setOptions($parse->cache->default);
        }

        Registry::set('cache', $cache->initialize());
    }

    /**
     * Loads session connector with base configuration
     */
    public static function setSession()
    {
        $session = new \Slick\Session();
        $parse = self::_getConfFor('Session');

        if (
            !empty($parse->session->default) 
            && !empty($parse->session->default->type)
        ) {
            $type = $parse->session->default->type;
            unset($parse->session->default->type);
            $session->setType($type)->setOptions($parse->session->default);
        }

        Registry::set('session', $session->initialize());
    }

    /**
     * Retrive a giver configuration ini using configuration.
     *
     * @param string $name The file name
     * 
     * @return Object The data from ini file as an object.
     */
    protected static function _getConfFor($name)
    {
        $conf = Registry::get('configuration');
        $conf = $conf->initialize();
        $inCore = array('Session', 'Cache');
        if (in_array($name, $inCore)) {
            return $conf->parse("Configuration/Core");
        }
        return $conf->parse("Configuration/{$name}");
    }

    /**
     * Set error level based on the $debug value.
     * 
     * @codeCoverageIgnore
     */
    protected static function _setErrorLevel()
    {
        // Set error level based on core debug mode setting
        if (self::$debugMode > 0) {
            if (self::$debugMode == 2) {
                error_reporting(E_ALL);
            } else {
                error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
            }
            ini_set("display_errors", "on");
        } else {
            ini_set("display_errors", "off");
        }
    }
}
