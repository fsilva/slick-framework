<?php

/**
 * Events
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Events
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

/**
 * Events
 * 
 * Events handle event creation and trigger.
 *
 * @package    Slick
 * @subpackage Events
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Events
{

    /**
     * A list of callback methods.
     * @var array
     */
    private static $_callbacks = array();

    /**
     * Avoid the creation of an ArrayMethods instance.
     * @codeCoverageIgnore
     */
    private function __construct()
    {
        // do nothing
    }

    /**
     * Avoid the clonation of an ArrayMethods instance.
     * @codeCoverageIgnore
     */
    private function __clone()
    {
        // do nothing
    }

    /**
     * Adds an callback for an event type.
     * 
     * @param string $type     The event type where to add the callback.
     * @param string $callback The callbacl to add.
     */
    public static function add($type, $callback)
    {
        if (empty(self::$_callbacks[$type])) {
            self::$_callbacks[$type] = array();
        }
        self::$_callbacks[$type][] = $callback;
    }

    /**
     * Fires all the callbacks for a given event type.
     * 
     * @param string $type       The event type that will be triggered.
     * @param mixed  $parameters Parameters to pass to the callbacks.
     */
    public static function fire($type, $parameters = array())
    {
        if (!empty(self::$_callbacks[$type])) {
            foreach (self::$_callbacks[$type] as $callback) {
                call_user_func_array($callback, $parameters);
            }
        }
    }

    /**
     * Removes a callback for a given event type.
     * 
     * @param string $type     The event type where to remove the callback.
     * @param string $callback The callbacl to remove.
     */
    public static function remove($type, $callback)
    {
        if (!empty(self::$_callbacks[$type])) {
            foreach (self::$_callbacks[$type] as $i => $found) {
                if ($callback == $found) {
                    unset(self::$_callbacks[$type][$i]);
                }
            }
        }
    }
}