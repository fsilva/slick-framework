<?php

/**
 * Twig
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Template\Engine\Extension
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Template\Engine\Extension;

use Slick\Core;
use Slick\Registry;
use Slick\Controller;

/**
 * Twig
 * 
 * This is an extention to twig template engine.
 *
 * @package    Slick
 * @subpackage Template\Engine\Extension
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Twig extends \Twig_Extension
{

    /**
     * Sets the extesnio name.
     * 
     * @return string The Extesntion name.
     */
    public function getName()
    {
        return 'Slick';
    }

    /**
     * Adds globals to the twig template engine.
     *  
     * @return array The key/value pair of constant names.
     */
    public function getGlobals()
    {
        $plugins = Registry::get('plugins', array());
        return array(
            '_plugins' => $plugins,
            '_basePath' => Core::$basePath,
            '_version' => Core::$version,
            '_debugMode' => Core::$debugMode,
        );
    }

    /**
     * Adds extension functions to the teiwg environment
     * 
     * @return array A list of Twig_SimpleFunction objects.
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'url', 
                function($path) {
                    if (is_array($path)) {
                        $path = Controller::urlFromArray($path);
                    }
                    return Core::$basePath . $path;
                }
            ),
        );
    }
}