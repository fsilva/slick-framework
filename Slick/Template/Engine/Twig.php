<?php

/**
 * Twig
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Template\Engine
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Template\Engine;

use Slick\Template;
use Slick\Template\Exception;

/**
 * Twig
 * 
 * Twig implementation for framework template system.
 *
 * @package    Slick
 * @subpackage Template\Engine
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Twig extends Template\Engine
{

    /**
     * The twig library template
     * @readwrite
     * @var Twig_Environment
     */
    protected $_twig;

    /**
     * The template souce
     * @read
     * @var String
     */
    protected $_source;

    /**
     * Overrides the Framework\Base to setup the twig library.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        $paths = array(
            CORE_PATH . '/Templates/Twig',
            CORE_PATH . '/Slick/Console/Templates'
        );
        if (is_dir(APP_PATH . '/Views')) {
            array_unshift($paths, APP_PATH . '/Views');
        }

        $this->_twig = new \Twig_Environment(
            new \Twig_Loader_Filesystem($paths)
        );

        $this->_twig->addExtension(new \Twig_Extensions_Extension_I18n());
        $this->_twig->addExtension(new \Twig_Extensions_Extension_Text());
        $this->_twig->addExtension(new Extension\Twig());
    }

    /**
     * Parses the source tempalte code.
     *
     * @param String $source The tempalte to parse
     * 
     * @return \Slick\Template\Engine Returns this instance for
     *  chainnig methods calls.
     */
    public function parse($source) 
    {
        $this->_source = $source;
        return $this;
    }

    /**
     * Processes the template with data to produce the final output.
     *
     * @param mixed $data The data that will be used to process the view.
     * @return String Returns processed output string.
     */
    public function process($data = array())
    {
        try {
            return $this->_twig->render($this->_source, $data);
        } catch (\Exception $e) {
            throw new Exception\Parser($e);
        }
    }
}
