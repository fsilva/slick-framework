<?php

/**
 * Twig
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Template
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Template;

use Slick\Base as Base;
use Slick\Template\Exception as Exception;

/**
 * Engine
 * 
 * Engine defines a template engine to use with framework template.
 *
 * @package    Slick
 * @subpackage Template
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Engine extends Base
{

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param String $method The method name.
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Parses the source tempalte code.
     *
     * @param String $source The tempalte to parse
     * 
     * @return Framework\Template\Engine Returns this instance for
     *  chainnig method calls.
     */
    abstract public function parse($source);

    /**
     * Processes the template with data to produce the final output.
     *
     * @param mixed $data The data that will be used to process the view.
     * 
     * @return String Returns processed output string.
     */
    abstract public function process($data);
}