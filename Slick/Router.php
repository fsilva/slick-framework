<?php

/**
 * Router
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base;
use Slick\Events;
use Slick\Registry;
use Slick\Inspector;
use Slick\Router\Exception;

/**
 * Router
 * 
 * Router will use the URL to determine the correct controller/action to
 * execute. It handles multiple defined routes and inferred routes if no
 * routes are matched.
 *
 * @package    Slick
 * @subpackage Router
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Router extends Base
{

    /**
     * The requested url.
     * @readwrite
     * @var string
     */
    protected $_url;

    /**
     * The requested file extension.
     * @readwrite
     * @var string
     */
    protected $_extension;

    /**
     * The controller to call
     * @read
     * @var string
     */
    protected $_controller = 'pages';

    /**
     * The controler's action/method.
     * @read
     * @var string
     */
    protected $_action = 'index';

    /**
     * A list of defined routes.
     * @read
     * @var array
     */
    protected $_routes = array();

    /**
     * Overrides the base implementaion exception calling.
     *
     * @param string $method The method name.
     * 
     * @return \Slick\Router\Exception\Implementation
     *   The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "{$method} method not implemented"
        );
    }

    /**
     * Adds a route to the list of defined routes.
     *
     * @param \Slick\Router\Route $route The route to add.
     * @return \Slick\Router The self instance for chain calls.
     */
    public function addRoute($route)
    {
        $this->_routes[] = $route;
        return $this;
    }

    /**
     * Removes a route from the list of defined routes.
     *
     * @param \Slick\Router\Route $route The route to remove.
     * @return \Slick\Router The self instance for chain calls.
     */
    public function removeRoute($route)
    {
        foreach ($this->_routes as $i => $stored) {
            if ($stored == $route) {
                unset($this->_routes[$i]);
            }
        }
        return $this;
    }

    /**
     * Returns the list of available route patterns.
     *
     * @return array A key/value pairs of patterns and route class names.
     */
    public function getRoutes()
    {
        $list = array();
        foreach ($this->_routes as $route) {
             $list[$route->pattern] = get_class($route);
        }
        return $list;
    }

    /**
     * Instantiates the controller, runs any @before, @after methods and
     * the action method.
     *
     * @param string $controller The controller class name.
     * @param string $action     The action method in the controller.
     * @param array  $parameters The request parameters as an array.
     */
    protected function _pass($controller, $action, $parameters = array())
    {
        $name = 'Controllers\\'.ucfirst($controller);
        $this->_controller = $controller;
        $this->_action = $action;

        if (!class_exists($name)) {
            $parts = explode('\\', $name);
            $cName = end($parts);
            throw new Exception\Controller(
                "Controller {$cName} not found",
                $cName
            );
        }

        $instance = new $name(
            array(
                'parameters' => $parameters,
                'extension' => $this->_extension
            )
        );

        Registry::set("controller", $instance);

        if (!method_exists($instance, $action)) {
            $instance->willRenderLayoutView = false;
            $instance->willRenderActionView = false;
            throw new Exception\Action("Action {$action} not found");
        }

        $inspector = new Inspector($instance);
        $methodMeta = $inspector->getMethodMeta($action);

        if (
            !empty($methodMeta['@protected']) || 
            !empty($methodMeta['@private'])
        ) {
            throw new Exception\Action("Action {$action} not found");
        }

        $hooks = function ($meta, $type) use ($inspector, $instance) {
            static $run;
            if (is_null($run)) {
                $run = array();
            }
            if (isset($meta[$type])) {
                
                foreach ($meta[$type] as $method) {
                    $hookMeta = $inspector->getMethodMeta($method); 
                    if (
                        in_array($method, $run) &&
                        !empty($hookMeta['@once'])
                    ) {
                        continue;
                    }
                    $run[] = $method;
                    $instance->$method();
                }
                
            }
        };

        $hooks($methodMeta, "@before");

        call_user_func_array(
            array(
                $instance,
                $action
            ),
            is_array($parameters) ? $parameters : array()
        );

        $hooks($methodMeta, "@after");

        $body = call_user_func_array(
            array(
                $instance,
                'render'
            ),
            array()
        );

        $this->response->setBody($body);

        //unset the controller.
        Registry::erase('controller');

    }

    /**
     * Loops thru all routes to find a match for the request string.
     * 
     * If there are no routes it will assume the controller/action/param/param
     * format to inferred the controller, action and parameter to run.
     * It will dispatch the request URL.
     *
     * It fires 'router.before.dispatch' event with parameters:
     *     $url -> The URL to dispatch.
     *
     * It fires 'router.after.dispatch' event with parameters:
     *     $url -> The URL to dispatch;
     *     $controller -> The intended controller
     *     $action -> The intended action
     *     $parameters -> The parameters supplied for the request
     */
    public function dispatch()
    {
        $url = $this->url;
        Events::fire('router.before.dispatch', array($this));
        $parameters = array();
        $controller = "pages";
        $action = "home";
        $matched = false;

        foreach ($this->_routes as $route) {
            $matches = $route->matches($url);
            if ($matches) {
                $controller = $route->controller;
                $action = $route->action;
                $parameters = $route->parameters;
                $matched = true;
                break;
            }
        }

        if (!$matched) {
            $parts = explode("/", trim($url, "/"));

            if (sizeof($parts) > 0 ) {
                $controller = $parts[0];

                if (sizeof($parts) >= 2) {
                    $action = $parts[1];
                    $parameters = array_slice($parts, 2);
                }
            }
        }
        
        $this->_pass($controller, $action, $parameters);
        Events::fire(
            'router.after.dispatch',
            array($this, $controller, $action, $parameters)
        );

        return $this;
    }
}
