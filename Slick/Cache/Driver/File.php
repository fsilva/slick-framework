<?php

/**
 * File cache driver
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Cache\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Cache\Driver;

use Slick\Cache as Cache;
use Slick\Cache\Exception as Exception;

/**
 * File cache driver
 *
 * This cache driver uses the file system to store the cached data.
 * It used the application "Tmp/Cache" folder to store the files. The data
 * is serialized and unserialized in the set/get methods.
 *
 * @package    Slick
 * @subpackage Cache\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class File extends Cache\Driver
{

    /**
     * Service connection state
     * 
     * @readwrite
     * @var boolean
     */
    protected $_isConnected = false;

    /**
     * File path
     * 
     * @readwrite
     * @var String
     */
    protected $_path;

    /**
     * Prefix for keys
     * 
     * @readwrite
     * @var string
     */
    protected $_prefix = 'slick_';

    /**
     * Class constructor
     * 
     * Overrides the default constructor to set the default cache path.
     *
     * @param array|Object $options The properties for the object
     *   beeing constructed.
     */
    public function __construct($options = array())
    {
        $this->_path = APP_PATH . '/Tmp/Cache';
        parent::__construct($options);
    }

    /**
     * Checks if service is a valid instance and its connected.
     *
     * @return boolean True if service is connected and valid,
     *   false otherwise.
     */
    protected function _isValidService()
    {
        $canWrite = is_writable($this->_path);
        if ($this->isConnected && $canWrite ) {
            return true;
        }
        return false;
    }

    /**
     * Verifies the path for cache files.
     *
     * @return Slick\Cache\Driver\File A sefl instance for chaining
     *   method calls.
     *
     * @throws Slick\Cache\Exception\Service If it cannot write contents to
     *   the cache file in the self::$_path directory path.
     */
    public function connect()
    {
        if (!is_writable($this->_path)) {
            throw new Exception\Service(
                "Unable to write cache files to {$this->_path}"
            );
        }
        $this->isConnected = true;
        return $this;
    }

    /**
     * Disconnects to File service.
     *
     * @return Slick\Cache\Driver\File A sefl instance for chaining
     *   method calls.
     */
    public function disconnect()
    {
        $this->isConnected = false;
        return $this;
    }

    /**
     * Retrives a previously stored value.
     *
     * @param String $key     The key under witch value was stored.
     * @param mixed  $default The default value, if no value was stored before.
     * 
     * @return mixed The stored value or the default value if it was
     *  not found on service cache.
     *
     * @throws Slick\Cache\Exception\Service If it cannot write contents to
     *   the cache file in the self::$_path directory path.
     */
    public function get($key, $default = null)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Unable to write to {$this->_path}");
        }

        $value = $this->_get($this->prefix.$key);

        if ($value) {
            return $value;
        }

        return $default;
    }

    /**
     * Utility method to get the contents of a file
     *
     * @param String $key The key under witch value was stored.
     * 
     * @return mixed The stored value
     */
    protected function _get($key)
    {
        $file = $this->_path .'/'. $key;
        if (!file_exists($file)) {
            return false;
        }

        $lines = file($file);

        if (intval(trim($lines[0])) > time()) {
            return unserialize(trim($lines[1]));
        }

        $this->_delete($key);
        return false;
    }

    /**
     * Set/stores a value with a given key.
     *
     * @param String  $key      The key where value will be stored.
     * @param mixed   $value    The value to store.
     * @param integer $duration The live time of cache in seconds.
     * 
     * @return Slick\Cache\Driver\File A sefl instance for chaining
     *   method calls.
     *
     * @throws Slick\Cache\Exception\Service If it cannot write contents to
     *   the cache file in the self::$_path directory path.
     */
    public function set($key, $value, $duration = 120)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Unable to write to {$this->_path}");
        }
        $this->_set($this->prefix.$key, $value, $duration);
        return $this;
    }

    /**
     * Utility method to write the value to the file
     *
     * @param String  $key      The key where value will be stored.
     * @param mixed   $value    The value to store.
     * @param integer $duration The live time of cache in seconds.
     *
     * @return Slick\Cache\Driver\File A sefl instance for chaining
     *   method calls.
     */
    protected function _set($key, $value, $duration)
    {
        $file = $this->_path .'/'. $key;
        $now = time() + $duration;
        $data = serialize($value);
        $contents = "{$now}\n{$data}";
        file_put_contents($file, $contents, LOCK_EX);
        return $this;
    }

    /**
     * Erase the value stored wit a given key.
     *
     * @param String $key The key under witch value was stored.
     * 
     * @return Slick\Cache\Driver\File A sefl instance for chaining
     *   method calls.
     *   
     * @throws Slick\Cache\Exception\Service If it cannot write contents to
     *   the cache file in the self::$_path directory path.
     */
    public function erase($key)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service("Unable to write to {$this->_path}");
        }
        $this->_delete($this->prefix.$key);
        return $this;
    }

    /**
     * Utility method to delete a cache file.
     *
     * @param String $key The key under witch value was stored.
     */
    public function _delete($key)
    {
        $file = $this->_path .'/'. $key;
        if (file_exists($file)) {
            unlink($file);
        }
    }

}
