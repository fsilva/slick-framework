<?php

/**
 * Memcached cache driver
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Cache\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */


namespace Slick\Cache\Driver;

use Slick\Cache as Cache;
use Slick\Cache\Exception as Exception;

/**
 * Memcached cache driver
 * 
 * Memcached is a driver to work with memcached deamon cache.
 *
 * @package    Slick
 * @subpackage Cache\Driver
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Memcached extends Cache\Driver
{

    /**
     * A Memcache instance.
     * 
     * @var \Memcache
     */
    protected $_service;

    /**
     * Memcached host
     * 
     * @readwrite
     * @var string
     */
    protected $_host = '127.0.0.1';

    /**
     * Memcached port
     * 
     * @readwrite
     * @var string
     */
    protected $_port = '11211';

    /**
     * Prefix for keys
     * 
     * @readwrite
     * @var string
     */
    protected $_prefix = 'slick_';

    /**
     * Service connection state
     * 
     * @readwrite
     * @var boolean
     */
    protected $_isConnected = false;

    /**
     * Checks if service is a valid instance and its connected.
     *
     * @return boolean True if service is connected and valid, false otherwise.
     */
    protected function _isValidService()
    {
        $isEmpty = empty($this->_service);
        $isInstance = $this->_service instanceof \Memcache;

        if ($this->isConnected && $isInstance && !$isEmpty) {
            return true;
        }

        return false;
    }

    /**
     * Connects to Memcached service.
     *
     * @return Slick\Cache\Driver\Memcached A sefl instance for chaining
     *   method calls.
     *
     * @throws Slick\Cache\Exception\Service If an error occours when trying
     *   to connect to memcached service.
     */
    public function connect()
    {
        try {
            $this->_service = new \Memcache();
            $this->_service->connect($this->host, $this->port);
            $this->isConnected = true;
        } catch (\Exception $e) {
            throw new Exception\Service(
                "Unable to connecto to Memcached service"
            );
        }

        return $this;
    }

    /**
     * Disconnects the Memcached service.
     *
     * @return Slick\Cache\Driver\Memcached A sefl instance for chaining
     *   method calls.
     */
    public function disconnect()
    {
        if ($this->_isValidService()) {
            $this->_service->close();
            $this->isConnected = false;
        }

        return $this;
    }

    /**
     * Retrives a previously stored value.
     *
     * @param String $key     The key under witch value was stored.
     * @param mixed  $default The default value, if no value was stored before.
     * 
     * @return mixed The stored value or the default value if it was
     *  not found on service cache.
     *
     * @throws Slick\Cache\Exception\Service If you are trying to set a cache
     *   values without connecting to memcached service first.
     */
    public function get($key, $default = null)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service(
                "Not connected to a valid Memcached service"
            );
        }

        $value = $this->_service->get($this->prefix.$key, MEMCACHE_COMPRESSED);

        if ($value) {
            return $value;
        }

        return $default;
    }

    /**
     * Set/stores a value with a given key.
     *
     * @param String  $key      The key where value will be stored.
     * @param mixed   $value    The value to store.
     * @param integer $duration The live time of cache in secondes.
     * 
     * @return Slick\Cache\Driver\Memcached A sefl instance for chaining
     *   method calls.
     *
     * @throws Slick\Cache\Exception\Service If you are trying to set a cache
     *   values without connecting to memcached service first.
     */
    public function set($key, $value, $duration = 120)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service(
                "Not connected to a valid Memcached service"
            );
        }
        $this->_service->set(
            $this->prefix.$key,
            $value,
            MEMCACHE_COMPRESSED, 
            $duration
        );
        return $this;
    }

    /**
     * Erase the value stored wit a given key.
     *
     * @param String $key The key under witch value was stored.
     * 
     * @return Slick\Cache\Driver\Memcached A sefl instance for chaining
     *   method calls.
     *
     * @throws Slick\Cache\Exception\Service If you are trying to erase a cache
     *   values without connecting to memcached service first.
     */
    public function erase($key)
    {
        if (!$this->_isValidService()) {
            throw new Exception\Service(
                "Not connected to a valid Memcached service"
            );
        }
        $this->_service->delete($this->prefix.$key);
        return $this;
    }
}
