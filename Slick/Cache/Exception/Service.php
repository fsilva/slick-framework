<?php

/**
 * Cache service exception
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Cache\Exception;

use Slick\Core as Core;

/**
 * Cache service exception
 * 
 * Service exception is throwed when an error occours when trying to
 * connect to the cache service provider.
 *
 * @package    Slick
 * @subpackage Exception\Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Service extends Core\Exception
{
    protected $_description = "The cache service you are trying to use isn't
            responding correctly or isn't installed in this server.<br />Check
            if the service is correctly installed and you have correctly set
            the settings for it and try again.";
}
