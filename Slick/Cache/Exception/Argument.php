<?php

/**
 * Cache argument exception
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Exception\Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Cache\Exception;

use Slick\Core as Core;

/**
 * Cache Argument exception.
 *
 * Argument exception is thrown when you try to initialize the cache with an
 * unset driver type or the driver type that you have set isn't found.  
 *
 * @package    Slick
 * @subpackage Exception\Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Argument extends Core\Exception
{
    protected $_description = "Trying to
            initialize the cache with an unset driver type or the driver type
            that you have set isn't found.";
}
