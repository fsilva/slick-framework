<?php

/**
 * Cache driver
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Cache;

use Slick\Base as Base;
use Slick\Cache\Exception as Exception;

/**
 * Cache driver.
 *
 * This is an abstract class that defines a cache driver.
 * If you create a new cache driver your class must extend this class as
 * it implements the cache driver interface that slick cache knows of.
 *
 * @package    Slick
 * @subpackage Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
abstract class Driver extends Base implements CacheDriverInterface
{

    /**
     * Store parser options from Cache file.
     * 
     * @var array
     */
    protected $_parsed = array();

    /**
     * Returns an instance of the driver.
     *
     * @return Framework\Cache\Driver
     */
    public function initialize()
    {
        return $this;
    }

    /**
     * Overrides the base implementaion exception calling.
     * 
     * @param String $method The method name.
     * 
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation(
            "The '{$method}' method is not implemented."
        );
    }

}

