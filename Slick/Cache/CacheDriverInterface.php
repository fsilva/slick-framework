<?php

/**
 * Cache driver interface
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick\Cache;

/**
 * Cache driver interface
 *
 * This interface defines the methods for a cache driver.
 *
 * @package    Slick
 * @subpackage Cache
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
interface CacheDriverInterface
{

    /**
     * Returns an instance of the driver.
     *
     * @return Slick\Cache\Driver The self instance for chaining method calls.
     */
    public function initialize();
    
    /**
     * Connects to Driver service.
     *
     * @return Slick\Cache\Driver The self instance for chaining method calls.
     */
    public function connect();

    /**
     * Disconnects from Driver service.
     *
     * @return Slick\Cache\Driver The self instance for chaining method calls.
     */
    public function disconnect();

    /**
     * Retrives a previously stored value.
     *
     * @param String $key     The key under witch value was stored.
     * @param mixed  $default The default value, if no value was
     *   stored before.
     * 
     * @return mixed The stored value or the default value if it was
     *  not found on service cache.
     */
    public function get($key, $default = null);

    /**
     * Set/stores a value with a given key.
     *
     * @param String  $key      The key where value will be stored.
     * @param mixed   $value    The value to store.
     * @param integer $duration The live time of cache in secondes.
     * 
     * @return Slick\Cache\Driver The self instance for chaining method calls.
     */
    public function set($key, $value, $duration = 120);

    /**
     * Erase the value stored wit a given key.
     *
     * @param String $key The key under witch value was stored.
     * 
     * @return Slick\Cache\Driver The self instance for chaining method calls.
     */
    public function erase($key);
}