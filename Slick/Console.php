<?php

/**
 * Slick Framework console dispatcher.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

namespace Slick;

use Slick\Base as Base;
use Slick\Core as Core;
use Slick\Text as Text;
use Slick\Console\Colors as Colors;
use Slick\Console\Exception as Exception;

/**
 * Console class handles the script execution
 *
 * @package    Slick
 * @subpackage Console
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 */
class Console extends Base
{

    /**
     * The ASCII art displayed when console starts
     * @var string
     */
    protected $_banner = <<<banner
  __  _ _      _                                _      
 / _\| (_) ___| | __   ___ ___  _ __  ___  ___ | | ___ 
 \ \ | | |/ __| |/ /  / __/ _ \| '_ \/ __|/ _ \| |/ _ \
 _\ \| | | (__|   <  | (_| (_) | | | \__ \ (_) | |  __/
 \__/|_|_|\___|_|\_\  \___\___/|_| |_|___/\___/|_|\___| 
banner;

    /**
     * The list script arguments.
     * @readwrite
     * @var array
     */
    protected $_args = array();

    /**
     * The list of flags used in command line
     * @readwrite
     * @var array
     */
    protected $_flags = array();

    /**
     * The Slick core path
     * @readwrite
     * @var string
     */
    protected $_core;

    /**
     * Slick shell for this call.
     * @read
     * @var Slick\Console\Shell
     */
    protected $_shell;

    /**
     * Stores the working directory path
     * @var string
     */
    public static $workingPath;

    /**
     * Flag for valid shell initialization.
     * @read
     * @var boolean
     */
    protected $_hasValidShell = false;

    /**
     * The verbose flag
     * @readwrite
     * @var boolean
     */
    protected $_verbose = false;

    /**
     * The quiet flag
     * @readwrite
     * @var boolean
     */
    protected $_quiet = false;

    /**
     * Flag to enable the use of ANSI colors.
     * @readwrite
     * @var boolean
     */
    protected $_colors = false;

    /**
     * Flag for plugin usage
     * @readwrite
     * @var boolean
     */
    protected $_usePlugin = false;

    /**
     * Plugin name to use
     * @readwrite
     * @var string
     */
    protected $_plugin = null;

    /**
     * Available error levels.
     * @readwrite
     * @var array
     */
    protected $_logLevels = array(
        'info' => 'blue',
        'debug' => 'light_cyan',
        'warning' => 'light_yellow',
        'error' => 'light_red',
    );

    /**
     * Overrides the base implementation exception calling.
     * 
     * @param string $method The method name.
     * 
     * @return Exception\Implementation The implementation exception.
     */
    protected function _getExceptionForImplementation($method)
    {
        return new Exception\Implementation("{$method} method not implemented");
    }

    /**
     * Prints out welcome banner, and starts console tasks.
     * 
     * @param array|Object $options The properties for the object
     *   being constructed.
     */
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->checkColors();
        $this->prepare();
    }

    /**
     * Returns the value of a flag used in the command.
     * 
     * @param string $name  The flag name. For example 'colors' for --colors
     * @param string $short The short version of the name. Example -c
     * 
     * @return string|boolean This will return the value string if specified,
     *   true if the flag is present but without any value or false if the flag 
     *   was not used in the command by the user.
     */
    public function getFlag($name, $short = false)
    {
        $return = null;
        if (isset($this->_flags[$name])) {
            $return = $this->_flags[$name];
        }

        if ($short && isset($this->_flags[$short])) {
            $return = $this->_flags[$short];
        }
        return $return;
    }

    /**
     * Dispatch the current request by running the shell main method.
     *
     * @return void
     */
    public function dispatch()
    {
        if ($this->_hasValidShell) {
            $this->shell->prepare();
            $name = ' ' . $this->shell->getName() . ' Shell';
            $description = $this->shell->getDescription();
            if ($this->colors) {
                $name = Colors::set($name, 'white');
                $description = Colors::set($description, 'light_gray');
            }
            $this->out($name)
                ->hBar()
                ->out($description, true)
                ->bLine();
            $this->shell->main();
        }
    }

    /**
     * Initializes the shell based on arguments passed.
     *
     * This method will search for shell classes in APP and Core 
     * paths, under Console\Shell folders.
     * 
     * @return boolean True if the class was found and initialized,
     *   false otherwise.
     */
    protected function initiateShell()
    {
        $name = isset($this->_args[0]) ? 
            ucfirst($this->_args[0]) : 'DefaultShell';

        $appShell = "Console\\Shell\\{$name}";
        $coreShell = "Slick\\Console\\Shell\\{$name}";
        $pluginShell = ($this->usePlugin) ? 
            "{$this->_plugin}\\{$appShell}" : false;

        if (class_exists($appShell)) {
            $this->_shell = new $appShell(array('console' => $this));
        } else if (class_exists($coreShell)) {
            $this->_shell = new $coreShell(array('console' => $this));
        } else if ($pluginShell && class_exists($pluginShell)) {
            $this->_shell = new $pluginShell(array('console' => $this));
        } else {
            $this->error("Cannot find shell class '{$name}'");
            return false;
        }

        return true;
    }

    /**
     * Prints out a message if the console isn't running in quiet mode.
     *
     * This should always be used for outputting information.
     * 
     * @param string  $message The message to print out.
     * @param boolean $wrap    If the text is longer then the terminal with
     *   you set this flag as true. Output will be split in several lines
     *   in order to fit the terminal width.
     * @param boolean $newLine If set to true it will print out a new line
     *   character after the output message. Default to true.
     * 
     * @return Slick\Console A self instance for method chain calls.
     */
    public function out($message = '', $wrap = false, $newLine = true)
    {

        if (!$this->quiet) {
            if (!$wrap) {
                print $newLine ? "{$message}\n" : $message;
                return $this;
            }

            $size = $this->getTerminalSize();
            $lines = Text::splitInLines($message, $size['width']);
            foreach ($lines as $line) {
                print "{$line}\n";
            }
        }
        return $this;
    }

    /**
     * Prints out an horizontal bar.
     *
     * @param char   $char  The character to use for drawing the bar.
     * @param string $color The string that identifies the ANSI color to use
     *
     * @see  Slick\Console\Colors
     * @return Slick\Console A self instance for method chain calls.
     */
    public function hBar($char = '-', $color = 'light_gray')
    {
        $size = $this->getTerminalSize();
        $line = '';
        for ($i = 0; $i<$size['width']; $i++) {
            $line .= $char;
        }
        if ($this->colors) {
            $line = Colors::set($line, $color);
        }
        $this->out($line);
        return $this;
    }

    /**
     * Prints out a blank line.
     * 
     * @codeCoverageIgnore
     * @return Slick\Console A self instance for method chain calls.
     */
    public function bLine()
    {
        $this->out();
        return $this;
    }

    /**
     * Prompts the user for input.
     *
     * @param string $message The message or questions that will be printed
     *   before the prompt cursor.
     *   
     * @codeCoverageIgnore
     * @return string The input from the user.
     */
    public function prompt($message)
    {
        if (PHP_OS == 'WINNT') {
            print $message;
            $input = stream_get_line(STDIN, 1024, PHP_EOL);
        } else {
            $input = readline("{$message} ");
        }
        return $input;
    }

    /**
     * Logs and prints out log messages.
     * 
     * @param string  $message The message to log.
     * @param string  $level   The log level for this message. Available levels
     *   for console are "info", "debug", "warning" and "error".
     * @param string  $body    A console output color for the message.
     * 
     * @param boolean $force   This flag set to true log will for to output the
     *   error in the console. This is used by Console::error().
     *  
     * @see  Slick\Console\Colors
     * @return Slick\Console A self instance for method chain calls.
     */
    public function log($message, $level = 'info', $body="light_gray",
        $force = false
    )
    {
        $now = strftime('%Y-%m-%d %T');
        $display = strtoupper($level);
        $info = "[{$now}] {$display}: ";
        if ($this->colors && isset($this->_logLevels[$level])) {
            $info = Colors::set($info, $this->_logLevels[$level]);
            $message = Colors::set($message, $body);
        }
        if ($force OR $this->verbose) {
            $this->bLine()->out("{$info}{$message}");
        }
        return $this;
    }

    /**
     * Logs an info level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console A self instance for method chain calls.
     */
    public function info($message)
    {
        return $this->log($message);
    }

    /**
     * Logs a debug level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console A self instance for method chain calls.
     */
    public function debug($message)
    {
        return $this->log($message, 'debug');
    }

    /**
     * Logs a warning level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console A self instance for method chain calls.
     */
    public function warning($message)
    {
        return $this->log($message, 'warning');
    }

    /**
     * Logs an error level message.
     * 
     * @param string $message The message to log.
     * 
     * @return Slick\Console A self instance for method chain calls.
     */
    public function error($message)
    {
        return $this->log($message, 'error', 'light_red', true);
    }

    /**
     * Retrieves the current terminal width (cols) and height (lines).
     *
     * @codeCoverageIgnore
     * @return array An array with two keys: width and height.
     */
    public function getTerminalSize()
    {
        $ds = DIRECTORY_SEPARATOR;
        $size = $output = array();
        if ($ds === '\\') {
            return $this->getTerminalSizeOnWindows();
        }

        $cmd = 'echo "lines\ncols"|tput -S';
        exec($cmd, $output);
        $size['width'] = $output[1];
        $size['height'] = $output[0];
        return $size;        
    }

    /**
     * Checks and sets the ability to output ANSI colors.
     *
     * @return void
     */
    protected function checkColors()
    {
        $colors = Colors::capable();
        $current = $this->getFlag('colors', 'c');
        if ($current !== null) {
            $colors = (int) $current;
        }
        $this->_colors = $colors;
    }

    /**
     * Prints out welcome banner
     * 
     * @codeCoverageIgnore
     * @return void
     */
    protected function printWelcome()
    {
        if (!$this->quiet) {
            passthru('clear');
        }
        $banner = $this->_banner;
        $version = " Slick PHP Framework " 
                 . Core::$version
                 . " by Filipe Silva";
        if ($this->colors) {
            $banner = Colors::set($banner, 'light_cyan');
            $version = Colors::set($version, 'light_gray');
        }
        $this->out($banner)->hBar()->out($version)->bLine();
    }

    /**
     * Prepares the arguments and initializes the shell for this call.
     *
     * @return void
     */
    protected function prepare()
    {
        $this->parseArguments();
        Console::$workingPath = $this->getFlag('working', 'w');
        $this->_verbose = $this->getFlag('verbose', 'v');
        $this->_quiet = $this->getFlag('quiet', 'q');
        $plugin = $this->getFlag('plugin', 'p');
        $this->_usePlugin = (boolean) $plugin;
        $this->_plugin = $plugin;
        $this->checkColors();
        $this->printWelcome();
        $this->_hasValidShell = $this->initiateShell();
    }

    /**
     * Parses the input arguments and flags from command line.
     *
     * @return void
     */
    protected function parseArguments()
    {
        $args = $flags = array();
        array_shift($this->_args);
        while (count($this->_args) > 0) {
            $arg = array_shift($this->_args);
            if (Text::match($arg, "^\-{1,2}.*$")) {
                if (Text::indexOf($arg, '=') > 0) {
                    $parts = explode('=', $arg);
                    $flags[str_replace('-', '', $parts[0])] = $parts[1];
                } else if (Text::match($arg, "^\-{1}[A-Za-z0-9\/\_]*$")) {
                    $value = (strlen($arg) > 2) ? substr($arg, 2) : true;
                    $flags[substr($arg, 1, 1)] = $value;
                } else {
                    $flags[str_replace('-', '', $arg)] = true;
                }
            } else {
                $args[] = $arg;
            }
        }

        $this->_args = $args;
        $this->_flags = $flags;
    }

    /**
     * Retrieves the current terminal width and height in MS Windows.
     *
     * @codeCoverageIgnore
     * @return array An array with two keys: width and height.
     */
    protected function getTerminalSizeOnWindows()
    {
        $output = array();
        $size = array('width'=>0,'height'=>0);
        exec('mode', $output);
        foreach ($output as $line) {
            $matches = array();
            $w = preg_match('/^\s*columns\:?\s*(\d+)\s*$/i', $line, $matches);
            if ($w) {
                $size['width'] = intval($matches[1]);
            } else {
                $h = preg_match('/^\s*lines\:?\s*(\d+)\s*$/i', $line, $matches);
                if ($h) {
                    $size['height'] = intval($matches[1]);
                }
            }
            if ($size['width'] AND $size['height']) {
                break;
            }
        }
        return $size;
    }

    /**
     * Prints out a goodbye message before destruction.
     * 
     * @codeCoverageIgnore
     */
    public function __destruct()
    {
        $this->bLine();
    }

}