<?php
/**
 * Slick console bootstrap.
 *
 * @package Slick
 * @subpackage Console
 * @since Version 0.1.0
 * @author Filipe Silva <silvam.filipe@gmail.com>
 *
 * @license Apache License, Version 2.0 (the "License")
 * @copyright 2013 Filipe Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Slick\Core as Core;
use Slick\Console as Console;

//Set include path and loads the autoloader class.
$frameworkPath = dirname(dirname(__FILE__));
define("CORE_PATH", $frameworkPath);

require_once CORE_PATH . DIRECTORY_SEPARATOR . 'SplClassLoader.php';

$vendors = CORE_PATH . DIRECTORY_SEPARATOR . 'Vendors/autoload.php';
$runOutsideComposer = is_file($vendors);

//Load modules from composer
if ($runOutsideComposer) {
    require_once $vendors;
} else {
    //Load modules from composer
    $vendor = dirname(dirname(dirname(dirname(__DIR__))));
    require_once $vendor . '/autoload.php';
}
$spl = new SplClassLoader('Slick', CORE_PATH);
$spl->register();

//Initialize framework
$parts = explode("=", $argv[1]);
define('APP_PATH', end($parts));
Core::appInit(APP_PATH);

//dispatch the requested script
$console = new Console(array('args' => $argv, 'core' => $frameworkPath));
$console->dispatch();
