<?PHP

/**
 * Slick framework bootstrap for web applications.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @author     Filipe Silva <silvam.filipe@gmail.com>
 * @copyright  Filipe Silva 2013
 * @license    Apache License, Version 2.0 (the "License")
 * @since      Version 1.0.0
 */

require_once CORE_PATH . DIRECTORY_SEPARATOR . 'SplClassLoader.php';

//Load modules from composer
$vendors = CORE_PATH . DIRECTORY_SEPARATOR . 'Vendors/autoload.php';
$runOutsideComposer = is_file($vendors);

//Load modules from composer
if ($runOutsideComposer) {
    require_once $vendors;
} else {
    //Load modules from composer
    $vendor = dirname(dirname(dirname(__DIR__)));
    require_once $vendor . '/autoload.php';
}

/**
 * Set SPL autoloader
 * @var /SplClassLoader
 */
$spl = new SplClassLoader('Slick', CORE_PATH);
$spl->register();

/**
 * Handles php error.
 * 
 * @param integer $severity The level of the error raised.
 * @param string  $msg      The error message
 * @param string  $file     The filename that the error was raised in
 * @param integer $line     The line number the error was raised at
 * @param array   $context  an array that points to the active symbol
 *  table at the point the error occurred
 * 
 * @return void
 * 
 * @throws \Slick\Core\Exception\Error If called.
 */
function _errorHandler($severity, $msg, $file, $line, $context)
{
    throw new \Slick\Core\Exception\Error($msg, 0, $severity, $file, $line);
}

/**
 * Handles uncoutch exceptions
 * 
 * @param  Exception $e The thrown exception.
 * 
 * @return boolean Always return true.
 */
function _exceptionHandler(\Exception $e)
{
    if (is_a($e, '\Slick\Core\Exception')) {
        header(' ', true, $e->getHttpCode());
        print $e->render();
    } else {
        header(' ', true, 500);
        print $exception;
    }
    return true;
}

//Register error handler
set_exception_handler('_exceptionHandler');
set_error_handler('_errorHandler');

